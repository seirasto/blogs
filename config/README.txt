
This README describes the files necessary to use the phrase-level sentiment
analyzer implemented for LiveJournal. For background on the method used to build
the classifier, please consult Agarwal et al.'s "Contextual Phrase-Level
Polarity Analysis using Lexical Affect Scoring and Syntactic N-grams".

The Wiktionary code is originally by Weiwei, although I've made a few changes to
allow us to extract links.

Everything else was written by Jacob Andreas (jda2129@columbia). Please feel
free to contact me if you have any other questions.


FILES

data/

DAL.txt
    The Dictionary of Affect in Language. Contains prior polarities for various
    dictionary words. See "Whissell's Dictionary of Affect in Language
    Technical Manual and User's Guide" (Whissel).

database.mpqa.2.0.tar.gz
    A tarball containing the complete MPQA corpus. You will need to extract this
    before using it. See "Annotating Expressions of Opinions and Emotions in
    Language" (Wiebe et al.).

lj-sub/
    Contains the subset of LiveJournal sentences analyzed for the first round.
    If you just want to test the old data, the only files you are interested in
    are lj-sub/chk/ann/complete.ann and lj-sub/chk/done/complete.chk (see
    "Annotation format" below). The rest are just intermediate files. These
    tags and chunks were generated using the CRF chunker.

mpqa-sub/
    Contains the annotated MPQA sentences used to train the classifier. These
    were generated using the preprocessor tools described below.
    
WordNet-3.0.tar.bz2
    The latest version of Wordnet.



preprocessor/

cplines.rb
    Copies the first n lines of one file to another, where n is a number
    secified on the command line (can't remember what this is used for).

extractor.rb
    Extracts annotations in the format used by our code (see "Annotation
    format").

sorter.rb
    Realigns the annotation boundaries so that they match up with the new word
    breaks introduced by the tagger.



Sentiment/

Contains a Netbeans project with the main sentiment analysis code.


wiktionary/

randomWalk.pl
    Code to do a random walk of the specified length through Wiktionary

wik.tar.gz
    Archive of Weiwei's code, tweaked to output link information (see
    "Limitations", below)

Wiktionary
    Weiwei's extracted code



SETUP

* Change file references in code to point to the location of those files on your
hard drive.

* Setup the WordNet interface by modifying Sentiment/config/file_properties.xml
to point to the location of WordNet on your hard drive.



ANNOTATION FORMAT

The program expects annotations in pairs of files: an "annotations file" and a
"chunks file". Every line of the chunks file should contain a single chunked
sentence, and every line of the annotations file should contain a list of three
comma-separated values, as follows:
1. "p" if the annotated phrase is positive, or "n" if the annotated phrase is
negative.
2. The index in the sentence of the first chunk in the annotated phrase,
starting from zero.
3. The index of the last chunk in the annotated phrase PLUS ONE.
e.g.
chunks: hope/NN/B-NP you/PRP/B-NP find/VBP/B-VP peace/NN/B-NP soon/RB/B-ADVP ././O
ann: p,0,4



LIMITATIONS

The Wiktionary walk code is a bit of a hack right now -- at some point we may
want to rewrite it to be a little more robust. At the moment, we indicate the
presence of links in our internal storage by marking them at the end with a
special symbol like *, which obviously means we can't search for multi-word
links appropriately. This should be fairly simple to take care of with a regex.
