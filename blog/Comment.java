/**
 * 
 */
package blog;

import java.util.Date;
import java.util.List;

/**
 * @author sara
 *
 */
public abstract class Comment {

	protected String username;
	protected String commentUrl;
	protected Date date;
	protected String comment;
	protected String id;
	
	public Comment(String commentUrlIn, String usernameIn, Date dateIn, String commentIn, String idIn) {
		commentUrl = commentUrlIn;
		username = usernameIn;
		date = dateIn;
		comment = commentIn;
		id = idIn;
	}
	
	public String getURL() {
		return commentUrl;
	}
	
	public String getUsername() {
		return username;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getID() {
		return id;
	}
	
	public String getComment() {
		return comment;
	}
	
	public abstract String getCommentText();
	public abstract List<Sentence> getSentences();
	
	public String toString() {
		return "username: " + username + 
				", comment: " + comment +
				", url: " + commentUrl +
				", date: " + date;
	}
	
	public int compareTo(Comment c) {
		if (c == null) return 0;
		return this.getDate().compareTo(c.getDate());
	}
}
