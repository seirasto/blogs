package blog;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import java.io.*;
import java.util.*;
import processing.*;
import sentiment.Utils;
import dom.Writer;

public class XMLUtilities {

	public static void main(String [] args) {
		
		//String document = "/proj/fluke/SCIL/corpora/wikipedia/WD_batch1LJ/Barack_Obama__Archive_70___Response_to_Gulf_Oil_Spill.xml";
		//String document = "/proj/nlp/users/sara/agreement/res/processed/processed-Barack_Obama__Archive_70___Citizenship_conspiracy_theories.xml/Barack_Obama__Archive_70___Citizenship_conspiracy_theories.xml.sentences";
		String document = "/proj/nlp/users/sara/corpora/discussion_forums/create_debate/religion/debate.show.Does_history_have_value_outside_of_academics_lj.xml";
		
		try {
			
			Contractions contractionDictionary = new Contractions(true);
			EmoticonDictionary emoticonDictionary = new EmoticonDictionary();
			
			removeInvalidXML(document);
			addSentenceTagsToBlogs(document,
					"/home/sara/tmp.xml.sentences",
					emoticonDictionary, contractionDictionary,"(?<=[.!?])\\s|\\n+");
			Document entry = XMLUtilities.getDocument("/home/sara/tmp.xml.sentences");
			//Document entry = XMLUtilities.getDocument(document);
			List<String> sentences = XMLUtilities.getSentences(entry);
			List<String> ids = XMLUtilities.getIDs(entry);
			
			for (int i = 0; i < sentences.size(); i++) {
				System.out.println(ids.get(i) + ": " + sentences.get(i));
			}
			//Document document = getDocument("/proj/nlp/users/sara/persuasion/corpus/entries-flat/processed-33149.xml/33149.xml.sentences");
			//System.out.println(document.getElementsByTagName("sentence").item(0).getFirstChild());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Document getDocument(String fileName) {
		DOMParser parser = new DOMParser();
		
		Document document = null;
		
		try {
			parser.setFeature("http://xml.org/sax/features/namespaces", false);
		
			InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName),"UTF-8");
			InputSource in = new InputSource(isr);
			parser.parse(in);
			
			document = parser.getDocument();
		
		} catch (Exception e) {
			System.err.println("[XMLUtilities.getDocument] " + e);
			e.printStackTrace();
		}
		
		return document;
	}
	
	public static void WriteToFile(String file, Document document) {
		try {
	    	FileOutputStream xml = new FileOutputStream(file);
			Writer writer = new Writer();
			writer.setOutput(xml,"UTF-8");
			writer.write(document);
		} catch (Exception e) {
			System.err.println("[XMLUtilties.WriteToFile] " + e);
			e.printStackTrace();
		}
	}
	
	public static String writeSentencesToTxtFile(String fileName, List<String> sentences) {
		return writeSentencesToTxtFile(fileName,".txt",sentences);
	}
	
	public static String writeSentencesToTxtFile(String fileName, String ext, List<String> sentences) {
		
		// write to file
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(fileName + ext));

	    	for (String sentence : sentences) {
	    		sentence = sentence.replaceAll("\\n", " ").trim();
	    		if (sentence.equals("")) continue;
	    		out.write(web.HTML.htmlToText(sentence) + "\n");
	    	}
			out.close();
		} catch (Exception e) {
			System.err.println("[XMLUtilities.writeSentenceToTxtFile] Error writing to file " + fileName + "." + ext + ":" + e);
		}
		return fileName + ext;
		
	}
	
	public static ArrayList<String> getSentences(Document entry) {
		
		ArrayList<String> sentences = new ArrayList<String>();
		
		NodeList sentenceElements = entry.getElementsByTagName("sentence");
		
		for (int index = 0; index < sentenceElements.getLength(); index++) {
			// empty sentence tags
			if (sentenceElements.item(index).getFirstChild() == null) continue;
			sentences.add(sentenceElements.item(index).getFirstChild().getTextContent());
		}
		
		return sentences;
	}
	
	public static ArrayList<String> getIDs(Document entry) {
		
		ArrayList<String> ids = new ArrayList<String>();
		
		Element e = (Element)entry.getElementsByTagName("description").item(0);
		
		NodeList s = e.getElementsByTagName("sentence");
		
		for (int index = 0; index < s.getLength(); index++) {
			if (s.item(index).getFirstChild() == null) continue;
			ids.add("root-" + index);
		}
		
		NodeList n = entry.getElementsByTagName("comment");
		
		for (int i = 0; i < n.getLength(); i++) {
			s = ((Element)n.item(i)).getElementsByTagName("sentence");
		
			for (int index = 0; index < s.getLength(); index++) {
				if (s.item(index).getFirstChild() == null) continue;
				ids.add(((Element)n.item(i)).getAttribute("url") + "-" + index);
			}
		}
		return ids;
	}
	
	/**
	 * Add sentence tags
	 * @param fileName
	 * @return
	 */
	public static String addSentenceTagsToBlogs(String fileName, String outputFileName, EmoticonDictionary emoticonDictionary, Contractions contractionDictionary) {
		return addSentenceTagsToBlogs(fileName,outputFileName,emoticonDictionary,contractionDictionary,null);
	}
	
	/**
	 * Add sentence tags
	 * @param fileName
	 * @return
	 */
	public static String addSentenceTagsToBlogs(String fileName, String outputFileName, EmoticonDictionary emoticonDictionary, Contractions contractionDictionary, String delim) {
		
    	try {
        		Document entry = getDocument(fileName);
        			        	
        		// entry
		    	Element description = (Element)((Element)entry.getElementsByTagName("item").item(0)).getElementsByTagName("description").item(0);
	        	
		    	// skip empty comment
	    		if (description.getFirstChild() != null) {
		    	
					// use sentence splitter to ensure that each sentence is on 
					// it's own line so that sentences will be looked at for sentiment
			    	String content = web.HTML.htmlToText(description.getFirstChild().getTextContent());
	
			    	content = preprocess(content,contractionDictionary);
			    	
			    	List<String> sentences = null;
			    	
			    	if (delim == null) {
			    		sentences = NlpUtils.splitSentences(content);
			    	}
			    	else {
			    		String delimited = content.replaceAll("(" + delim + ")", "$1DELIM");
			    		sentences = Arrays.asList(delimited.split("DELIM"));		    		
			    	}
			    	Element newDescription = entry.createElement("description");
			    	
			    	for (String sentence : sentences) {
			    		
			    		// add emoticons to previous sentence
			    		/*if (emoticonDictionary.hasEmoticon(sentence)) {
			    			Element sElement = (Element)newDescription.getChildNodes().item(newDescription.getChildNodes().getLength()-1);
			    			sElement.setTextContent(sElement.getTextContent() + sentence);
			    		}*/
			    		
			    		sentence = postprocess(sentence);
			    		if (sentence.matches("[   \\s]*")) continue;
			    		Element sElement = entry.createElement("sentence");
			    		Text s = entry.createTextNode("sentence");
						s.setTextContent(sentence);
						sElement.appendChild(s);
						newDescription.appendChild(sElement);					
			    	}
			    	entry.getElementsByTagName("item").item(0).replaceChild(newDescription, description);
	    		}
	    		
		    	// comments
		    	NodeList comments = entry.getElementsByTagName("text");
		    	
		    	for (int index = 0; index < comments.getLength(); index++) {
		    		// skip empty comment
		    		if (comments.item(index).getFirstChild() == null) continue;
		    		
		    		String content = web.HTML.htmlToText(comments.item(index).getFirstChild().getTextContent());
		    		content = preprocess(content, contractionDictionary); 
		    		
		    		List<String> sentences = null;
		    		
			    	if (delim == null) {
			    		sentences = NlpUtils.splitSentences(content);
			    	}
			    	else {
			    		String delimited = content.replaceAll("(" + delim + ")", "$1DELIM");
			    		sentences = Arrays.asList(delimited.split("DELIM"));		    		
			    	}
		    		
		    		comments.item(index).removeChild(comments.item(index).getFirstChild());
		    		
			    	for (String sentence : sentences) {
			    		sentence = postprocess(sentence);
			    		if (sentence.matches("\\s*")) continue;
			    		Element sElement = entry.createElement("sentence");
			    		Text s = entry.createTextNode("sentence");
						s.setTextContent(sentence);
						sElement.appendChild(s);
						comments.item(index).appendChild(sElement);					
			    	}
		    	}
		    	
		    	// write to file
		    	new File(outputFileName).getParentFile().mkdir();
		    	FileOutputStream xml = new FileOutputStream(outputFileName);
				Writer writer = new Writer();
				writer.setOutput(xml,"UTF-8");
				writer.write(entry);
		} catch (Exception e) {
			System.err.println("[XMLUtilities.addSentenceTagsToBlogs] Error writing to file " + outputFileName + ": " + e);
			e.printStackTrace();
		}
		return outputFileName;
	}
	
	public static void removeInvalidXML(String file) {
		List<String> sentences = Utils.readLines(file);
		List<String> stripped = new ArrayList<String>();
		
		for (String sentence : sentences) {
			stripped.add(stripNonValidXMLCharacters(sentence));
		}
		
		// write to file
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(file));

	    	for (String sentence : stripped) {
	    		out.write(sentence + "\n");
	    	}
			out.close();
		} catch (Exception e) {
			System.err.println("[XMLUtilities.writeSentenceToTxtFile] Error writing to file " + file + ":" + e);
		}
	}
	
	 /**
     * This method ensures that the output String has only
     * valid XML unicode characters as specified by the
     * XML 1.0 standard. For reference, please see
     * <a href=”http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char”>the
     * standard</a>. This method will return an empty
     * String if the input is null or empty.
     *
     * @param in The String whose non-valid characters we want to remove.
     * @return The in String, stripped of non-valid characters.
     */
    public static String stripNonValidXMLCharacters(String in) {
        StringBuffer out = new StringBuffer(); // Used to hold the output.
        char current; // Used to reference the current character.

        if (in == null || ("".equals(in))) return ""; // vacancy test.
        for (int i = 0; i < in.length(); i++) {
            current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
            if ((current == 0x9) ||
                (current == 0xA) ||
                (current == 0xD) ||
                ((current >= 0x20) && (current <= 0xD7FF)) ||
                ((current >= 0xE000) && (current <= 0xFFFD)) ||
                ((current >= 0x10000) && (current <= 0x10FFFF)))
                out.append(current);
        }
        return out.toString();
    } 
	
	private static String preprocess(String content, Contractions contractionsDictionary) {
		
		try {
			content = new SwapEmoticons().swapEmoticons(content, true);
			content = contractionsDictionary.swapContractions(content);
	    	// to keep proper brackets
	    	content = convertBrackets(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}
	
	private static String postprocess(String content) {
		try {
			content = new SwapEmoticons().swapEmoticons(content,false);
			content = revertBrackets(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}
		
	private static String convertBrackets(String string) {
		string = string.replaceAll("\\(", "-RRP-");
		string = string.replaceAll("\\[", "-RRB-");
		string = string.replaceAll("\\{", "-RRC-");
		string = string.replaceAll("<", "-RRL-");
		string = string.replaceAll("\\)", "-LRP-");
		string = string.replaceAll("]", "-LRB-");
		string = string.replaceAll("}", "-LRC-");
		string = string.replaceAll(">", "-LRL-");
		return string;
	}
	
	private static String revertBrackets(String string) {
		string = string.replaceAll("-( )?RRP( )?-", "(");
		string = string.replaceAll("-( )?RRB( )?-", "[");
		string = string.replaceAll("-( )?RRC( )?-", "{");
		string = string.replaceAll("-( )?RRL( )?-", "<");
		string = string.replaceAll("-( )?LRP( )?-", ")");
		string = string.replaceAll("-( )?LRB( )?-", "]");
		string = string.replaceAll("-( )?LRC( )?-", "}");
		string = string.replaceAll("-( )?LRL( )?-", ">");
		return string;
	}
}
