package blog;
/**
 * 
 */


import java.util.*;
import java.util.regex.*;

import org.w3c.dom.*;

/**
 * @author sara
 *
 */
public class Sentence {
	
	String [] words;
	String [] pos;
	ArrayList<String> dependencies;
	String sentenceText;  // just sentence
	String sentencePOS; // just POS
	String sentence; // sentence + POS
	
	public static final String POS_TAGS = "CC|CD|DT|EX|FW|IN|JJ|JJ[RS]|LS|MD|NN|NN[PS]+|NNPS|PDT|POS|PRP|PRP\\$|RB|RBR|RBS|RP|SYM|TO|UH|VB|VB[GNPZD]|WDT|WP|WP\\$|WRB|\\p{Punct}";

	public Sentence(String sentenceIn, String posIn, ArrayList<String> dependenciesIn) {
		this(sentenceIn, posIn, dependenciesIn, -1);
	}
	
	/**
	 * Create a sentence which will include a string and possibly part-of-speech and dependencies
	 * @param sentenceIn
	 * @param posIn
	 * @param dependenciesIn
	 */
	public Sentence(String sentenceIn, String posIn, ArrayList<String> dependenciesIn, int sentenceSize) {
		if (sentenceIn == null || sentenceIn.equals("")) return;

		if (sentenceIn.length() > sentenceSize && sentenceSize != -1) return;
		
		sentenceText = "";
		sentencePOS = "";
		String posSplit = sentenceIn.split("/").length > sentenceIn.split("_").length ? "/" : "_";
		
		
		// check if pos is available
		if (posIn != null) {

			sentence = posIn;
			sentence = sentence.trim().replaceAll("\\s+", " ");
			
			String [] data = posIn.split(" ");
			
			words = new String[data.length];
			pos = new String[data.length];
			dependencies = dependenciesIn;
			
			int position = 0;
			words[position] = "";
			 
			for (int index = 0; index < data.length; index++) {
				if (data[index].equals("")) continue;
				
				int split = data[index].lastIndexOf("/");
				if (data[index].lastIndexOf("_") > split)
					split = data[index].lastIndexOf("_");
				
				if (split == -1 || !data[index].substring(split+1).matches("(" + Sentence.POS_TAGS + ")")) {// || data[index].length() - split > 5) {
					words[position] += data[index] + " ";					
					sentenceText += data[index] + " ";
				}
				else {
					words[position] += data[index].substring(0,split);
					words[position] = web.HTML.htmlToText(words[position]);
					if (words[position].matches("-RRB-")) 
						words[position] = ")";
					else if (words[position].matches("-LRB-")) {
						words[position] = "(";
					}
					if (!words[position].equals("")) {
						pos[position] = data[index].substring(split + 1);
						sentencePOS += pos[position] + " ";
						sentenceText += words[position] + " ";
						position++;
						if (position < words.length) words[position] = "";
					}
				}
			}
			//words = sentenceText.split("\\p{Space}+");
		}
		else if (sentenceIn.split(posSplit).length >= sentenceIn.split(" ").length) {
			
			sentence = sentenceIn;			
			sentence = sentence.trim().replaceAll("\\s+", " ");
			String [] data = sentence.split(" ");
			
			words = new String[data.length];
			pos = new String[data.length];
			dependencies = dependenciesIn;

			int position = 0;
			words[position] = "";
			
			for (int index = 0; index < data.length; index++) {
				if (data[index].equals("")) continue;
				
				int split = data[index].lastIndexOf("/");
				if (data[index].lastIndexOf("_") > split)
					split = data[index].lastIndexOf("_");
				
				// make sure its a POS
				if (split == -1 || !data[index].substring(split+1).matches("[CDEFIJLMNPRSTUVW][BCDHJNOPRSTWXY][DSPMGNZRTB\\$]*")) { //data[index].length() - split > 5) {
					words[index] = data[index];
					sentenceText += data[index];  
				}
				else {
					words[index] = data[index].substring(0,split);
					pos[index] = data[index].substring(split+1);
					
					if (words[index].matches("-RRB-")) 
						words[index] = ")";
					else if (words[index].matches("-LRB-"))
						words[index] = "(";
					
					sentenceText += words[index] + " ";
					sentencePOS += pos[index] + " ";
				}
			}
			
		}
		
		sentenceText = web.HTML.htmlToText(sentenceIn);
		words = sentenceText.split("\\p{Space}+");
		
		sentenceText = sentenceText.trim();
		sentencePOS = sentencePOS.trim();
	}

	public static Sentence processSentence(String sentenceIn, int size) {
		return new Sentence(sentenceIn, null, null, size);
	}

	
	public static Sentence processSentence(String sentenceIn) {
		return new Sentence(sentenceIn, null, null);
	}
	
	public static Sentence processSentence(Element sentenceIn) {
		return processSentence(sentenceIn, -1);
	}
	
	/**
	 * Process a sentence from XML format
	 * @param sentenceIn
	 * @return
	 */
	public static Sentence processSentence(Element sentenceIn, int sentenceSize) {

		try {
			
			ArrayList<String> dependencies = new ArrayList<String>();
			
			if (sentenceIn == null || sentenceIn.getFirstChild() == null) return null;
			
			if (sentenceIn.getElementsByTagName("string").getLength() == 0) {
				String string = sentenceIn.getFirstChild().getTextContent();
	    		return new Sentence(string,null,dependencies);
			}
			
	    	Node stringTag = sentenceIn.getElementsByTagName("string").item(0).getFirstChild();
	    	String string = stringTag != null ? stringTag.getNodeValue() : "";
	    	
	    	Node posTag = sentenceIn.getElementsByTagName("pos").item(0).getFirstChild();
	    	String pos = posTag != null ? posTag.getNodeValue() : "";
	    	
	    	Node dependenciesTag = sentenceIn.getElementsByTagName("dependencies").item(0).getFirstChild();
	    	
	    	Pattern p = Pattern.compile("[a-z]*\\(\\p{Graph}*-[0-9]*, \\p{Graph}*-[0-9]*\\)");
	    	Matcher m = p.matcher(dependenciesTag != null ? dependenciesTag.getNodeValue() : "");
	    	
	    	while (m.find()) {
	    		dependencies.add(m.group());
	    	}
			
			if (string == null || string.equals("")) return null;
			if (string.length() > sentenceSize && sentenceSize != -1) return null;
	    	return new Sentence(string,pos,dependencies, sentenceSize);
	    	
		} catch (Exception e) {
			System.err.println("Error occurred in Entry.processEntry: " + e);
			e.printStackTrace();
		}

		return null;
	}
	
	public String getText() {
		return sentenceText;
	}
	
	public String getSentencePOS() {
		return sentencePOS;
	}
	
	public String getSentence() {
		return sentence;
	}
	
	public int getLength() {
		return sentenceText.length();
	}
	
	public String getWord(int index) {
		if (words == null) return null;
		return words[index];
	}
	
	public int getNumWords() {
		return words.length;
	}
	
	public String getPOS(int index) {
		if (pos == null) return null;
		
		return pos[index];
	}
	
	public ArrayList<String> getDependencies() {
		return dependencies;
	}
	
	public String toString() {
		return sentence + ", " + sentenceText + ", " + sentencePOS + ", " + dependencies.toString();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
