/**
 * 
 */
package blog;

import java.util.*;

/**
 * @author sara
 *
 */
public abstract class Entry {

	protected String url;
	protected Date date;
	protected String entry = "";
	protected String title = "";
	protected HashMap<String,Comment> comments;
	
	public Entry(String urlIn, Date dateIn, String entryIn, String titleIn) {
		url = urlIn;
		date = dateIn;
		entry = entryIn;
		comments = new HashMap<String,Comment>();
		title = titleIn;
	}
	
	public String getURL() {
		return url;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getEntry() {
		return entry;
	}
	
	public abstract String getEntryText();
	
	public void addComment(Comment comment) {
		comments.put(comment.getURL().toString(), comment);
	}
	
	public Comment getComment(String url) {
		return comments.get(url);
	}
	
	public Iterator<String> getComments() {
		return comments.keySet().iterator();
	}
	
	public Set<String> getCommentKeys() {
		return comments.keySet();
	}
	
	public int getNumComments() {
		return comments.size();
	}
	
	public abstract Object[] getSortedComments();
	public abstract String getSortedCommentsToString();
	public abstract List<Sentence> getSentences();
	
	public String toString() {
		String string = "url: " + url +
				", date: " + date +
				", entry: " + entry + "\n";
		
		Iterator<String> iterator = comments.keySet().iterator();
		while (iterator.hasNext()) {
			string += "comment: " + comments.get(iterator.next()).toString() + "\n";
		}
		
		return string;
	}
}