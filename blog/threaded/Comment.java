/**
 * 
 */
package blog.threaded;

import java.util.Date;
import java.util.List;

import blog.Sentence;

/**
 * @author sara
 *
 */
public abstract class Comment extends blog.Comment {

	protected String parentURL;
	protected String parentID;
	
	public Comment(String commentUrlIn, String usernameIn, Date dateIn,
			String commentIn, String commentID, String parentUrlIn, String parentIdIn) {
		super(commentUrlIn, usernameIn, dateIn, commentIn, commentID);

		parentURL = parentUrlIn;
		parentID = parentIdIn;
	}
	
	public String getParentId() {
		return parentID;
	}
	
	public String getParentURL() {
		return parentURL;
	}
	
	public String toString() {
		return "username: " + username + " (" + id + ", " + commentUrl + "):" +
				"\n comment: " + comment.substring(0,comment.length() > 80 ? 80 : comment.length()) +
				",\n parent id: " + parentID + ", parent url: " +  parentURL +
				",\n date: " + date;
	}
	
	public abstract List<Sentence> getSentences();
}
