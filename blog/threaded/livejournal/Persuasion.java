package blog.threaded.livejournal;

import java.util.*;
import org.w3c.dom.*;
import processing.NlpUtils;

/**
 * Contains all the persuasion elements found in a blog entry
 * @author sara
 *
 */
public class Persuasion {

	String claim;
	int id;
	Hashtable<String,ArrayList<String>> support;
	List<String> claimSentences;
	
	public Persuasion(String claimIn, int idIn) {
		claim = claimIn;
		id = idIn;
		
		claimSentences = NlpUtils.splitSentences(claim);
		
		support = new Hashtable<String,ArrayList<String>>();
	}
	
	public static HashSet<String> getPersuasionText(Element entry) {
		Hashtable<Integer,Persuasion> persuasions = processEntryForPersuasion(entry);
		
		Iterator<Integer> it = persuasions.keySet().iterator();
		
		HashSet<String> persuasionText = new HashSet<String>();
		
		while (it.hasNext()) {
			Object key = it.next();
			persuasionText.add(persuasions.get(key).getClaim());
		}
		return persuasionText;
	}
	
	public static Hashtable<Integer,Persuasion> processEntryForPersuasion(Element entry) {
	
		Hashtable<Integer,Persuasion> persuasions = new Hashtable<Integer,Persuasion>();
		
		NodeList claims = entry.getElementsByTagName("claim");

		// setup persuasion by getting all claims in the entry *required
		for (int index = 0; index < claims.getLength(); index++) {
			
			Element claim = (Element)claims.item(index);
			
			Integer id = Integer.valueOf(claim.getAttribute("id"));
			String text = claim.getFirstChild().getNodeValue();
			
			persuasions.put(id,new Persuasion(text, id));
		}

		// add support - must contain at least one of these!
		Persuasion.processSupport(entry.getElementsByTagName("justification"),persuasions,"justification");
		Persuasion.processSupport(entry.getElementsByTagName("grounding"),persuasions,"grounding");
		Persuasion.processSupport(entry.getElementsByTagName("reiteration"),persuasions,"reiteration");
		// this support is optional
		Persuasion.processSupport(entry.getElementsByTagName("elaboration"),persuasions,"elaboration");
		
		return persuasions;
	}
	
	private static void processSupport(NodeList supportIn, Hashtable<Integer,Persuasion> persuasions, String type) {
		
		for (int index = 0; index < supportIn.getLength(); index++) {
		
			Element s = (Element)supportIn.item(index);
			
			Integer id = Integer.valueOf(s.getAttribute("id"));
			String text = s.getFirstChild().getNodeValue();
			
			persuasions.get(id).addSupport(type,text);
		}
	}
	
	public void addSupport(String type, String text) {
		ArrayList<String> supportText = support.get(type);
		
		if (supportText == null) supportText = new ArrayList<String>();
		
		supportText.add(text);
		support.put(type, supportText);
	}
	
	public String getClaim() {
		return claim;
	}


	public List<String> getClaimAsSentences() {
		return claimSentences;
	}
	
	public int getClaimID() {
		return id;
	}

	public ArrayList<String> getSupport(String type) {
		return support.get(type);
	}
	
	public boolean compareTo(String predictionText) {
		
		for (String sentence : claimSentences) {
			if (sentence.indexOf(predictionText) >= 0 || predictionText.indexOf(sentence) >= 0)
				return true;
		}
		
		return false;
	}
	
	public String toString() {
		String string = "";
		
		string += "claim " + this.id + ": " + this.claim + "\n";
		
		Iterator<String> supportIterator = support.keySet().iterator();
		
		while (supportIterator.hasNext()) {
			String type = (String)supportIterator.next();
			string += type + ": " + support.get(type) + "\n";
		}
		
		return string;
	}
	
	public static void main(String [] args) {
		
		String entry = "/proj/nlp/users/sara/corpora/blogs/livejournal/persuasion-training/annotated-entries-clean/badmagic/560950.xml";
		
		Blog b = Blog.processBlog(entry);
		Entry e = (Entry)b.getEntry((String)b.getEntries().next());
		Hashtable<Integer,Persuasion> persuasions = Persuasion.processEntryForPersuasion(e.getEntryElement());
		
		Iterator<Integer> i = persuasions.keySet().iterator();
		
		while (i.hasNext()) 
			System.out.println(persuasions.get(i.next()).toString() + "\n");
		
	}
}
