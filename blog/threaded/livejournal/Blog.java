/**
 * 
 */
package blog.threaded.livejournal;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.net.URL;

import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import dom.Writer;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.*;

import collocation.*;
import processing.*;

/**
 * @author sara
 *
 */
public class Blog extends blog.Blog {

		private String fullName;
		private int numPosts;
		private String bio;
		private Hashtable<String,String> interests;
		private String outputDirectory;
		private String title;
		private String description;
		private String type;
		private String image;
		private String id;
		private Date lastBuildDate;
		
        /**
         * @param usernameIn
         * @param languageInisr.close
         * @param dobIn
         * @param countryIn
         * @param cityIn
         * @param homepageIn
         */
        public Blog(String outputDirectoryIn, String usernameIn, String languageIn, Date dobIn,
                        String countryIn, String cityIn, URL homepageIn, String fullNameIn, int numPostsIn, String bioIn,
                        String politicalParty, String politicalView, String religion) {
                super(usernameIn, languageIn, dobIn, countryIn, cityIn, homepageIn, politicalParty, politicalView, religion);
                fullName = fullNameIn;
                numPosts = numPostsIn;
                bio = bioIn;
                interests = new Hashtable<String,String>();
                outputDirectory = outputDirectoryIn;
        }
        
        public static Blog processBlog(String outputDirectory, String username) {
        	return Blog.processBlog(outputDirectory, username, -1, -1, -1, -1);
        }

        public static Blog processBlog(String outputDirectory, String username, int numEntries, int numComments, int maxSentenceSize, int numWords) {
        	File directory = new File(outputDirectory + "/entries/" + username + "/");
        	return Blog.processBlog(outputDirectory, outputDirectory + "/profiles/" + username +".xml", directory.listFiles(), numEntries, numComments, maxSentenceSize, numWords);
        }

        
        public static Blog processBlog(String profilesDirectory, String entriesDirectory, String username) {
        	return Blog.processBlog(profilesDirectory, entriesDirectory, username, -1, -1, -1, -1);
        }
        
        public static Blog processBlog(String profilesDirectory, String entriesDirectory, String username, int numEntries, int numComments, int maxSentenceSize, int numWords) {
        	File directory = entriesDirectory != null ? new File(entriesDirectory + "/entries/" + username + "/") : null;
        	return Blog.processBlog(profilesDirectory, profilesDirectory + "/profiles/" + username +".xml", entriesDirectory == null ? null : directory.listFiles(), numEntries, numComments, maxSentenceSize, numWords);
        }

        public static Blog processBlog(String entry) {
        	return Blog.processBlog(entry, -1, -1, -1, -1);
        }
        
        public static Blog processBlog(String entry, int numEntries, int numComments, int maxSentenceSize, int numWords) {
        	File [] directory = {new File(entry)};
        	return Blog.processBlog(null, null, directory, numEntries, numComments, maxSentenceSize, numWords);
        }
        
        public static void addPOS(String outputDirectory, String username, String taggerIn) {
        	Blog b = Blog.processBlog(outputDirectory, username);

        	if (b.hasPOS()) {
        		System.err.println("[Blog.addPOS] blog entries for " + username + " already contains POS.");
        		return;
        	}
        	
        	// only add pos if its english
        	if (b.getCountry() == null || (!b.getCountry().equals("US") && !b.getCountry().equals("CA") && !b.getCountry().equals("UK") && !b.getCountry().equals("AU"))) {
        		System.err.println("[Blog.addPOS] invalid country " + b.getCountry() + " for " + username);
        		return;
        	}
        	
        	File [] entriesFilesIn = new File(outputDirectory + "/entries/" + username + "/").listFiles();
        	
        	DOMParser parser = new DOMParser();
        	MaxentTagger tagger = null;
        	
    		try {
    			tagger = new MaxentTagger(taggerIn);
    			parser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return;
    		}
    		
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	try {
            		InputStreamReader isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		parser.parse(in);
            		
            		Document blog = parser.getDocument();
            		
            		// Each entry            		
    	        	NodeList entries = blog.getElementsByTagName("item");
    	        	
    	        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
    	        		NodeList des = ((Element)entries.item(entriesIndex)).getElementsByTagName("description");
    	        		
    	        		if (des == null) continue;
    	        		Element descriptionTag = (Element)des.item(0);
    	    	    	
    	        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null ? 
    	        				descriptionTag.getFirstChild().getNodeValue() : "";
    	        		
    	        		String [] paragraphs = description.split("&lt;br /&gt;");
    	        		
    	        		String taggedDescription = "";
    	        		
    	        		for (int i = 0; i < paragraphs.length; i++) {
    	        			if (paragraphs[i].length() > 20000) {
    	        				
    	        				String [] sentences = paragraphs[i].split("\\. ");
    	        				
    	        				for (int j = 0; j < sentences.length; j++) {
    	        					taggedDescription += tagger.tagString(web.HTML.stripTags(sentences[j]) + ".");
    	        				}
    	        				
    	        				taggedDescription += "\n";
    	        			}
    	        			else {
    	        				taggedDescription += tagger.tagString(web.HTML.stripTags(paragraphs[i]) + "\n");
    	        			}
    	        		}
    	        		if (taggedDescription.length() > 0) 
    	        			descriptionTag.getFirstChild().setNodeValue(taggedDescription);

    	        		// Each comment
    	        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
    	        		
    	        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
    	        			Element commentTag = (Element)comments.item(commentsIndex);
    	        			
    	        			String comment = commentTag != null && commentTag.getFirstChild() != null ?
    	        					tagger.tagString(web.HTML.stripTags(commentTag.getFirstChild().getNodeValue())) : "";
    	        					
    	        			if (comment.length() > 0)		
    	        				commentTag.getFirstChild().setNodeValue(comment);		
    	        		}
    	        	}
    	        	
    	        	isr.close();
    	        	// overwrite to store pos version of file since it includes text
    	        	System.err.println("[Blog.addPOS] writing to " + entriesFilesIn[index]);
    	        	FileOutputStream xml = new FileOutputStream(entriesFilesIn[index]);
    				Writer writer = new Writer();
    				writer.setOutput(xml,"UTF-8");
    				writer.write(blog);
    	        	
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.processBlog for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	}
        	}
        }
        
        public static void addPOSFlatEntry(String inputFile, String taggerIn) {

        	new File(new File(inputFile).getParent() + "-pos/").mkdir();
        	File outputFile = new File(new File(inputFile).getParent() + "-pos/" + new File(inputFile).getName());
        	
        	if (outputFile.exists()) {
        		System.err.println("[Blog.addPOSFlatEntry] " + inputFile + " already contains POS.");
        		return;
        	}
        
        	DOMParser parser = new DOMParser();
        	MaxentTagger tagger = null;
        	
    		try {
    			tagger = new MaxentTagger(taggerIn);
    			parser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return;
    		}
    		
        	try {
        		InputStreamReader isr = new InputStreamReader(new FileInputStream(inputFile),"UTF-8");
        		InputSource in = new InputSource(isr);
        		parser.parse(in);
        		
        		Document blog = parser.getDocument();
        		
        		// Each entry            		
	        	NodeList entries = blog.getElementsByTagName("item");
	        	
	        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
	        		NodeList des = ((Element)entries.item(entriesIndex)).getElementsByTagName("description");
	        		
	        		if (des == null) continue;
	        		Element descriptionTag = (Element)des.item(0);
	    	    	
	        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null ? 
	        				descriptionTag.getFirstChild().getNodeValue() : "";
	        		
	        		String [] paragraphs = description.split("&lt;br /&gt;");
	        		
	        		String taggedDescription = "";
	        		
	        		for (int i = 0; i < paragraphs.length; i++) {
	        			if (paragraphs[i].length() > 20000) {
	        				
	        				String [] sentences = paragraphs[i].split("\\. ");
	        				
	        				for (int j = 0; j < sentences.length; j++) {
	        					taggedDescription += tagger.tagString(web.HTML.stripTags(sentences[j]) + ".");
	        				}
	        				
	        				taggedDescription += "\n";
	        			}
	        			else {
	        				taggedDescription += tagger.tagString(web.HTML.stripTags(paragraphs[i]) + "\n");
	        			}
	        		}
	        		if (taggedDescription.length() > 0) 
	        			descriptionTag.getFirstChild().setNodeValue(taggedDescription);

	        		// Each comment
	        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
	        		
	        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
	        			Element commentTag = (Element)comments.item(commentsIndex);
	        			
	        			String comment = commentTag != null && commentTag.getFirstChild() != null ?
	        					tagger.tagString(web.HTML.stripTags(commentTag.getFirstChild().getNodeValue())) : "";
	        					
	        			if (comment.length() > 0)		
	        				commentTag.getFirstChild().setNodeValue(comment);		
	        		}
	        	}
	        	
	        	isr.close();
	        	// overwrite to store pos version of file since it includes text
	        	System.err.println("[Blog.addPOS] writing to " + outputFile);
	        	FileOutputStream xml = new FileOutputStream(outputFile);
				Writer writer = new Writer();
				writer.setOutput(xml,"UTF-8");
				writer.write(blog);
	        	
        	} catch (Exception e) {
        		System.err.println("Error occurred in Blog.processBlog for blog entries file " + outputFile + ": " + e);
        		e.printStackTrace();
        	}
        }
        
        public static void removePOS(String inputDirectory, String outputDirectory, String username) {
        	Blog b = Blog.processBlog(inputDirectory, username);
        	
       		File [] entriesFilesIn = new File(inputDirectory + "/entries/" + username + "/").listFiles();
    		
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

            	// only add pos if its english
            	if (b.getCountry() == null || (!b.getCountry().equals("US") && !b.getCountry().equals("CA") && !b.getCountry().equals("UK") && !b.getCountry().equals("AU"))) {
            		System.err.println("[Blog.removePOS] invalid country " + b.getCountry() + " for " + username  + " just moving file over.");
        			File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
        			outputFile.getParentFile().mkdirs();
        			entriesFilesIn[index].renameTo(outputFile);
        			entriesFilesIn[index].getParentFile().delete();
        			continue;
        		}
        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		if (entriesFilesIn[index].toString().endsWith("~")) {
        			entriesFilesIn[index].delete();
        			entriesFilesIn[index] = null;
        			System.err.println("[Blog.removePOS] deleting file " + entriesFilesIn[index]);
        			continue;
        		}
        		
            	DOMParser parser = new DOMParser();
            	
        		try {
        			parser.setFeature("http://xml.org/sax/features/namespaces", false);
        		} catch (Exception e) {
        			System.err.println("Error occurred in Blog.processBlog " + e);
        			return;
        		}
        		
            	try {
            		InputStreamReader  isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		parser.parse(in);
            		
            		Document blog = parser.getDocument();
            		
            		// Each entry            		
    	        	NodeList entries = blog.getElementsByTagName("item");
    	        	
    	        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
    	        		Element descriptionTag = (Element)((Element)entries.item(entriesIndex)).getElementsByTagName("description").item(0);
    	    	    	
    	        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null ? 
    	        				descriptionTag.getFirstChild().getNodeValue() : "";
    	        	
    	        		String [] paragraphs = description.split("&lt;br /&gt;");
    	        		
    	        		String taggedDescription = "";
    	        		
    	        		for (int i = 0; i < paragraphs.length; i++) {

   	        				String [] sentences = paragraphs[i].split("\\. ");
    	        				
	        				for (int j = 0; j < sentences.length; j++) {
	        					taggedDescription += sentences[j].replaceAll("/\\p{Graph}* "," ");
	        				}
	        				
	        				taggedDescription += "\n";
    	        		}

    	        		if (taggedDescription.length() > 0) 
    	        			descriptionTag.getFirstChild().setNodeValue(taggedDescription);

    	        		// Each comment
    	        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
    	        		
    	        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
    	        			Element commentTag = (Element)comments.item(commentsIndex);
    	        			
    	        			String comment = commentTag != null && commentTag.getFirstChild() != null ?
    	        					web.HTML.stripTags(commentTag.getFirstChild().getNodeValue()).replaceAll("/\\p{Graph}* "," ") : "";
    	        					
    	        			if (comment.length() > 0)		
    	        				commentTag.getFirstChild().setNodeValue(comment);		
    	        		}
    	        	}
    	        	
    	        	isr.close();
    	        	// overwrite to store pos version of file since it includes text
    	        	File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
    	        	outputFile.getParentFile().mkdirs();
    	        	System.err.println("[Blog.removePOS] writing to " + outputFile);
    	        	FileOutputStream xml = new FileOutputStream(outputFile);
    				Writer writer = new Writer();
    				writer.setOutput(xml,"UTF-8");
    				writer.write(blog);
    	        	
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.processBlog for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	}
        	}
        }
        
        private static void removeTag(Document d, String t) {
        	NodeList tags = d.getElementsByTagName(t);
        	
        	for (int i = 0; i < tags.getLength(); i++) {
	    		Element tag = (Element)d.getElementsByTagName(t).item(i);
	    		
	        	if (tag != null) {
	        		tag.getParentNode().removeChild(tag);
	        	}
        	}
        }
        
        /**
         * Remove personal information from the profiles and blogs
         * @param inputDirectory
         * @param outputDirectory
         * @param username
         */
        public static void removePersonalInformation(String inputDirectory, String outputDirectory, String username) {
        	
        	// remove personal info and save file
    		
        	DOMParser xmlParser = new DOMParser();
        	
    		try {
    			xmlParser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return;
    		}
    		
    		String profileFile = inputDirectory + "/profiles/" + username + ".xml";
    		
    		try {
    			InputStreamReader isr = new InputStreamReader(new FileInputStream(profileFile),"UTF-8");
	    		InputSource in = new InputSource(isr);
	    		xmlParser.parse(in);
	    		
	    		Document profile = xmlParser.getDocument();
	    		
	    		Blog.removeTag(profile,"foaf:name");
	    		Blog.removeTag(profile,"foaf:icqChatID");
	    		Blog.removeTag(profile,"foaf:aimChatID");
	    		Blog.removeTag(profile,"foaf:jabberID");
	    		Blog.removeTag(profile,"foaf:msnChatID");
	    		Blog.removeTag(profile,"foaf:yahooChatID");
	    		Blog.removeTag(profile,"foaf:member_name");
	    		isr.close();
	        	//profile.removeChild(profile.getElementById("foaf:icqChatID"));
        	
	        	// store syntax dependencies version of file
				File outputFile = new File(profileFile.replaceFirst(inputDirectory,outputDirectory));
				outputFile.getParentFile().mkdirs();
	        	System.err.println("[Blog.removePersonalInformation] writing to " + outputFile);
	        	FileOutputStream xml = new FileOutputStream(outputFile);
				Writer writer = new Writer();
				writer.setOutput(xml,"UTF-8");
				writer.write(profile);
        	} catch (Exception e) {
        		System.err.println("Error occurred in Blog.removePersonalInformation for blog " + profileFile + ": " + e);
        		e.printStackTrace();
        	} 
        	
        	// copy entries
        	File [] entriesFilesIn = new File(inputDirectory + "/entries/" + username + "/").listFiles();
        	
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	try {
            		
            		InputStream ins = new FileInputStream(entriesFilesIn[index]);
            		File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
            		outputFile.getParentFile().mkdirs();
                    OutputStream out = new FileOutputStream(outputFile);
                
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = ins.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    ins.close();
                    out.close();
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.removePersonalInformation for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	} 
        	}
        }

        /**
         * Add syntax dependencies and POS
         * @param inputDirectory
         * @param outputDirectory
         * @param username
         * @param lexicalizedParser
         */
        public static void addSyntaxDependencies(String inputDirectory, String outputDirectory, String username, MaxentTagger tagger, LexicalizedParser lexicalizedParser, boolean updateComments, SwapEmoticons swapper) {
        	
        	File [] entriesFilesIn = new File(inputDirectory + "/entries/" + username + "/").listFiles();
        	
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	DOMParser xmlParser = new DOMParser();
            	
        		try {
        			xmlParser.setFeature("http://xml.org/sax/features/namespaces", false);
        		} catch (Exception e) {
        			System.err.println("Error occurred in Blog.processBlog " + e);
        			return;
        		}
        		
            	try {
            		InputStreamReader isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		xmlParser.parse(in);
            		
            		// update entries with dependencies and POS
            		Document blog = Blog.updateEntries(xmlParser.getDocument(), xmlParser, tagger, lexicalizedParser, updateComments, swapper);
            		isr.close();
    	        	// store syntax dependencies version of file
    				File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
    	        	outputFile.getParentFile().mkdirs();
    	        	System.err.println("[Blog.addSyntaxDependencies] writing to " + outputFile);
    	        	FileOutputStream xml = new FileOutputStream(outputFile);
    				Writer writer = new Writer();
    				writer.setOutput(xml,"UTF-8");
    				writer.write(blog);
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.addSyntaxDependencies for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	}
        	}
        }
        
        /**
         * Add syntax dependencies and POS
         * @param inputDirectory
         * @param outputDirectory
         * @param username
         * @param lexicalizedParser
         */
        public static void addSyntaxDependencies(String file, String oFile, MaxentTagger tagger, LexicalizedParser lexicalizedParser, boolean updateComments, SwapEmoticons swapper) {
        	
        	DOMParser xmlParser = new DOMParser();
        	
    		try {
    			xmlParser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return;
    		}
    		
        	try {
        		InputStreamReader isr = new InputStreamReader(new FileInputStream(file),"UTF-8");
        		InputSource in = new InputSource(isr);
        		xmlParser.parse(in);
        		
        		// update entries with dependencies and POS
        		Document blog = Blog.updateEntries(xmlParser.getDocument(), xmlParser, tagger, lexicalizedParser, updateComments, swapper);
        		isr.close();
	        	// store syntax dependencies version of file
				File outputFile = new File(oFile);
	        	outputFile.getParentFile().mkdirs();
	        	System.err.println("[Blog.addSyntaxDependencies] writing to " + outputFile);
	        	FileOutputStream xml = new FileOutputStream(outputFile);
				Writer writer = new Writer();
				writer.setOutput(xml,"UTF-8");
				writer.write(blog);
        	} catch (Exception e) {
        		System.err.println("Error occurred in Blog.addSyntaxDependencies for blog entries file " + file + ": " + e);
        		e.printStackTrace();
        	}
        }

        private static Document updateEntries(Document blog, DOMParser xmlParser, MaxentTagger tagger, LexicalizedParser lexicalizedParser, boolean updateComments, SwapEmoticons swapper) throws Exception {

        	// to be used later
		    TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
      	
    		// Each entry            		
        	NodeList entries = blog.getElementsByTagName("item");
        	
        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
        		NodeList des = ((Element)entries.item(entriesIndex)).getElementsByTagName("description");
        		
        		if (des == null || des.getLength() == 0) continue;
        		Element descriptionTag = (Element)des.item(0);
    	    	
        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null 
        				&& descriptionTag.getFirstChild().getNodeValue() != null ? 
        				descriptionTag.getFirstChild().getNodeValue() : "";
        		if(descriptionTag != null && descriptionTag.getFirstChild() != null)
        			descriptionTag.removeChild(descriptionTag.getFirstChild());
        		
        		description = description.replaceAll("&nbsp;|&amp;nbsp;"," ");
        		description = description.replaceAll("&lt;br /&gt;", "<br />");
        		String [] paragraphs = description.split("(<br />)+");
        		
        		int id = 1;
        		
        		for (int i = 0; i < paragraphs.length; i++) {
        			
        			paragraphs[i] = web.HTML.htmlToText(paragraphs[i]); 
       				
        			//List<String> sentences = processing.NlpUtils.splitSentences(paragraphs[i]);
        			List<String> sentences = processing.StringProcessing.SentenceSplitter(paragraphs[i]);
        				
    				for (int j = 0; j < sentences.size(); j++) {
    					
    					String s = sentences.get(j);
    					
    					// remove all link's and image's for parsing
    					s = s.replaceAll("LLLINKKK|IIIMAGEEE", "");
    					
    					if (s.length() == 0 || s.trim().length() == 0) continue;
    					
    					if (s.length() > 100) { 
    						
							int space = s.indexOf(" ",90);
							if (space >= 0 && space < 100)
								s = s.substring(0, s.indexOf(" ", 90));
							else if (s.length() > 100) {
								s = s.substring(0, 100);
								if (s.lastIndexOf(" ") > 0)
									s = s.substring(0,s.lastIndexOf(" "));
								s = "";
							}
						}	
    					// convert all emoticons so that we don't lose them
    					s = swapper.swapEmoticons(s, true);
    					
    					if (s.length() == 0 || s.trim().length() == 0) continue;
						
    					Tree parse = null;
    					
    					try {
    						parse = (Tree) lexicalizedParser.parse(s);
    					} catch (Exception e) {
    						System.err.println("[Blog.updateEntries] error parsing tree");
    						continue;
    					}
        					
    					GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
    				    
    				    String dependencies = gs.typedDependencies().toString();
    				    
    				    String taggedSentence = tagger.tagString(s);
    				    
    				    // put the emoticons back in - we don't need to do this. we can always refer to the codes later!
    				    //taggedSentence = swapper.swapEmoticons(taggedSentence, false);
    				    
    				    Element sentence = blog.createElement("sentence");
    				    sentence.setAttribute("id", new Integer(id).toString());
    				    
    				    Element text = blog.createElement("string");
    				    text.setTextContent(sentences.get(j));
			    
    				    Element pos = blog.createElement("pos");
    				    pos.setTextContent(taggedSentence);
    				    
    				    Element deps = blog.createElement("dependencies");
    				    deps.setTextContent(dependencies.substring(1, dependencies.length()-1));
    				    
    				    sentence.appendChild(text);
    				    sentence.appendChild(pos);
    				    sentence.appendChild(deps);
    				    descriptionTag.appendChild(sentence);
    				    id++;
    				}
    			}
        		
        		if (!updateComments) continue;
        		
        		// Each comment
        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
        		
        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
        			Element commentTag = (Element)comments.item(commentsIndex);
        			
        			String comment = commentTag != null && commentTag.getFirstChild() != null
        					&& commentTag.getFirstChild().getNodeValue() != null ?
        					commentTag.getFirstChild().getNodeValue() : "";
        					
        			comment = web.HTML.htmlToText(comment);		
        			
        			// clear comment - different setup being created here
   					if (commentTag != null && commentTag.getFirstChild() != null
        					&& commentTag.getFirstChild().getNodeValue() != null) commentTag.getFirstChild().setNodeValue("");
       					
					ArrayList<String> sentences = processing.StringProcessing.SentenceSplitter(comment);
					//List<String> sentences = processing.NlpUtils.splitSentences(comment);
    				
    				for (int j = 0; j < sentences.size(); j++) {
    					//String s = sentences[j].replaceAll("/\\p{Graph}* "," ").trim();
    					if (sentences.get(j).length() == 0) continue;
    					
    					sentences.set(j, sentences.get(j)); 
    					
						// truncate if too long
    					String s = sentences.get(j);
    					
    					if (s.length() > 100) { 
    						
							int space = s.indexOf(" ",90);
							if (space >= 0 && space < 100)
								s = s.substring(0, s.indexOf(" ", 90));
							else if (s.length() > 100) {
								s = s.substring(0, 100);
								if (s.lastIndexOf(" ") > 0)
									s = s.substring(0,s.lastIndexOf(" "));
								s = "";
							}
						}	
						
    					Tree parse = null;
    					
    					try {
    						parse = (Tree) lexicalizedParser.parse(sentences.get(j));
    					} catch (Exception e) {
    						System.err.println("[Blog.updateEntries] error parsing tree");
    						continue;
    					}
    					
    				    GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
    				    String dependencies = gs.typedDependenciesCollapsed().toString();

    				    String taggedSentence = tagger.tagString(s); //parse.taggedYield().toString();
    				    
    				    Element sentence = blog.createElement("sentence");
    				    sentence.setAttribute("id", new Integer(j+1).toString());
    				    
    				    Element text = blog.createElement("string");
    				    text.setTextContent(sentences.get(j));
    				    
    				    Element pos = blog.createElement("pos");
    				    pos.setTextContent(taggedSentence);
    				    
    				    Element deps = blog.createElement("dependencies");
    				    deps.setTextContent(dependencies.substring(1, dependencies.length()-1));
    				    
    				    sentence.appendChild(text);
    				    sentence.appendChild(pos);
    				    sentence.appendChild(deps);
    				    commentTag.appendChild(sentence);
    				}		
        		}
        	}

        	return blog;
        }
        
        private static Blog processBlog(String outputDirectory, String profileFileIn,
                       File [] entriesFilesIn, int numEntries, int numComments, int maxSentenceSize, int numWords) {
        	
        	DOMParser parser = new DOMParser();
    		
    		try {
    			parser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return null;
    		}
    		
    		Blog blog = null;

    		if (profileFileIn != null && new File(profileFileIn).exists()) {
    		
		    	try {
		    		InputStreamReader isr = new InputStreamReader(new FileInputStream(profileFileIn),"UTF-8");
		    		InputSource in = new InputSource(isr);
		    		parser.parse(in);
		
		    		// add profile info
		        	Document profile = parser.getDocument();
		        	Element person = (Element)profile.getElementsByTagName("foaf:Person").item(0);
		        	String username = person.getElementsByTagName("foaf:nick").item(0).getFirstChild().getNodeValue();
		        	NodeList nameTag = person.getElementsByTagName("foaf:name");
		        	String fullname = nameTag != null && nameTag.getLength() > 0 && nameTag.item(0).getFirstChild() != null ? nameTag.item(0).getFirstChild().getNodeValue() : null;
		        	NodeList postedTag = person.getElementsByTagName("ya:posted");
		        	int postCount = postedTag != null && postedTag.getLength() > 0 && postedTag.item(0).getFirstChild() != null? Integer.parseInt(postedTag.item(0).getFirstChild().getNodeValue()) : -1;
		        	
		        	NodeList dobTag = person.getElementsByTagName("foaf:dateOfBirth");
		        	
		        	// somehow blog without date got in... delete it
		        	if (dobTag == null || dobTag.getLength() == 0) {
						System.err.println("[Blog.main] invalid id, " + username + ", missing dob. ");
		        		return null;
		        	}
		        	
		        	String date = dobTag.item(0).getFirstChild().getNodeValue();
		        	SimpleDateFormat format = date.length() > 5 ? new SimpleDateFormat("yyyy-MM-dd") : new SimpleDateFormat("yyyy");
		        	Date dob = dobTag != null && dobTag.getLength() > 0 ? format.parse(date) : null;
		        	NodeList countryTag = person.getElementsByTagName("ya:country");
		        	String country = countryTag != null && countryTag.getLength() > 0 ? countryTag.item(0).getAttributes().getNamedItem("dc:title").getNodeValue() : null;
		        	NodeList cityTag = person.getElementsByTagName("ya:city");
		        	String city = cityTag != null && cityTag.getLength() > 0 ? cityTag.item(0).getAttributes().getNamedItem("dc:title").getNodeValue() : null;
		        	URL homepage = new URL(person.getElementsByTagName("foaf:openid").item(0).getAttributes().getNamedItem("rdf:resource").getNodeValue());
		        	NodeList bioTag = person.getElementsByTagName("ya:bio");
		        	String biography = bioTag != null && bioTag.getLength() > 0 ? 
		        			 bioTag.item(0).getFirstChild() != null ? web.HTML.stripTags(bioTag.item(0).getFirstChild().getNodeValue()) : ""
		        					 : null;
		        	
		        	NodeList politicsTag = person.getElementsByTagName("politics");
		        	
		        	String politicalView = null;
		        	String politicalParty = null;
		        	for (int index = 0; index < politicsTag.getLength(); index++) {
		        		String politics = politicsTag.item(index).getFirstChild().getNodeValue().replaceAll("\"", "").trim();
		        		
		        		if (politics.indexOf("republican") >= 0) politicalParty = "republican";
		        		if (politics.indexOf("democrat") >= 0) politicalParty = "democrat";
		        		if (politics.indexOf("liberal") >= 0) politicalView = "liberal";
		        		if (politics.indexOf("conservative") >= 0) politicalView = "conservative";
		        	}
		        	NodeList religionTag = person.getElementsByTagName("religion");
		        	String religion = (religionTag != null && religionTag.getLength() > 0 ? religionTag.item(0).getFirstChild().getNodeValue().replaceAll("\"", "").trim() : null);
		            blog = new Blog(outputDirectory, username, null, dob, country, city, homepage, fullname, postCount, biography, politicalParty, politicalView, religion);
		            
		            // add interests
		            NodeList interests = person.getElementsByTagName("foaf:interest");
		            
		            for (int index = 0; index < interests.getLength(); index++) {
		            	String id = interests.item(index).getAttributes().getNamedItem("dc:interestID") != null ? interests.item(index).getAttributes().getNamedItem("dc:interestID").getNodeValue() : "";
		            	String title = interests.item(index).getAttributes().getNamedItem("dc:title") != null ? interests.item(index).getAttributes().getNamedItem("dc:title").getNodeValue() : "";
		    			String count = interests.item(index).getAttributes().getNamedItem("dc:interestWideCount") != null ? interests.item(index).getAttributes().getNamedItem("dc:interestWideCount").getNodeValue() : "";
		            	blog.addInterest(id,title,count);
		            }
		            
		            // add friends
		            NodeList friends = person.getElementsByTagName("foaf:knows");
		            
		            for (int index = 0; index < friends.getLength(); index++) {
		            	
		            	// make sure there is a friend type. If there is none it is probably a deleted account, so ignore it.
		            	if (((Element)friends.item(index)).getAttributeNode("dc:type") == null) continue;
		            	
		            	String friendName = ((Element)friends.item(index)).getElementsByTagName("foaf:nick").item(0).getFirstChild().getNodeValue();
		            	String friendType = ((Element)friends.item(index)).getAttributeNode("dc:type").getNodeValue();
		            	blog.addFriend(friendName,friendType);
		            }
		            isr.close();
		    	} catch (Exception e) {
		    		System.err.println("Error occurred in Blog.processBlog for profile file " + profileFileIn + ": " + e);
		    		e.printStackTrace();
		    		return null;
		    	}
    		}
    		else {
    			//System.err.println("[Blog.processBlog] no profile available");
    			blog = new Blog(outputDirectory,null, null, null, null, null, null, null, -1, null, null, null, null);
		           
    		}
    		
        	// add blog entries
        	for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	try {
            		InputStreamReader isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		parser.parse(in);
            		
            		Document profile = parser.getDocument();
            		
            		// General entry information
		        	NodeList politicsTag = profile.getElementsByTagName("politics");
		        	
		        	for (int i = 0; i < politicsTag.getLength(); i++) {
		        		String politics = politicsTag.item(i).getFirstChild().getNodeValue().replaceAll("\"", "").trim();
		        		
		        		if (politics.indexOf("republican") >= 0) blog.setPoliticalParty("republican");
		        		if (politics.indexOf("democrat") >= 0) blog.setPoliticalParty("democrat");
		        		if (politics.indexOf("liberal") >= 0) blog.setPoliticalView("liberal");
		        		if (politics.indexOf("conservative") >= 0) blog.setPoliticalView("conservative");
		        	}
		        	NodeList religionTag = profile.getElementsByTagName("religion");
		        	String religion = (religionTag != null && religionTag.getLength() > 0 ? religionTag.item(0).getFirstChild().getNodeValue().replaceAll("\"", "").trim() : null);
		        	blog.setReligion(religion);
		        	
		        	NodeList dobTag = profile.getElementsByTagName("foaf:dateOfBirth");
		        	if (dobTag != null && dobTag.getLength() > 0) {
			        	String date = dobTag.item(0).getFirstChild().getNodeValue();
			        	SimpleDateFormat format = date.length() > 5 ? new SimpleDateFormat("yyyy-MM-dd") : new SimpleDateFormat("yyyy");
			        	Date dob = dobTag != null && dobTag.getLength() > 0 ? format.parse(date) : null;
			        	blog.setDOB(dob);
		        	}

		        	NodeList genderTag = profile.getElementsByTagName("gender");
		        	if (genderTag != null && genderTag.getLength() > 0) {
			        	String gender = genderTag.item(0).getFirstChild().getNodeValue();
			        	blog.setGender(gender);
		        	}
		        	
		        	String author = profile.getElementsByTagName("author").item(0) == null ? null : profile.getElementsByTagName("author").item(0).getFirstChild().getNodeValue();
            		blog.setTitle(profile.getElementsByTagName("title").item(0) == null ? "" : profile.getElementsByTagName("title").item(0).getFirstChild().getNodeValue());
            		if (blog.getUsername() == null) {
            			if (author != null && entriesFilesIn[index].toString().indexOf("livejournal") == -1) blog.setUsername(author);
            			else blog.setUsername(blog.getTitle());
            		}
            		blog.setDescription((profile.getElementsByTagName("description") == null || profile.getElementsByTagName("description").item(0) == null || profile.getElementsByTagName("description").item(0).getFirstChild() == null)  ? "" : profile.getElementsByTagName("description").item(0).getFirstChild().getNodeValue());
            		if (profile.getElementsByTagName("lj:journaltype").getLength() != 0) blog.setType(profile.getElementsByTagName("lj:journaltype").item(0).getFirstChild().getNodeValue());
            		if (profile.getElementsByTagName("lastBuildDate").item(0) != null) blog.setLastBuildDate(profile.getElementsByTagName("lastBuildDate").item(0).getFirstChild().getNodeValue());
            		//blog.setID(profile.getElementsByTagName("lj:journalid").item(0).getFirstChild().getNodeValue());
            		//blog.setImage(profile.getElementsByTagName("url").item(0).getFirstChild().getNodeValue());
            		
            		// Each entry            		
    	        	NodeList entries = profile.getElementsByTagName("item");
    	        	
    	        	for (int entriesIndex = 0; entriesIndex < entries.getLength() && (numEntries == -1 || entriesIndex < numEntries); entriesIndex++) {
    	        		try {
    	        			Entry entry = Entry.processEntry((Element)entries.item(entriesIndex),numComments, maxSentenceSize, numWords);
    	        			if (blog.getLastBuildDate() == null) blog.setLastBuildDate(entry.getDate());
    	        			blog.addEntry(entry);
    	        		} catch (Exception e) {
    	        			System.err.println("Error occured in Blog.processBlog processing entry " + entriesIndex + " for " + entriesFilesIn[index] + ": " + e);
    	        			e.printStackTrace();
    	        		}
    	        	}
    	        	isr.close();
    	        	
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.processBlog for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            		return null;
            	}
        	}
        	return blog;
        }
        
        public boolean hasPOS() {
        	Entry first = (blog.threaded.livejournal.Entry)entries.get(this.getEntries().next());
        	if (first.getPOSText().isEmpty() || first.getPOSText().trim().equals("")) return false;
        	return true;
        	
        }
        
        public void setTitle(String titleIn) {
        	title = titleIn;
        }
        
        public void setDescription(String descriptionIn) {
        	description = descriptionIn;
        }
        
        public void setType(String typeIn) {
        	type = typeIn;
        }
        
        public void setLastBuildDate(String lbd) {
        	try {
	        	SimpleDateFormat lastBuildDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
	        	lastBuildDate = (Date)lastBuildDateFormat.parse(lbd);
        	} catch (Exception e) {
        		System.err.println("[Blog.setLastBuildDate] " + e);
        		e.printStackTrace();
        	}
        }
        
        public void setLastBuildDate(Date lbd) {
        	lastBuildDate = lbd;
        }
        
        public Date getLastBuildDate() {
        	return lastBuildDate;
        }
        
        public void setImage(String imageURL) {
        	image = imageURL;
        }
        
        public void setID(String idIn) {
        	id = idIn;
        }
        
        public String getTitle() {
        	return title;
        }
        
        public String getDescription() {
        	return description;
        }
        
        public String getType() {
        	return type;
        }
        
        public String getImage() {
        	return image;
        }
        
        public String getID() {
        	return id;
        }
        
        /**
         * Full name as defined by user. May not be their actual name
         * @return
         */
        public String getFullName() {
        	return fullName;
        }
        
        /**
         * Number of posts as per profile, all of these posts may not be downloaded
         * @return
         */
        public int getLifetimePostCount() {
        	return numPosts;
        }
        
        public void addInterest(String id, String interest, String interestCount) {
        	interests.put(interest,id + ":" + interestCount);
        }
        
        public boolean hasInterest(String interest) {
        	return interests.containsKey(interest);
        }
        
        public String getInterest(String interest) {
        	return interests.get(interest);
        }
        
        public Iterator<String> getInterests() {
        	return interests.keySet().iterator();
        }
        
        public String getBio() {
        	return bio;
        }
        
        public Blog getFriend(String username) {
        	if (!friends.containsKey(username)) return null;
			return Blog.processBlog(outputDirectory, username);
		}
        
    	public Blog getFriendsOf(String username) {
    		if (!friendsOf.containsKey(username)) return null;
			return Blog.processBlog(outputDirectory, username);
    	}
    	
    	public Blog getMutualFriends(String username) {
    		if (!mutualFriends.containsKey(username)) return null;
			return Blog.processBlog(outputDirectory, username);
    	}
    	
    	public void addFriend(String username, String type) {
    		if (type.equals("Friend")) {
    			friends.put(username, username);
    		}
    		else if (type.equals("FriendOf")) {
    			friendsOf.put(username, username);
    		}
    		else if (type.equals("MutualFriend")) {
    			mutualFriends.put(username, username);
    		}
    	}
    	
    	public int getNumEntries() {
    		return entries.size();
    	}
    	
        /**
         * @param args
         */
        public static void main(String[] args) throws Exception {
			
        	
        	
        	System.err.println("START: " + new Date());
        	
        	String directory = "/proj/nlp/users/sara/corpora/blogs/livejournal/";
        	String tagger = "/proj/nlp/users/sara/java/input/left3words-wsj-0-18.tagger";
        	String parser = "/proj/nlp/users/sara/java/input/englishPCFG.ser.gz";

        	// code to process blog
        	if (args[0].equals("blog")) {
        		//String profilesDirectory = directory + args[1];
        		//String entriesDirectory = directory + args[2];
        		//String username = args[3];
        		//Blog b = Blog.processBlog(profilesDirectory,entriesDirectory, username);
        		//Blog b = Blog.processBlog("/proj/nlp/users/orb/unannotated-entries/130518.xml");
        		//Blog b = Blog.processBlog("/proj/fluke/SCIL/corpora/wikipedia/WD_batch1LJ/Rihanna__Archive_2___The_Incident_.28per_se.29.xml");
        		//Blog b = Blog.processBlog("/proj/nlp/users/sara/SCIL/SCs/Influencer/data/sc-annotated-wd-april2012/" +
        				//"temp/processed/processed-Abortion__Archive_23___A_picture_of_abortion..xml/Abortion__Archive_23___A_picture_of_abortion..xml.sentences");
        		//"/proj/nlp/fluke/SCIL/corpora/arabic/LJ/scil-wiki__Secularism____D9_88_D8_AC_D9_87_D8_A9__D9_86_D8_B8_D8_B1.xml"
        		Blog b = Blog.processBlog(args[1]);
        		Iterator<?> it = b.getEntries();
        		
        		while (it.hasNext()) {
        			blog.threaded.livejournal.Entry e = (blog.threaded.livejournal.Entry)b.getEntry((String)it.next());
        			System.out.println(e.getEntryText());
        			System.out.println("---------------------- COMMENTS -----------------");
        			System.out.println(e.getSortedCommentsToString());
        			/*if (e.getNumComments() > 0) {
        				Iterator itE = e.getComments();
        				
        				while (itE.hasNext()) {
        					System.out.println(e.getComment((String)itE.next()).getCommentText());
        				}
        			}*/
        		
        		}
        		//blog.XMLUtilities.addSentenceTagsToBlogs("/proj/nlp/users/persuasion/wd_single_working/data/threads/Catholic_Church__Archive_15___NPOV_problem.xml","/proj/nlp/users/sara/Catholic_Church__Archive_15___NPOV_problem.xml");        		
        		//LexicalizedParser lexicalizedParser = new LexicalizedParser(parser);
        		//SwapEmoticons swapper = new SwapEmoticons();
        		//Blog.addSyntaxDependencies(entriesDirectory, entriesDirectory + "/processed/", username, lexicalizedParser, false, swapper);
        	}
			// code to search corpus
			else if (args[0].equals("search")) {
	        	String outputDirectory = "/proj/nlp/users/sara/corpora/blogs/livejournal/output/";
	        	Xtract xtract = new Xtract(tagger, parser, outputDirectory);
				
				for (int i = 1; i < args.length; i++) {
					
					ArrayList<String> phrases = xtract.searchCorpus(args[i]);
					System.err.println("[Blog.main] documents loaded. searching xtract for " + args[i] + ".");
					
					System.out.println(phrases.size() + " matches for " + args[i] + " found.");
					
					for (int index = 0; index < phrases.size(); index++) {
						System.out.println(index + ". " + phrases.get(index));
					}
				}
        	}
        	else if (args[0].equals("training-set")) {
        	
        		int count = Integer.valueOf(args[1]);
			
        		System.err.println("[Blog.main] processing " + count + " bloggers");
        		
				// code to add dependencies & POS from downloaded blogs
	        	String inputDirectory = directory + "original-corpus/";
	        	String outputDirectory = directory + "subcorpus/";
	        	        	
	        	LexicalizedParser lexicalizedParser = LexicalizedParser.loadModel(parser);
			    MaxentTagger taggerInit = new MaxentTagger("edu/stanford/nlp/models/pos-tagger/english-left3words-distsim.tagger");

	        	ArrayList<String> bloggers = new ArrayList<String>();
	        	
	        	//HashSet<String> testBlogs = new HashSet<String>();
	        	
	        	// read in list of blogs in test set so that we don't use them in training
	        	/*try {
					BufferedReader in = new BufferedReader(
							new FileReader(directory + "test-corpus/" + minAge + "-" + maxAge + "/profile_list.txt"));
			
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						testBlogs.add(inputLine);
					}
					in.close();
	        	}
				catch (Exception e) {
					System.err.println("[Blog.main] error occurred reading profile_list of test set");
					e.printStackTrace();
				}
	        	*/
	        	try {
	        		/*
					BufferedReader in = new BufferedReader(
							new FileReader(directory + "age-lists/" + minAge + "-" + maxAge + ".txt"));
			
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						bloggers.add(inputLine);
					}
					in.close();
					
					Collections.shuffle(bloggers);
					new File(outputDirectory).mkdirs();
    				BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "profile_list.txt"));
    				*/
	        	
	        		BufferedReader in = new BufferedReader(
						new FileReader(outputDirectory + "profile_list.txt"));
		
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						bloggers.add(inputLine);
					}
					in.close();
    				
    				int i = 0; // keep track of number of files processed successfully
    				
    				// load emoticon file for swapping
    				SwapEmoticons swapper = new SwapEmoticons();
    				
					for (int index = 0; index < bloggers.size() && i < count; index++) {
			        				        	
						// check to make sure blogger is not in test set given to iarpa
						//if (testBlogs.contains(bloggers.get(index))) continue;
						
						System.err.println("[Blog.main] " + i + ". adding Dependencies & POS to " + bloggers.get(index));

						try {
							Blog.addSyntaxDependencies(inputDirectory, outputDirectory, bloggers.get(index), taggerInit, lexicalizedParser, true, swapper);
		    				// add to profile_list.txt
		    				// out.write(bloggers.get(index) + "\n");
						} catch (Exception e) {
							System.err.println("Error occurred in Blog.main adding syntax dependencies: " + e);
							e.printStackTrace();
							continue;
						}
						i++;
					}
					//out.close();
					System.err.println("[Blog.main] completed adding syntax dependencies");
	        	} catch (Exception e) {
					System.err.println("Error occurred in Blog.main: " + e);
					e.printStackTrace();
				}
        	}
        	else if (args[0].equals("test-set")) {
            	
        		String minAge = args[1];
        		String maxAge = args[2];
        		int count = Integer.valueOf(args[3]);
			
        		System.err.println("[Blog.main] processing " + count + " bloggers between " + minAge + "-" + maxAge);
        		
				// code to add dependencies & POS from downloaded blogs
	        	String inputDirectory = directory + "original-corpus/";
	        	String outputDirectory = directory + "subcorpus/";
	
	        	ArrayList<String> bloggers = new ArrayList<String>();
	        	
	        	// don't look at used blogs (from training and iarpa test set)
	        	HashSet<String> usedBlogs = new HashSet<String>();
	        	
	        	// read in list of blogs in test set so that we don't use them in training
	        	try {
					BufferedReader in = new BufferedReader(
							new FileReader(directory + "test-corpus/" + minAge + "-" + maxAge + "/profile_list.txt"));
			
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						usedBlogs.add(inputLine);
					}
					in.close();
	        	}
				catch (Exception e) {
					System.err.println("[Blog.main] error occurred reading profile_list of test set");
					e.printStackTrace();
				}
				try {
					BufferedReader in = new BufferedReader(
							new FileReader(directory + "subcorpus/profile_list.txt"));
			
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						usedBlogs.add(inputLine);
					}
					in.close();
	        	}
				catch (Exception e) {
					System.err.println("[Blog.main] error occurred reading profile_list of subcorpus");
					e.printStackTrace();
				}
				try {
					BufferedReader in = new BufferedReader(
							new FileReader(directory + "subcorpus-test/profile_list.txt"));
			
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						usedBlogs.add(inputLine);
					}
					in.close();
	        	}
				catch (Exception e) {
					System.err.println("[Blog.main] error occurred reading profile_list of subcorpus-test");
					e.printStackTrace();
				}
	        	
	        	try {
					BufferedReader in = new BufferedReader(
							new FileReader(directory + "age-lists/" + minAge + "-" + maxAge + ".txt"));
			
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						if(usedBlogs.contains(inputLine)) continue;
						bloggers.add(inputLine);
					}
					in.close();
					
					Collections.shuffle(bloggers);
					new File(outputDirectory).mkdirs();
					// append to profile_list
    				BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "profile_list.txt",true));
    				
    				int i = 0; // keep track of number of files processed successfully
    				
    				LexicalizedParser lexicalizedParser = LexicalizedParser.loadModel(parser);
    				
    				// load emoticon file for swapping
    				SwapEmoticons swapper = new SwapEmoticons();
    				
    			    MaxentTagger taggerInit = new MaxentTagger("edu/stanford/nlp/models/pos-tagger/english-left3words-distsim.tagger");
    				
					for (int index = 0; index < bloggers.size() && index < count; index++) {
			        				        	
						//System.err.println("[Blog.main] " + i + ". removing personal info from " + bloggers.get(index));
						System.err.println("[Blog.main] " + i + ". adding POS and dependencies to " + bloggers.get(index));

						try {
							//Blog.removePersonalInformation(inputDirectory, outputDirectory, bloggers.get(index));
		    				// add to profile_list.txt
							Blog.addSyntaxDependencies(inputDirectory, outputDirectory, bloggers.get(index), taggerInit, lexicalizedParser, false, swapper);
		    				out.write(bloggers.get(index) + "\n");
						} catch (Exception e) {
							System.err.println("Error occurred in Blog.main creating test set: " + e);
							e.printStackTrace();
							continue;
						}
						i++;
					}
					out.close();
					System.err.println("[Blog.main] completed adding syntax dependencies");
	        	} catch (Exception e) {
					System.err.println("Error occurred in Blog.main: " + e);
					e.printStackTrace();
				}
        	}
        	System.err.println("END: " + new Date());
        }
}
