/**
 * 
 */
package blog.threaded.livejournal;

import java.util.*;
import java.net.*;
import java.io.*;
import java.util.regex.*;
import java.text.*;

//import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.*;

import dom.Writer;
//import org.xml.sax.*;

import bot.*;

/**
 * @author sara
 *
 */
public class LiveJournalRobot extends Robot {

    //private static final String REGEX = "\n";
	public ArrayList<String> urls;
	String outputDirectory;
	boolean overwrite = false;
	int count = 0;

	/**
	 * 
	 * @param outputDirectoryIn
	 * @param overwriteIn
	 */
	public LiveJournalRobot(String outputDirectoryIn, boolean overwriteIn) {
		urls = new ArrayList<String>();
		outputDirectory = outputDirectoryIn;
		overwrite = overwriteIn;
		count = 0;
	}

	/**
	 * 
	 * @param outputDirectoryIn
	 */
	public LiveJournalRobot(String outputDirectoryIn) {
		this(outputDirectoryIn, false);
	}

	/**
	 * crawl the website. 
	 */
	public boolean run(ArrayList<String> data) {
		return this.run(data,null);
	}
	
	/**
	 * crawl the website, but ignore users that are in the blacklist
	 */
	public boolean run(ArrayList<String> data,Hashtable<String,String> blacklist) {

		try {
			new File(outputDirectory).mkdirs();
			BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/profile_list.txt",true));
			BufferedWriter bl = new BufferedWriter(new FileWriter(outputDirectory + "/blacklist.txt",true));
			
	        for(String id : data) {
	        	
	        	if (id.startsWith("#")) continue;
	        	id = id.replaceAll("[>< ]", "");
	        	
	        	if (id.trim().equals("")) continue;
	        	
	        	urls.add(id);
	        	
	        	if (blacklist != null && blacklist.containsKey(id)) continue;
	        	
	        	Document profile = null;
	        	profile = web.Data.getXMLPage(new URL(id.indexOf("_") >= 0 ?
    	        		"http://users.livejournal.com/" + id + "/data/foaf" : "http://" + id + ".livejournal.com/data/foaf"));
	        	
	        	if (profile == null) {
	        		System.err.println("[LiveJournalRobot.run] no profile found for id " + id);
	        		continue;
	        	}
	        	
        		Element page = profile.getDocumentElement();
        		NodeList dobs = page.getElementsByTagName("foaf:dateOfBirth"); 
        		NodeList countryNode = page.getElementsByTagName("ya:country"); //dc:title attribute = US|CA|UK|AU
        		
    			// Keep list of profiles already visited to save time in future runs
        		blacklist.put(id, id);
    			bl.write(id + "\n");
    			bl.flush();
    			
    			boolean keep = true;
    			
    			String dob = "";
    			String country = "";
    			
    			if (dobs == null || dobs.getLength() == 0 || dobs.item(0).getFirstChild() == null) {
    				continue;
    			}
    			else {
    				dob = dobs.item(0).getFirstChild().getNodeValue();
    			}
    			
    			if (countryNode == null || countryNode.getLength() == 0 || countryNode.item(0).getAttributes().getNamedItem("dc:title") == null) {
    				keep = false;
    			}
    			else {
    				country = countryNode.item(0).getAttributes().getNamedItem("dc:title").getNodeValue();
    			}
    			
        		// if there is a DOB and it contains a year: xxxx-xx-xx or xxxx, then save it
        		if (!dob.matches("(19[3-9][0-9]-[0-3][0-9]-[0-9][0-9]|19[3-9][0-9])")
        				|| !country.matches("US|UK|CA|AU")) keep = false; 

    			// profile matches are good, lets make sure they are still blogging
        		Document entries = null;
        		entries = web.Data.getXMLPage(new URL(id.indexOf("_") >= 0 ?
    	        		"http://users.livejournal.com/" + id + "/data/rss" : "http://" + id + ".livejournal.com/data/rss"));
    	        	
        		if (entries == null) {
        			System.err.println("[LiveJournalRobot.run] no entry found for id " + id);
        			keep = false;
    	        }
        			
            	NodeList lbDate = entries.getDocumentElement().getElementsByTagName("lastBuildDate"); 
        			
            	String date = "";
            	
            	if (lbDate == null || lbDate.getLength() == 0 || lbDate.item(0).getFirstChild() == null) {
            		keep = false;
            	}
            	else {
                	SimpleDateFormat lastBuildDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
                	Date lastBuildDate = (Date)lastBuildDateFormat.parse(lbDate.item(0).getFirstChild().getNodeValue());
                		
                	date = new SimpleDateFormat("MM-dd-yyyy").format(lastBuildDate);
                	int year = Integer.valueOf(new SimpleDateFormat("yyyy").format(lastBuildDate));
                	if (year < 2009 || year > 2010) keep = false;
            	}
            		
            	NodeList journalTypeNode = entries.getDocumentElement().getElementsByTagName("lj:journaltype");
            		
            	String journalType = (journalTypeNode != null && journalTypeNode.getLength() > 0 && journalTypeNode.item(0).getFirstChild() != null) ?
            				journalTypeNode.item(0).getFirstChild().getNodeValue() : "";
            		
            	System.err.println(id + ", " + dob + ", " + country + ", " + date + ", " + journalType + ", " + keep);
            	if (!keep) continue;
            	
        		// 1. Print out id
     			out.write(id + "\n");
     			count++;
     			out.flush();
    			
    			// 2. Store entire profile file
    			this.downloadProfile(id);
    			this.downloadBlog(id);
			}
	        out.close();
	        bl.close();
	        System.err.println("[LiveJournalRobot.run] finished a friend list!");
	        return true;
		} catch (Exception e) {
    		System.err.println("Error occured in LiveJournalRobot.run: " + e);
    		e.printStackTrace();
    	}
		
		return false;
	}
	
	/**
	 * Download and store the profile for username in outputDirectory. Add friend relationship and interest stats first.
	 * @param username
	 */
	public void downloadProfile(String username) {
		try {
			Document profileData = null;
			
			if (username.indexOf("_") >= 0) {
				profileData = web.Data.getXMLPage(new URL("http://users.livejournal.com/" + username + "/data/foaf"));
	    	}
	    	else {
	    		profileData = web.Data.getXMLPage(new URL("http://" + username + ".livejournal.com/data/foaf"));
	    	}
			
			//	Don't do any processing if the profile is empty
			if (profileData==null)	return;			
			
			ArrayList<String> friendsData = web.Data.getPage(new URL("http://www.livejournal.com/misc/fdata.bml?user="+username));
    		ArrayList<String> interestData = web.Data.getPage(new URL("http://www.livejournal.com/misc/interestdata.bml?user="+username));
			
			//BufferedWriter pr = new BufferedWriter(new FileWriter(outputDirectory + "/profiles/" + username + ".xml"));
			profileData = addTypeOfFriend(profileData, friendsData, username);
			profileData = addStatsOfInterest(profileData, interestData, username);
			
			//pr.write(profileData);
			//pr.close();
			
			new File(outputDirectory + "/profiles/").mkdirs();
			FileOutputStream xml = new FileOutputStream(outputDirectory + "/profiles/" + username + ".xml");
			Writer writer = new Writer();
			writer.setOutput(xml,"UTF-8");
			writer.write(profileData);
			
		} catch (Exception e) {
    		System.err.println("Error occured in LiveJournalRobot.downloadProfile for user " + username + ": " + e);
    		e.printStackTrace();
    		return ;
    	} 	
	}

	private Document addStatsOfInterest(Document profileData, ArrayList<String> interestData, String username) {
		
		Hashtable<String, String>statInterest = new Hashtable<String, String>();

		for(int k=0;k < interestData.size();k++){
			if (interestData.get(k).startsWith("#")) continue;
			if (interestData.get(k).startsWith("!")) continue;
			if (interestData.get(k).trim().isEmpty()) continue;

			String [] eachCol = interestData.get(k).split("\\s+");
			
			try {
					int intId = Integer.parseInt(eachCol[0]);
					int intCount = Integer.parseInt(eachCol[1]);
					String interest = interestData.get(k).replaceFirst("[0-9]+\\s+[0-9]+", "").trim();
					statInterest.put(interest, intId + ":" + intCount);
			}
			catch (Exception e) {
				System.err.println("Error occurred in LiveJournalRobot.addStatsOfInterest: something is wrong in interest list for " + username + " " + interestData.get(k) + " " + e);
				e.printStackTrace();
			}
		}
		if(interestData!=null){
			NodeList profileElements = profileData.getElementsByTagName("foaf:interest");
			
			for(int index =0; index < profileElements.getLength(); index++) {

				Node personInterest = profileElements.item(index);
				String interestName = ((Element)personInterest).getAttribute("dc:title");

				String val = statInterest.get(interestName);

				if(val!=null){
					String [] vals = val.split(":");
					if(vals.length!=2){
						System.err.println("Error finding the two statistics! Username: "+username);
					}
					else{
						((Element)personInterest).setAttribute("dc:interestID", vals[0]);
						((Element)personInterest).setAttribute("dc:interestWideCount", vals[1]);
					}
				}
			}
		}
		
		//Now
		return profileData;
	}

	private Document addTypeOfFriend(Document profileData, ArrayList<String> friendsData, String username) {
		// We have already checked for profileData = NULL, so no need in this function
		Hashtable<String, String>typeFriendRight = new Hashtable<String, String>();
		Hashtable<String, String>typeFriendLeft = new Hashtable<String, String>();
		NodeList profileElements;
		
		// compute friend types
		for(int k = 0; k < friendsData.size(); k++){
			if(friendsData.get(k).trim().isEmpty() || 
					friendsData.get(k).startsWith("#") || friendsData.get(k).startsWith("!")) continue;

				try {
					String [] friendLine = friendsData.get(k).split("\\s+");

					if(friendsData.get(k).startsWith(">")){
						typeFriendRight.put(friendLine[1], friendLine[0]);
					}
					else if (friendsData.get(k).startsWith("<")){
						typeFriendLeft.put(friendLine[1], friendLine[0]);						
					}
					else {
						throw new Exception();
					}
				}
				catch (Exception e) {
					System.err.println("Error occurred in addTypeOfFriend for " + username + " on " + friendsData.get(k) + ": " + e);
					e.printStackTrace();
				}
		}

		// update friend data with friend type
		if (friendsData != null) {
			
			// update existing friends
			profileElements = profileData.getElementsByTagName("foaf:knows");
			
			for (int i = 0;i < profileElements.getLength(); i++) {
				Node personKnows = profileElements.item(i);
				String friendName = ((Element)personKnows).getElementsByTagName("foaf:nick").item(0).getTextContent();
				
				String valRight = typeFriendLeft.get(friendName);
				String valLeft = typeFriendRight.get(friendName);
				 
				if(valLeft==null && valRight==null){
					//System.err.println("Neither of < and > found for user: "+username+" when friend name is: "+friendName+" ...Adding dc:type=NA");
					continue;
				}
				else if (valLeft !=null && valRight!=null){
					((Element)personKnows).setAttribute("dc:type", "MutualFriend");
					typeFriendLeft.remove(friendName);
					typeFriendRight.remove(friendName);
				}
				else if (valLeft != null) {
					((Element)personKnows).setAttribute("dc:type", "FriendOf");
					typeFriendLeft.remove(friendName);
				}
				else if (valRight != null) {
					((Element)personKnows).setAttribute("dc:type", "Friend");
					typeFriendRight.remove(friendName);
				}
			}
			
			// add all the friendOf people. They weren't in the xml file before
			if(!typeFriendLeft.isEmpty()){
				Enumeration <String> en = typeFriendLeft.keys();
				
				while(en.hasMoreElements()) {
				
					String friendOf = en.nextElement();
					NodeList nodeListPerson = profileData.getElementsByTagName("foaf:Person");
					Node nodePerson = nodeListPerson.item(0);
				//	System.err.println(nodePerson.getTextContent());
					Element newNodeKnows = profileData.createElement("foaf:knows");
					Element newNodePerson = profileData.createElement("foaf:Person");
					Element newNodeNick = profileData.createElement("foaf:nick");
					Text friendOfName = profileData.createTextNode(friendOf);
					
					Text newline = profileData.createTextNode("newline");
					newline.setTextContent("\n");
					newline.setNodeValue("\n");
					Text spaceline = profileData.createTextNode("spaceline");
					spaceline.setTextContent("  ");
					spaceline.setNodeValue("  ");
					
					newNodeNick.appendChild(friendOfName);
					
					newNodePerson.appendChild(newline.cloneNode(true));
					
					newNodePerson.appendChild(newNodeNick);
					
					newNodePerson.appendChild(newline.cloneNode(true));
					
					((Element)newNodeKnows).setAttribute("dc:type", "FriendOf");
					newNodeKnows.appendChild(newline.cloneNode(true));
										
					newNodeKnows.appendChild(newNodePerson);
					newNodeKnows.appendChild(newline.cloneNode(true));
					
					nodePerson.appendChild(newNodeKnows);
					nodePerson.appendChild(newline.cloneNode(true));					
				}
			}
		}
		
		return profileData;
	}

	public void downloadProfiles(ArrayList<String> usernames) {
		//int i=1;
		String username;
		File checkFile;
		for (int index = 0; index < usernames.size(); index++) {
			username = usernames.get(index);
			checkFile = null;
			checkFile = new File(outputDirectory + "/profiles/" + username + ".xml");
			
			if(checkFile.exists() && !overwrite){
				System.err.println("[LiveJournalRobot.downloadProfiles] File already exists for user: "+username+" ... Not downloading again!");
				continue;
			}
			//i++;
			this.downloadProfile(username);
		}
	}
	/**
	 * Download all pages of blogs in list and store them in outputDirectory
	 * @param usernames
	 */
	public void downloadBlogs(ArrayList<String> usernames, int min, int max) {
		
		for (int index = min; index < usernames.size() && (max == -1 || index <= max) ; index++) {
			this.downloadBlog(usernames.get(index));
		}
	}
	
	/**
	 * Download a blog and store it in outputDirectory (see constructor)
	 * only download entries that havent been downloaded yet.
	 * @param username
	 */
	public void downloadBlog(String username) {
		
		File directory = new File(outputDirectory + "/entries/" + username + "/");

		Date end = null;

		SimpleDateFormat downloadFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
		SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
		SimpleDateFormat formatWithTime = new SimpleDateFormat("yyyy.MM.dd.HH:mm:ss");
		
		/** 
		 * get the start and end date of blogs we have already downloaded so that we don't download duplicates.
		 */
		if (directory.exists()) {
			try {
				String [] entries = directory.list();
				
				for (int index = 0; index < entries.length; index++) {
					String fileName = entries[index].substring(0,entries[index].length() - ".xml".length());
					String [] date = fileName.split("-");
					
					if (date.length < 2) continue;
					
					if (!date[2].trim().isEmpty()) {
						Date e = date[2].indexOf(":") >= 0 ? formatWithTime.parse(date[2]) : format.parse(date[2]);
						
						
						if (end == null || e.after(end)) {
							end = e;
						}
					}
				}
			} catch (Exception e) {
				System.err.println("Error occurred in LiveJournalRobot.downloadBlog checking start and end dates of previously downloaded blog entries: " + e);
				e.printStackTrace();
			}
		}
		
		/**
		 * Update and save the entries: 1. only download ones that haven't been downloaded yet. 2. add comments to entries
		 */
		try {
			Document page = web.Data.getXMLPage(new URL("http://" + username + ".livejournal.com/data/rss"));
			String startDate = "";
			String endDate = "";
			
			if (page == null) return;
			
			try {
				
				NodeList items = page.getElementsByTagName("item");
				Node channel = page.getElementsByTagName("channel").item(0);
				
				for (int itemsIndex = 0; itemsIndex < items.getLength(); itemsIndex++) {
					
					Element thisItem = (Element)items.item(itemsIndex);
					
					// get rid of outdated ones
					Date pubDate = downloadFormat.parse(thisItem.getElementsByTagName("pubDate").item(0).getFirstChild().getNodeValue());
					
					if (!overwrite && end != null && (pubDate.before(end) || pubDate.equals(end))) {
						channel.removeChild(thisItem.getNextSibling());
						channel.removeChild(thisItem);
						itemsIndex --;
						continue;
					}
					else {
						//Element description = (Element)thisItem.getElementsByTagName("description").item(0);
						//description.getFirstChild().setTextContent(web.HTML.htmlToText(description.getFirstChild().getTextContent()));
					}
					
					// add comments to new entries
					try {
						Node commentNode = ((Element)items.item(itemsIndex)).getElementsByTagName("comments").item(0);
						
						if (commentNode == null) continue;
						
						// set up new comment node
						Node newCommentNode = this.createNewCommentNode(page, username, commentNode);
	
						if (newCommentNode == null) continue;
						
						// Add the comment data to the rss xml feed
						thisItem.replaceChild(newCommentNode,commentNode);
					
					}
					catch (Exception e) {
						System.err.println("Error occurred in LiveJournalRobot.downloadBlogs downloading " + username + ", " + ": " + e);
						e.printStackTrace();
					}
				}
				
				// Write the file, use start and end date of entries to keep track of data downloaded
				NodeList dates = page.getElementsByTagName("pubDate");
				// date: Thu, 18 May 2006 05:30:38 GMT
				endDate = items.getLength() == 0 ? "" : formatWithTime.format(downloadFormat.parse(dates.item(0).getFirstChild().getNodeValue()));
				startDate = items.getLength() == 0 ? "" : formatWithTime.format(downloadFormat.parse(dates.item(dates.getLength()-1).getFirstChild().getNodeValue()));
				
				// no new entries
				if (endDate.isEmpty() && startDate.isEmpty()) 
					return;
				
			} catch (Exception e) {
				System.err.println("Error occurred in LiveJournalRobot.downloadBlogs downloading (processing xml file) " + username + ": " + e);
				e.printStackTrace();
			}
 			
			File dir = new File(outputDirectory + "/entries/" + username);
			dir.mkdir();

			
			// if we are overwriting we must remove all files in the folder first since date overlaps may be an issue
			File [] files = dir.listFiles();
			if (overwrite && files != null) {
				
				for (int index = 0; index < files.length; index++) {
					files[index].delete();
				}
			}
			
			new File(outputDirectory + "/entries/" + username).mkdirs();
			FileOutputStream xml = new FileOutputStream(outputDirectory + "/entries/" + username + "/" + username + "-" + startDate + "-" + endDate + ".xml");
			Writer writer = new Writer();
			writer.setOutput(xml,"UTF-8");
			writer.write(page);
			//System.err.println("[LiveJournalRobot.downloadBlog] successfully downloaded blog for " + username);
			
		} catch (Exception e) {
			System.err.println("Error occurred in LiveJournalRobot.downloadBlogs downloading " + username + ": " + e);
			e.printStackTrace();
		}
	}

	/**
	 * Create the new comment node by adding more details 
	 * 
	 * @param page
	 * @param username
	 * @param commentNode
	 * @return
	 */
	private Node createNewCommentNode(Document page, String username, Node commentNode) {
		
		if (commentNode.getFirstChild() == null) return null;
		
		String commentUrl = commentNode.getFirstChild().getNodeValue();
		String data = null;
		
		try {
			data = web.Data.getPageAsString(new URL(commentUrl));
		} catch (Exception e) {
			System.err.println("Error occurred in LiveJournalRobot.createNewCommentNode: " + e);
		}
		
		// comments contain: commentator's username, date of comment, comment, link to specific comment. 
		// check to make sure there are comments
		if (data.indexOf("talk-comment") < 0) return null;
	
		Element newCommentsNode = page.createElement("comments");
		newCommentsNode.setAttribute("url", commentUrl);
		
		Text newline = page.createTextNode("newline");
		newline.setTextContent("\n");
		newline.setNodeValue("\n");

		newCommentsNode.appendChild(newline.cloneNode(true));
		
		// comments extend on to multiple pages
		if (data.indexOf("Page 1 of") > 0) {
			Pattern p = Pattern.compile("Page 1 of [0-9]*</td>");
			Matcher m = p.matcher(data);
			m.find();
			int numPages = Integer.valueOf(m.group().substring("Page 1 of ".length(),m.group().indexOf("</td>")));
			
			for (int index = 2; index <= numPages; index++) {
				try {
					data += web.Data.getPageAsString(new URL(commentUrl + "?page=" + index));
				} catch (Exception e) {
					System.err.println("Error occurred in LiveJournalRobot.createNewCommentNode: " + e);
				}
			}
		}
		
		data = data.substring(data.indexOf("'talk-comment'") + 14);
		String [] commentString = data.split("'talk-comment'");
		
		for (int c = 0; c < commentString.length; c++) {
			
			try {
				// username - if anonymous, skip username steps.
				int pos = commentString[c].indexOf("lj:user=\'") >= 0 ? commentString[c].indexOf("lj:user=\'") + "lj:user=\'".length() : -1;
				
				Element user = page.createElement("user");
				String cUsername = "anonymous";
				if (pos >= 0 && pos < commentString[c].indexOf("Reply to this")) {
					cUsername = commentString[c].substring(pos,
											commentString[c].indexOf("'", pos)); 
					
					// urls
					pos = commentString[c].indexOf("href='",pos) + "href='".length();
					String profile_url = commentString[c].substring(pos, commentString[c].indexOf("'", pos));
					pos = commentString[c].indexOf("href='",pos)  + "href='".length();
					String blog_url = commentString[c].substring(pos, commentString[c].indexOf("'", pos));
					user.setAttribute("blog_url", blog_url);
					user.setAttribute("profile_url", profile_url);
				}
				else {
					pos = 0;
				}
				
				pos = commentString[c].indexOf("href='",pos)  + "href='".length();
				String comment_url = commentString[c].substring(pos, commentString[c].indexOf("'", pos));
				
				// actual comment!
				pos = commentString[c].indexOf("<td class='talk-comment-box'>",pos) + "<td class='talk-comment-box'>".length();
				String theComment = commentString[c].substring(pos,	commentString[c].indexOf("<p", pos));
				//theComment = web.HTML.htmlToText(theComment);
				
				// date ex: 2008-06-25 12:44 pm UTC
				Pattern p = Pattern.compile("[0-9][0-9][0-9][0-9]-[01][0-9]-[0123][0-9] [012][0-9]:[0-5][0-9] [ap]m UTC");
				Matcher m = p.matcher(commentString[c]);
				m.find();
				String date = m.group();
				
				// parent comment: ex: <a href='http://ourdancer.livejournal.com/43473.html?thread=65489#t65489'>Parent</a>
				//<a href='http://ourdancer.livejournal.com/43473.html?thread=65489#t65489'>Parent</a>)(<
				p = Pattern.compile("<a href='http://" + username + "\\.livejournal\\.com/[0-9]+\\.html\\?thread=[0-9]+#t[0-9]+'>Parent</a>");
				m = p.matcher(commentString[c]);

				String parentURL = "";
				String parentID = "";
				if (m.find()) {
					parentURL = m.group().substring("<a href='".length(),m.group().indexOf("'>Parent</a>"));
					parentID = parentURL.substring(parentURL.indexOf("#")+1);
				}
				else {
					parentURL = commentUrl;
					parentID = parentURL.substring(parentURL.lastIndexOf("/")+1,parentURL.indexOf(".html"));
				}
				
				
				// add comment to comments element
				Element newComment = page.createElement("comment");
				newComment.setAttribute("date-time", date);
				newComment.setAttribute("url", comment_url);
				newComment.setAttribute("parent-id", parentID);
				newComment.setAttribute("parent-url", parentURL);
				
				Text uname = page.createTextNode("username");
				uname.setTextContent(cUsername);
				user.appendChild(uname);
				
				Element textElt = page.createElement("text");						
				Text text = page.createTextNode("text");
				text.setTextContent(theComment);
				textElt.appendChild(text);
				
				// newline used to add lines for readability
				newComment.appendChild(newline.cloneNode(true));
				newComment.appendChild(user);
				newComment.appendChild(newline.cloneNode(true));
				newComment.appendChild(textElt);
				newComment.appendChild(newline.cloneNode(true));
				
				newCommentsNode.appendChild(newComment);
				newCommentsNode.appendChild(newline.cloneNode(true));
			}
			catch (Exception e) {
				System.err.println("Error occurred in LiveJournalRobot.downloadBlogs downloading " + username + " on comment " + commentUrl + ": " + e);
				e.printStackTrace();
			}
		}
		
		return newCommentsNode;
	}
	
	/* (non-Javadoc)
	 * @see bot.Robot#urls()
	 */
	public ArrayList<String> urls() {
		// TODO Auto-generated method stub
		return urls;
	}

	public void clearUrls() {
		urls = new ArrayList<String>();
	}
	
	public int getNumDownloads() {
		return count;
	}
	
	public static HashSet<String> loadProfileList(String file) {
		// if test is true populate list of test blogs
		if (file == null) return null;
		
		File list = new File(file);
   				
		if (!list.exists()) return null;
		
		HashSet<String> blogList = new HashSet<String>();
		
   		try {
	   		BufferedReader in = new BufferedReader(new FileReader(list));
	   			
	   			String inputLine = "";
	   			
	   			while ((inputLine = in.readLine()) != null) {
		        	blogList.add(inputLine);
	   			}
		} catch (Exception e) {
			System.err.println("[LiveJournalRobot.loadProfileList]: " + e);
			e.printStackTrace();
		}
   		return blogList;	
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String outputDirectory = null;
		String username = null;
		String inputFile = null;
		boolean overwrite = false;
		int max = -1;
		int min = 0;
		int depth = 0;
		
		//LiveJournalRobot LJRobot1 = new LiveJournalRobot("/proj/nlp/users/sara/corpora/blogs/livejournal/tmp/",true);
		//LJRobot1.downloadBlog("sebab");
		//System.exit(0);
		
		if (args.length <= 0) {
			System.out.println("USAGE: <crawl|download> -outputDirectory \"\" -username \"\"");
			System.exit(0);
		}
		
		String action = args[0];
		
		// Output Directory ex: "/proj/nlp/users/sara/corpora/blogs/livejournal"
		for (int index = 1; index < args.length; index++) {
		
			if (args[index].equals("-output")) {
				outputDirectory = args[++index];
			}
			else if (args[index].equals("-username")) {
				username = args[++index];
			}
			else if (args[index].equals("-input")) {
				inputFile = args[++index];
			}
			else if (args[index].equals("-overwrite")) {
				overwrite = true;
			}
			else if (args[index].equals("-depth")) {
				depth = Integer.parseInt(args[++index]);
			}
			else if (args[index].equals("-max")) {
				max = Integer.parseInt(args[++index]);
			}
			else if (args[index].equals("-min")) {
				min = Integer.parseInt(args[++index]);
			}
		}
		
		if (action.equals("crawl") && (username == null || outputDirectory == null ) && depth == 0) {
			System.out.println("Missing required crawl parameter");
			System.out.println("USAGE: crawl -output \"\" -username \"\" -depth \"\" (-max \"\")");
			System.exit(0);
		}
		if (action.equals("download") && (outputDirectory == null || 
				(inputFile == null && username == null )) ) {
			System.out.println("Missing required download parameter");
			System.out.println("USAGE: download -output \"\" <-input \"\"|-username \"\">");
			System.exit(0);
		}
		else if (!action.equals("crawl") && !action.equals("download")) {
			System.out.println("Invalid action " + action);
			System.out.println("USAGE: <crawl|download> -output \"\" -username \"\"");	
			System.exit(0);
		}
		
		System.err.println("START: " + new Date());

		// Crawl for usernames
		if (action.equals("crawl")) {
			Crawler crawler = new Crawler("http://www.livejournal.com/misc/fdata.bml?user=", new LiveJournalRobot(outputDirectory,overwrite),outputDirectory + "/blacklist.txt");
			crawler.crawl(username, depth, max);
		}
		// Read the usernames and download the blogs
		else if (action.equals("download")) {
		
			ArrayList<String> usernames = new ArrayList<String>();
			
			if (inputFile != null) {
			
				try {
					BufferedReader in = new BufferedReader(
							new FileReader(inputFile));
			
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						//String [] data = inputLine.split(",");
						usernames.add(inputLine);
					}
				} catch (Exception e) {
					System.err.println("Error occurred in LiveJournalRobot.main: " + e);
					e.printStackTrace();
				}
			}
			else {
				usernames.add(username);
			}
			
			LiveJournalRobot LJRobot = new LiveJournalRobot(outputDirectory,overwrite);
			//LJRobot.downloadProfiles(usernames);
			LJRobot.downloadBlogs(usernames,min,max);
		}
		
		System.err.println("END: " + new Date());
	}

}
