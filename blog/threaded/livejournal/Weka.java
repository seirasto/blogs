package blog.threaded.livejournal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
//import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.*;
import weka.classifiers.functions.Logistic;
//import src.main.java.weka.classifiers.functions.*;
import java.io.*;
import collocation.*;
import blog.Sentence;

/**
 * Class to gather features and then run weka
 * @author sara
 *
 */

public class Weka {

	String profilesDirectory;
	String entriesDirectory;
	String testEntriesDirectory;
	Hashtable<String,String> bigrams;
	Hashtable<String,String> syntaxBigrams;
	Hashtable<String,String> bigramsPOS;
	Hashtable<String,String> syntaxBigramsPOS;
	Hashtable<String,Hashtable<String,String>> bigramsByRange;
	Hashtable<String,Hashtable<String,String>> syntaxBigramsByRange;
	Hashtable<String,Hashtable<String,String>> bigramsByRangePOS;
	Hashtable<String,Hashtable<String,String>> syntaxBigramsByRangePOS;
	HashSet<String> ngrams;
	Hashtable<String,List<String>> ngramsByRange;
	Hashtable<String,Integer> grams;
	Hashtable<String,Hashtable<String,Integer>> gramsByRange;
	Hashtable<String,Integer> interests;
	Hashtable<String,Hashtable<String,Integer>> interestsByRange;
	//Hashtable<String,Integer> curInterests;
	Hashtable<String,Integer> attributesKeep;
	Hashtable<String,Integer> attributes;
	Hashtable<String,String> blogs;
	Hashtable<String,Hashtable<String,String>> gramAttributes;
	Hashtable<String,Hashtable<String,String>> testGramAttributes;
	Hashtable<String,String> profiles;
	HashSet<String> dictionary;
	HashSet<String> emoticons;
	HashSet<String> acronyms;
	
	// it we are using a separate test set
	Hashtable<String,String> testBlogs;
	
	//int numEntries = 20;
	int numBloggers = 1000;
	int numWords = 1000;
	String outputDirectory;
	String minAge;
	String maxAge;
	String [] ageRanges;
	String curRange;
	
	static final String TAGGER = "/proj/nlp/users/sara/java/input/left3words-wsj-0-18.tagger";
	static final String PARSER = "/proj/nlp/users/sara/java/input/englishPCFG.ser.gz";
	
	/** 
	 * Read all the blogs. Compute Features
	 *
	 */
	public Weka(String oDirectory, String profilesDirectoryIn, String entriesDirectoryIn, String [] ageRangesIn, Hashtable<String,Integer> attributesKeepIn,
			int numBloggersIn, int numWordsIn, String dictionaryFile, String emoticonFile, String acronymFile) {
		this(oDirectory, profilesDirectoryIn, entriesDirectoryIn, ageRangesIn, attributesKeepIn,
			numBloggersIn, numWordsIn, dictionaryFile, emoticonFile, acronymFile, null,0);
	}
	public Weka(String oDirectory, String profilesDirectoryIn, String entriesDirectoryIn, String [] ageRangesIn, Hashtable<String,Integer> attributesKeepIn,
			int numBloggersIn, int numWordsIn, String dictionaryFile, String emoticonFile, String acronymFile, String testEntriesDirectoryIn, int numTestBloggers) {
		
		ageRanges = ageRangesIn;
		outputDirectory = oDirectory;
    	attributesKeep = attributesKeepIn;
   		blogs = new Hashtable<String,String>();
   		testBlogs = new Hashtable<String,String>();
   		bigrams = new Hashtable<String,String>();
   		syntaxBigrams = new Hashtable<String,String>();
   		bigramsPOS = new Hashtable<String,String>();
   		syntaxBigramsPOS = new Hashtable<String,String>();
   		bigramsByRange = new Hashtable<String,Hashtable<String,String>>();
   		syntaxBigramsByRange = new Hashtable<String,Hashtable<String,String>>();
   		bigramsByRangePOS = new Hashtable<String,Hashtable<String,String>>();
   		syntaxBigramsByRangePOS = new Hashtable<String,Hashtable<String,String>>();
   		ngrams = new HashSet<String>();
   		ngramsByRange = new Hashtable<String,List<String>>();
   		grams = new Hashtable<String,Integer>();
   		gramsByRange = new Hashtable<String,Hashtable<String,Integer>>();
   		interests = new Hashtable<String,Integer>();
   		interestsByRange = new Hashtable<String,Hashtable<String,Integer>>();
   		profilesDirectory = profilesDirectoryIn;
   		entriesDirectory = entriesDirectoryIn;
   		testEntriesDirectory = testEntriesDirectoryIn;
   		numBloggers = numBloggersIn;
   		numWords = numWordsIn;
   		
   		// populate list of profiles
   		File [] directories = new File(entriesDirectoryIn).listFiles();
   		
   		for (int index = 0; index < directories.length; index++) {
   			if (!directories[index].getName().equals("profile_list.txt")) continue;
   				
   			try {
	   			BufferedReader in = new BufferedReader(
						new FileReader(directories[index].getPath()));
	   			
	   			String inputLine = "";
	   			
	   			while ((inputLine = in.readLine()) != null) {
		        	blogs.put(inputLine,"");
	   			}
   			} catch (Exception e) {
   				System.err.println("Error processing blogs: " + e);
   				e.printStackTrace();
   			}
   		}
   		
   		Hashtable<String,String> blogsToKeep = new Hashtable<String,String>();
			
		// go through hash to get correct number of bloggers per age group
		for (int index = 0; index < ageRanges.length; index++) {
			Iterator<String> it = blogs.keySet().iterator();
			int count = 0;
			
			while (it.hasNext() && count < numBloggers) {
				String username = (String)it.next();
				//String range = blogs.get(username);
				
				Blog b = Blog.processBlog(profilesDirectory, entriesDirectory, username);
	        	
				if (this.inRange(ageRanges[index], new java.text.SimpleDateFormat("yyyy").format(b.getDOB())) && b.getNumEntries() > 0) {
					blogsToKeep.put(username, ageRanges[index]);
					count++;
				}
			}
		}
		blogs = blogsToKeep;
		
		// if test is true populate list of test blogs
		if (testEntriesDirectoryIn != null) {
	   		directories = new File(testEntriesDirectory).listFiles();
	   		
	   		for (int index = 0; index < directories.length; index++) {
	   			if (!directories[index].getName().equals("profile_list.txt")) continue;
	   				
	   			try {
		   			BufferedReader in = new BufferedReader(
							new FileReader(directories[index].getPath()));
		   			
		   			String inputLine = "";
		   			
		   			while ((inputLine = in.readLine()) != null) {
			        	testBlogs.put(inputLine,"");
		   			}
	   			} catch (Exception e) {
	   				System.err.println("Error processing blogs: " + e);
	   				e.printStackTrace();
	   			}
	   		}
	   		
	   		Hashtable<String,String> testBlogsToKeep = new Hashtable<String,String>();
				
			// go through hash to get correct number of bloggers per age group
			for (int index = 0; index < ageRanges.length; index++) {
				Iterator<String> it = testBlogs.keySet().iterator();
				int count = 0;
				
				while (it.hasNext() && count < numTestBloggers) {
					String username = (String)it.next();
					//String range = blogs.get(username);
					
					Blog b = Blog.processBlog(profilesDirectory, testEntriesDirectory, username);
		        	
					if (this.inRange(ageRanges[index], new java.text.SimpleDateFormat("yyyy").format(b.getDOB())) && b.getNumEntries() > 0) {
						testBlogsToKeep.put(username, ageRanges[index]);
						count++;
					}
				}
			}
			testBlogs = testBlogsToKeep;
		}
		
		// populate emoticons
		emoticons = new HashSet<String>();
		
		try {
   			BufferedReader in = new BufferedReader(
					new FileReader(emoticonFile));
   			
   			String emoticon = "";
   			
   			while ((emoticon = in.readLine()) != null) {
	        	emoticons.add(emoticon.split("\\t")[0]);
   			}
		} catch (Exception e) {
			System.err.println("Error processing emoticons: " + e);
			e.printStackTrace();
		}
		
		// populate acronyms
		acronyms = new HashSet<String>();
		
		try {
   			BufferedReader in = new BufferedReader(
					new FileReader(acronymFile));
   			
   			String acronym = "";
   			
   			while ((acronym = in.readLine()) != null) {
	        	acronyms.add(acronym.toLowerCase());
   			}
		} catch (Exception e) {
			System.err.println("Error processing acronyms: " + e);
			e.printStackTrace();
		}
		
		// populate dictionary
		dictionary = new HashSet<String>();
		try {
   			BufferedReader in = new BufferedReader(
					new FileReader(dictionaryFile));
   			
   			String word = "";
   			
   			while ((word = in.readLine()) != null) {
	        	dictionary.add(word.toLowerCase());
   			}
		} catch (Exception e) {
			System.err.println("Error processing dictionary: " + e);
			e.printStackTrace();
		}
	}
	
	public void addAgeRange(String range, String outputDirectory, int numRanges, String comparer) {
		
		System.err.println("[Weka.addAgeRange] Running age range " + range + ".");
		
		Xtract xtract = null;
		int totalWords = 0;
		
		if (attributesKeep.containsKey("bigrams") || attributesKeep.containsKey("syntaxBigrams") ||
				attributesKeep.containsKey("bigrams-pos") || attributesKeep.containsKey("syntaxBigrams-pos") ||
    			attributesKeep.containsKey("ngrams") || attributesKeep.containsKey("grams")) {
			xtract = new Xtract(PARSER, outputDirectory, "csubj|nn|dobj|amod");
		}
		
		try {
			
			//int i = 0;
			
			Iterator<String> it = blogs.keySet().iterator();
			
			while (it.hasNext()) {
			
				String username = (String)it.next();

				Blog b = Blog.processBlog(profilesDirectory, entriesDirectory, username);
	        	
				if (!this.inRange(range, new java.text.SimpleDateFormat("yyyy").format(b.getDOB()))) continue;
			
				//i++;
				
	        	// 1. process for interests
	        	if (attributesKeep.containsKey("interests")) {
	        		this.processInterests(b, range);
	        	}
	        	if (attributesKeep.containsKey("bigrams") || attributesKeep.containsKey("syntaxBigrams") ||
	        			attributesKeep.containsKey("bigrams-pos") || attributesKeep.containsKey("syntaxBigrams-pos") ||
	        			attributesKeep.containsKey("ngrams") || attributesKeep.containsKey("grams")) {
	        	// 2. add to xtract for future processing
	        		totalWords += this.addToXtract(xtract, b, numWords, comparer);
	        	}
			}
			System.err.println("[Weka.addAgeRange] " + numBloggers + " blogs loaded, " + totalWords +  " words.\nBegin running xtract.");
			
	    	// 3. using xtract, process for ngrams, bigrams, and syntax bigrams
			this.processWords(xtract, range, numRanges, comparer);
		} catch (Exception e) {
			System.err.println("Error occurred in Weka.addAgeRange: " + e);
			e.printStackTrace();
		}
	}
	
	class InterestsComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			return interestsByRange.get(curRange).get(t2).compareTo(interestsByRange.get(curRange).get(t1));
		}
	}
	
	public void printReport(String directory) {

		String string = "";
		
		if (attributesKeep.containsKey("interests")) {
			string += "interests\n\n";
			ArrayList<Object []> its = new ArrayList<Object []>();
			
			for (int a = 0; a < ageRanges.length; a++) {
				curRange = ageRanges[a];

				// interests need to be sorted
				Object [] keys = interestsByRange.get(ageRanges[a]).keySet().toArray();
				Arrays.sort(keys, new InterestsComparator());
				its.add(keys);
				string += ageRanges[a] + ", ";
			}
			string += "\n";

			for (int i = 0; i < attributesKeep.get("interests"); i++) {
				for (int j = 0; j < ageRanges.length; j++) {
					if (i < its.get(j).length && its.get(j)[i]  != null)
						string += its.get(j)[i] + ", ";
				}
				string += "\n";
			}
			string += "\n-------------------------------------\n";
		}
		if (attributesKeep.containsKey("bigrams")) {
			string += "bigrams\n\n";
			@SuppressWarnings("rawtypes")
			Iterator [] its = new Iterator [ageRanges.length];
			
			for (int a = 0; a < ageRanges.length; a++) {
				its[a] = bigramsByRange.get(ageRanges[a]).keySet().iterator();
				string += ageRanges[a] + ", ";
			}
			string += "\n";
			
			for (int i = 0; i < attributesKeep.get("bigrams"); i++) {
				for (int j = 0; j < ageRanges.length; j++) {
					if (its[j].hasNext()) {
						Object key = its[j].next();
						string += "<" + key + "> " +  bigramsByRange.get(ageRanges[j]).get(key) + ", ";
					}
				}
				string += "\n";
			}
			string += "\n-------------------------------------\n";
		}
		if (attributesKeep.containsKey("syntaxBigrams")) {
			string += "syntax bigrams\n\n";
			@SuppressWarnings("rawtypes")
			Iterator [] its = new Iterator [ageRanges.length];
			
			for (int a = 0; a < ageRanges.length; a++) { 
				its[a] = syntaxBigramsByRange.get(ageRanges[a]).keySet().iterator();
				string += ageRanges[a] + ", ";
			}
			string += "\n";
			
			for (int i = 0; i < attributesKeep.get("syntaxBigrams"); i++) {
				for (int j = 0; j < ageRanges.length; j++) {
					if (its[j].hasNext()) {
						Object key = its[j].next();
						string += "<" + key + "> " + syntaxBigramsByRange.get(ageRanges[j]).get(key) +  ", ";
					}
				}
				string += "\n";
			}
			string += "\n-------------------------------------\n";
		}
		if (attributesKeep.containsKey("bigrams-pos")) {
			string += "bigrams pos\n\n";
			@SuppressWarnings("rawtypes")
			Iterator [] its = new Iterator [ageRanges.length];
			
			for (int a = 0; a < ageRanges.length; a++) {
				its[a] = bigramsByRangePOS.get(ageRanges[a]).keySet().iterator();
				string += ageRanges[a] + ", ";
			}
			string += "\n";
			
			for (int i = 0; i < attributesKeep.get("bigrams"); i++) {
				for (int j = 0; j < ageRanges.length; j++) {
					if (its[j].hasNext()) {
						Object key = its[j].next();
						string += "<" + key + "> " +  bigramsByRangePOS.get(ageRanges[j]).get(key) + ", ";
					}
				}
				string += "\n";
			}
			string += "\n-------------------------------------\n";
		}
		if (attributesKeep.containsKey("syntaxBigrams-pos")) {
			string += "syntax bigrams pos\n\n";
			@SuppressWarnings("rawtypes")
			Iterator [] its = new Iterator [ageRanges.length];
			
			for (int a = 0; a < ageRanges.length; a++) { 
				its[a] = syntaxBigramsByRangePOS.get(ageRanges[a]).keySet().iterator();
				string += ageRanges[a] + ", ";
			}
			string += "\n";
			
			for (int i = 0; i < attributesKeep.get("syntaxBigrams"); i++) {
				for (int j = 0; j < ageRanges.length; j++) {
					if (its[j].hasNext()) {
						Object key = its[j].next();
						string += "<" + key + "> " + syntaxBigramsByRangePOS.get(ageRanges[j]).get(key) +  ", ";
					}
				}
				string += "\n";
			}
			string += "\n-------------------------------------\n";
		}
		/*if (attributesKeep.containsKey("ngrams")) {
			string += "ngrams\n\n";
			
			for (int a = 0; a < ageRanges.length; a++) { 
				string += ageRanges[a] + ", ";
			}
			string += "\n";
			
			for (int i = 0; i < attributesKeep.get("ngrams"); i++) {
				for (int j = 0; j < ageRanges.length; j++) {
					if (i < ngramsByRange.get(ageRanges[j]).size())
						string += ngramsByRange.get(ageRanges[j]).get(i) + ", ";
				}
				string += "\n";
			}
			string += "\n-------------------------------------\n";
		}*/
		if (attributesKeep.containsKey("grams")) {
			string += "grams\n\n";
			
			@SuppressWarnings("rawtypes")
			Iterator [] its = new Iterator [ageRanges.length];
			
			for (int a = 0; a < ageRanges.length; a++) {
				its[a] = gramsByRange.get(ageRanges[a]).keySet().iterator();
				string += ageRanges[a] + ", ";
			}
			string += "\n";
			
			for (int i = 0; i < attributesKeep.get("grams"); i++) {
				for (int j = 0; j < ageRanges.length; j++) {
					if (its[j].hasNext()) {
						Object key = its[j].next();
						string += "<" + key + "> " + gramsByRange.get(ageRanges[j]).get(key) + ", ";
					}
				}
				string += "\n";
			}
			string += "\n-------------------------------------\n";
		}
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(directory));
			out.write(string);
			out.close();
		} catch (Exception e) {
			System.err.println("[Weka.printReport] error writing report to file " + directory + ":" + e);
			e.printStackTrace();
		}
	}
	
	private void processInterests(Blog blog, String range) {
		Iterator<?> it = blog.getInterests();
    	
    	while (it.hasNext()) {
    		String interest = (String)it.next();
    		Hashtable<String,Integer> in = interestsByRange.get(range);
    		
    		if (in == null) {
    			in = new Hashtable<String,Integer>();
    			in.put(interest, 1);
    		}
    		else {
    			in.put(interest, in.containsKey(interest) ? in.get(interest) + 1 : 1);
    		}
    		interestsByRange.put(range, in);
    		//curInterests.put(interest, interests.containsKey(interest) ? interests.get(interest) + 1 : 1);
    	}
	}
	
	/**
	 * Get ngrams, bigrams, and syntax bigrams by running xtract
	 */
	private void processWords(Xtract xtract, String range, int numRanges, String comparer) {
		if (!attributesKeep.containsKey("bigrams") && !attributesKeep.containsKey("syntaxBigrams")
				&& !attributesKeep.containsKey("bigrams-pos") && !attributesKeep.containsKey("syntaxBigrams-pos")
				&& !attributesKeep.containsKey("ngrams") && !attributesKeep.containsKey("grams")) return;
		
		xtract.run();
		
		if (attributesKeep.containsKey("bigrams")) {
			bigramsByRange.put(range, xtract.getBigramKeysRegExp(attributesKeep.get("bigrams"), comparer, "text"));
			//if (b != null) bigrams.putAll(b);
		}
		if (attributesKeep.containsKey("syntaxBigrams")) {
			syntaxBigramsByRange.put(range, xtract.getSyntaxBigramKeysRegExp(attributesKeep.get("syntaxBigrams"), comparer, "text"));
			//if (b != null) syntaxBigrams.putAll(b);
		}
		if (attributesKeep.containsKey("bigrams-pos")) {
			bigramsByRangePOS.put(range, xtract.getBigramKeysRegExp(attributesKeep.get("bigrams-pos"), comparer, "pos"));
			//if (b != null) bigrams.putAll(b);
		}
		if (attributesKeep.containsKey("syntaxBigrams-pos")) {
			syntaxBigramsByRangePOS.put(range, xtract.getSyntaxBigramKeysRegExp(attributesKeep.get("syntaxBigrams-pos"), comparer, "pos"));
			//if (b != null) syntaxBigrams.putAll(b);
		}
		if (attributesKeep.containsKey("ngrams")) {
			ngramsByRange.put(range, xtract.getNgrams(attributesKeep.get("ngrams")));
			//if (b != null) ngrams.addAll(b);
		}
		if (attributesKeep.containsKey("grams")) {
			gramsByRange.put(range, xtract.getGramsQuantities(attributesKeep.get("grams"), comparer));
			//if (b != null) grams.addAll(b);
		}
	}
	
	public ArrayList<String> bigrams(int quantity) {
		ArrayList<String> bigrams = new ArrayList<String>();
		return bigrams;
	}
	
	public int addToXtract(Xtract xtract, Blog blog, int numWords, String comparer) {
		
		Iterator<?> it = blog.getEntries();						
		int wordCount = 0;
		
		while (it.hasNext() && wordCount < numWords) {
        	Entry e = (Entry)blog.getEntry((String)it.next());
        	
        	// convert to collocation sentence
        	ArrayList<blog.Sentence> sentences = e.getSentences();
        	ArrayList<collocation.Sentence> xSentences = new ArrayList<collocation.Sentence>(); 
        	
        	for (int index = 0; index < sentences.size(); index++) {
        		Sentence s = sentences.get(index);
        		if (s == null || s.getText() == null) continue;
        		xSentences.add(new collocation.Sentence(s.getSentence(),s.getDependencies(), 
        				comparer.equals("ef") ? e.getURL().toString() : blog.getUsername()));
        		wordCount += s.getNumWords();
        	}
        	
        	xtract.addDocument(xSentences);
		}
		return wordCount;
	}
	
	public void merge(boolean merge) {
		
		if (attributesKeep.containsKey("interests")) {

			 // get subset of each array to be of size attributesKeep.get("interests")
			 for (int index = 0; index < ageRanges.length; index++) {
				 
				 curRange = ageRanges[index];
				 Object [] keys = interestsByRange.get(ageRanges[index]).keySet().toArray();
				 Arrays.sort(keys, new InterestsComparator());
				 
				 Hashtable<String,Integer> subset = new Hashtable<String,Integer>();
				 
				 for (int interestIndex = 0; interestIndex < keys.length && interestIndex < attributesKeep.get("interests"); interestIndex++) {
					 subset.put((String)keys[interestIndex], interestsByRange.get(ageRanges[index]).get(keys[interestIndex]));
				 }
				 interestsByRange.put(ageRanges[index], subset);
			 }
			
			 for (int index = 0; index < ageRanges.length; index++) {
				 
				 curRange = ageRanges[index];
				 Object [] keys = interestsByRange.get(ageRanges[index]).keySet().toArray();
				 Arrays.sort(keys, new InterestsComparator());
				 
				 for (int i = 0; i < keys.length; i++) {
					 
					 Object interest = keys[i];
					 
					 boolean unique = true;
					 for (int j = 0; j < ageRanges.length && merge; j++) {
						 if (j != index && interestsByRange.get(ageRanges[j]).containsKey(interest)) {
							 unique = false;
							 break;
						 }
					 }
					 if (unique) interests.put((String)interest, interestsByRange.get(ageRanges[index]).get(interest));
				 }
			 }
		 }
		if (attributesKeep.containsKey("grams")) {
			for (int index = 0; index < ageRanges.length; index++) {
				 
				 Hashtable<String,Integer> g = gramsByRange.get(ageRanges[index]);
				 Object [] keys = g.keySet().toArray();
				 
				 for (int i = 0; i < g.size(); i++) {
					 
					 Object gram = keys[i];
					 boolean unique = true;
					 for (int j = 0; j < ageRanges.length && merge; j++) {
						 if (j != index && gramsByRange.get(ageRanges[j]).containsKey(gram)) {
							 unique = false;
							 break;
						 }
					 }
					 if (unique) grams.put((String)gram, g.get(gram));
				 }
			 }
		}
		if (attributesKeep.containsKey("bigrams")) {
			this.submerge(merge, bigramsByRange, bigrams);
		}
		if (attributesKeep.containsKey("syntaxBigrams")) {
			 this.submerge(merge, syntaxBigramsByRange, syntaxBigrams);
		}
		if (attributesKeep.containsKey("bigrams-pos")) {
			this.submerge(merge, bigramsByRangePOS, bigramsPOS);
		}
		if (attributesKeep.containsKey("syntaxBigrams-pos")) {
			 this.submerge(merge, syntaxBigramsByRangePOS, syntaxBigramsPOS);
		}
	}
	
	private void submerge(boolean merge, Hashtable<String,Hashtable<String,String>> ranges, Hashtable<String,String> merged) {
		for (int index = 0; index < ageRanges.length; index++) {
			 
			 Hashtable<String,String> bg = ranges.get(ageRanges[index]);
			 Object [] keys = bg.keySet().toArray();
			 
			 for (int i = 0; i < bg.size(); i++) {
				 
				 Object bigram = keys[i];
				 boolean unique = true;
				 for (int j = 0; j < ageRanges.length && merge; j++) {
					 if (j != index && ranges.get(ageRanges[j]).containsKey(bigram)) {
						 unique = false;
						 break;
					 }
				 }
				 if (unique) merged.put((String)bigram, bg.get(bigram));
			 }
		 }
	}
	
	public Hashtable<String,Integer> interests(int quantity) {
		return interests;
	}
	
	public Hashtable<String,Integer> getAttributes() {
		 attributes = new Hashtable<String,Integer>();
		 
		 int index = 0;
		 // id
		 //attributes.put("USERNAME STRING u",index++);
		 // friend count
		 if (attributesKeep.containsKey("friends"))
			 attributes.put("FRIENDS NUMERIC f",index++);
		 // post count
		 if (attributesKeep.containsKey("posts"))
			 attributes.put("POSTS NUMERIC p",index++);
		 //interests
		 if (attributesKeep.containsKey("interests")) {
			 Iterator<String> it = interests.keySet().iterator();
			 int count = 0;			 
			 while (it.hasNext() && count < attributesKeep.get("interests")) {
				 String key = (String)it.next();
				 key = "'" + key.replaceAll("'", "\\\\'") + "' NUMERIC i";
				 attributes.put(key,index++);
				 count++;
			 }
		 }
		 // grams
		 if (attributesKeep.containsKey("grams") && !grams.isEmpty()) {
			 int count = 0;
			 Iterator<String> it = grams.keySet().iterator();
			 while (it.hasNext() && count < attributesKeep.get("grams")) {
				 String key = "'" + ((String)it.next()).replaceAll("'", "\\\\'") + "' NUMERIC g";
				 attributes.put(key,index++);
				 count++;
			 }
		 }
		 // bigrams
		 if (attributesKeep.containsKey("bigrams") && !bigrams.isEmpty()) {
			 Iterator<String> it = bigrams.keySet().iterator();
			 int count = 0;
			 
			 while (it.hasNext() && count < attributesKeep.get("bigrams")) {
				 String key = "'" + ((String)it.next()).replaceAll("'", "\\\\'") + "' NUMERIC b";
				 attributes.put(key,index++);
				 count++;
			 }
		 }
		 // syntax bigrams
		 if (attributesKeep.containsKey("syntaxBigrams") && !syntaxBigrams.isEmpty()) {
			 Iterator<String> it = syntaxBigrams.keySet().iterator();
			 int count = 0;
			 
			 while (it.hasNext() && count < attributesKeep.get("syntaxBigrams")) {
				 String key = "'" + ((String)it.next()).replaceAll("'", "\\\\'") + "' NUMERIC s";
				 attributes.put(key,index++);
				 count++;
			 }
		 }
		 // bigrams pos
		 if (attributesKeep.containsKey("bigrams-pos") && !bigramsPOS.isEmpty()) {
			 Iterator<String> it = bigramsPOS.keySet().iterator();
			 int count = 0;
			 
			 while (it.hasNext() && count < attributesKeep.get("bigrams-pos")) {
				 String key = "'" + ((String)it.next()).replaceAll("'", "\\\\'") + "' NUMERIC 1";
				 attributes.put(key,index++);
				 count++;
			 }
		 }
		 // syntax bigrams pos
		 if (attributesKeep.containsKey("syntaxBigrams-pos") && !syntaxBigramsPOS.isEmpty()) {
			 Iterator<String> it = syntaxBigramsPOS.keySet().iterator();
			 int count = 0;
			 
			 while (it.hasNext() && count < attributesKeep.get("syntaxBigrams-pos")) {
				 String key = "'" + ((String)it.next()).replaceAll("'", "\\\\'") + "' NUMERIC 2";
				 attributes.put(key,index++);
				 count++;
			 }
		 }
		 // ngrams
		 if (attributesKeep.containsKey("ngrams") && !ngrams.isEmpty()) {
			 Iterator<String> it = ngrams.iterator();
			 int count = 0;

			 while (it.hasNext() && count < attributesKeep.get("ngrams")) {
				 String key = "'" + ((String)it.next()).replaceAll("'", "\\\\'") + "' NUMERIC n";
				 attributes.put(key,index++);
				 count++;
			 }
		 }
 
		 if (attributesKeep.containsKey("capitalization"))
			 attributes.put("CAPITALIZATION REAL C", index++);
		 if (attributesKeep.containsKey("links"))
			 attributes.put("LINKS REAL l",index++);
		 if (attributesKeep.containsKey("sentenceLength"))
			 attributes.put("SENTENCELENGTH REAL s",index++);				
		 // misspellings
		 if (attributesKeep.containsKey("misspellings"))
			 attributes.put("MISSPELLINGS REAL m",index++);
		 // emoticons
		 if (attributesKeep.containsKey("emoticons"))
			 attributes.put("EMOTICONS REAL :",index++);
		 // acronyms
		 if (attributesKeep.containsKey("acronyms"))
			 attributes.put("ACRONYMS REAL l",index++); 	 
		 // punctuation
		 if (attributesKeep.containsKey("punctuation"))
			 attributes.put("PUNCTUATION REAL .",index++);
		 
		 if (attributesKeep.containsKey("comments"))
			 attributes.put("COMMENTS NUMERIC c",index++);			
		 if (attributesKeep.containsKey("entries"))
			 attributes.put("ENTRIES NUMERIC e",index++);		
		 if (attributesKeep.containsKey("avgComments"))
			 attributes.put("AVGCOMMENTS REAL a",index++);
		 if (attributesKeep.containsKey("avgDay"))
			 attributes.put("AVGDAY REAL a",index++);
		 if (attributesKeep.containsKey("avgTime"))
			 attributes.put("AVGTIME REAL a",index++);
		 if (attributesKeep.containsKey("modeDay"))
			 attributes.put("COMMONDAY NUMERIC c",index++);
		 if (attributesKeep.containsKey("modeTime"))
			 attributes.put("COMMONTIME NUMERIC c",index++);
			 
		 // label
		 String label = "";
		 for (int i = 0; i < ageRanges.length; i++) {
			 label += i + ", ";
		 }
		 attributes.put("YEAR {" + label + "} y",index++);
		 // show year as label: attributes.put("YEAR NUMERIC y",index++);
		 
		 return attributes;
	}
	
	private int getRange(String y) {
		
		int year = Integer.valueOf(y);
		
		for (int index = 0; index < ageRanges.length; index++) {
			int dash = ageRanges[index].indexOf("-");
			int start = Integer.valueOf(ageRanges[index].substring(0, dash));
			int end = Integer.valueOf(ageRanges[index].substring(dash + 1));
			
			if (year >= start && year <= end) return index;
		}
		return -1;
	}
	
	private boolean inRange(String range, String y) {
		
		int year = Integer.valueOf(y);
		int dash = range.indexOf("-");
		int start = Integer.valueOf(range.substring(0, dash));
		int end = Integer.valueOf(range.substring(dash + 1));
			
		if (year >= start && year <= end) return true;
		return false;
	}
	
	public void computeGramData(Hashtable<String,Integer> attributes) {
		gramAttributes = this.computeGramData(attributes,blogs,entriesDirectory);
		testGramAttributes = this.computeGramData(attributes,testBlogs,testEntriesDirectory);
	}
	
	public Hashtable<String,Hashtable<String,String>> computeGramData(Hashtable<String,Integer> attributes, Hashtable<String,String> blogsToUse, String directory) {
		Hashtable<String,Hashtable<String,String>> gramAttributesToPopulate = new Hashtable<String,Hashtable<String,String>>();
		
		Iterator<String> usernames = blogsToUse.keySet().iterator();
		
		int processed = 0;
		
		while (usernames.hasNext()) {
			
			processed++;
			if (processed % 100 == 0) {
				System.err.println("[Weka.computeGramData] Processed " + processed + "/" + blogsToUse.size() + " blogs at " + new Date());
			}
			
			String username = usernames.next();
			// # of words that are not in dictionary (misspellings/slang)
			double misspellings = 0;
			// # of emoticons
			double numEmoticons = 0;
			// # of acronyms
			double numAcronyms = 0;
			// punctuation (excluding emoticons)
			double punctuation = 0;
			 //refers to [LINK] and [IMAGE]
			double numLinks = 0;
			// HAHA
			double numCapitalWords = 0;
		
			Blog b = Blog.processBlog(profilesDirectory, directory, username);
			
			Iterator<?> it = b.getEntries();
			
			Hashtable<String,Integer> attributeCount = new Hashtable<String,Integer>();
			
			while (it.hasNext()) {
				
				Entry et = (Entry)b.getEntry((String)it.next());
				String entry = et.getEntryText();
				String posEntry = et.getEntry();

				Iterator<String> attributeIterator = attributes.keySet().iterator();
				
				while (attributeIterator.hasNext()) {
					
					String key = (String)attributeIterator.next();
					char type = key.charAt(key.length()-1);
					if (type == 'b' || type == 's'
						|| type == 'g' || type == 'n' || type == '1' || type == '2') {
						
							String attribute = key.substring(1,key.indexOf("' NUMERIC"));
							boolean pos = false;
														
							try {
								
								if (type == 's' || type == '2' || type == 'b' || type == '1') {
									switch(type) {
										case 'b':
											attribute = bigrams.get(attribute.replaceAll("\\\\'", "'"));
											break;
										case '1':
											attribute = bigramsPOS.get(attribute.replaceAll("\\\\'", "'"));
											break;
										case 's':
											attribute = syntaxBigrams.get(attribute.replaceAll("\\\\'", "'"));
											break;
										case '2':
											attribute = syntaxBigramsPOS.get(attribute.replaceAll("\\\\'", "'"));
											break;
									}
								
									attribute = attribute.substring(attribute.indexOf("-") + 1);
									
									if (attribute.indexOf("\\p{Graph}+/") >= 0 || attribute.indexOf("/\\p{Graph}+") >= 0) {
										pos = true;
									}
								}
								
								Pattern p = Pattern.compile("(^| )" + attribute + "( |$)",Pattern.CASE_INSENSITIVE);
								Matcher m = p.matcher(pos ? posEntry.toLowerCase() : entry.toLowerCase());
								
								int count = 0;
								
								while (m.find()) count++;

								if (count > 0) {
									attributeCount.put(key, attributeCount.containsKey(key) ? attributeCount.get(key) + count : count);
									//System.err.println("[Weka.computeGramData()] " + count + " matches for " + attribute + " in " + b.getUsername());
								}
							} catch (Exception e) {
								System.err.println("[Weka.computeGramData] Error occurred in finding match for \"" + key.substring(1,key.indexOf("' NUMERIC")) + "\" in entry " + entry + "\n: " + e);
								e.printStackTrace();
							}
						}
					
				}
				
				// other entry related computations
				String [] words = entry.split(" ");
				
				double na = 0;
				double punct = 0;
				double emot = 0;
				double acr = 0;
				double links = 0;
				double caps = 0;
				
				for (int index = 0; index < words.length; index++) {
					
					if (words[index].contains("[LINK]") || words[index].contains("[IMAGE]")) {
						links++;
						if (words[index].replaceAll("\\[(LINK|IMAGE)\\]", "").equals("")) continue;
						words[index] = words[index].replaceAll("\\[(LINK|IMAGE)\\]","");
					}
					
					// to capture normal punctuation such as: . and ,
					if (words[index].length() > 1 && !words[index].matches("\\p{Punct}+") 
							&& String.valueOf(words[index].charAt(words[index].length()-1)).matches("\\p{Punct}")) {
						words[index] = words[index].replaceFirst("\\p{Punct}+$", "");
						punct++;
					}
					
					// at least two letters to avoid I,A
					if (words[index].matches("[A-Z][A-Z']+")) caps++;
					
					if (emoticons.contains(words[index])) 
						emot++;
					else if (acronyms.contains(words[index].toLowerCase())) 
						acr++;
					else if (words[index].toLowerCase().matches("\\p{Punct}+")) punct++;
					else if (dictionary.contains(words[index].replaceAll("\\p{Punct}","").toLowerCase())
							|| dictionary.contains(words[index].toLowerCase())
							|| words[index].matches("[0-9]+")) continue;					
					else na++;
				}
				
				punctuation += punct; ///words.length;
				misspellings += na; ///words.length;
				numEmoticons += emot; ///words.length;
				numAcronyms += acr; ///words.length;
				numLinks += links; ///words.length;
				numCapitalWords += caps; ///words.length;
			}	
			
			gramAttributesToPopulate.put(username,new Hashtable<String,String>());
			
			Iterator<String> keys = attributeCount.keySet().iterator();
			
			while (keys.hasNext()) {
				String key = (String)keys.next();
				if (attributes.get(key) == null) {
					System.err.println("[Weka.computeGramData] no attribute for " + key);
					continue;
				}
				if (attributeCount.get(key) > 0) {
					gramAttributesToPopulate.get(username).put(key + " " + attributeCount.get(key),String.valueOf(key.charAt(key.length()-1)));
				}
			}
			
			gramAttributesToPopulate.get(username).put("PUNCTUATION REAL .",String.valueOf(punctuation/b.getNumEntries()));
			gramAttributesToPopulate.get(username).put("MISSPELLINGS REAL .",String.valueOf(misspellings/b.getNumEntries()));
			gramAttributesToPopulate.get(username).put("EMOTICONS REAL .",String.valueOf(numEmoticons/b.getNumEntries()));
			gramAttributesToPopulate.get(username).put("ACRONYMS REAL .",String.valueOf(numAcronyms/b.getNumEntries()));
			gramAttributesToPopulate.get(username).put("LINKS REAL .",String.valueOf(numLinks/b.getNumEntries()));
			gramAttributesToPopulate.get(username).put("CAPITALIZATION REAL .",String.valueOf(numCapitalWords/b.getNumEntries()));
		}
		return gramAttributesToPopulate;
	}
	
	public ArrayList<String> getData(Hashtable<String,Integer> attributes, boolean test) {
		
		ArrayList<String> data = new ArrayList<String>();
		
		Iterator<String> usernames = test ? testBlogs.keySet().iterator() : blogs.keySet().iterator();
		
		//int processed = 0;
		
		while (usernames.hasNext()) {
			
			/*processed++;
			if (processed % 100 == 0) {
				System.err.println("[Weka.getData] Processed " + processed + "/" + blogs.size() + " blogs at " + new Date());
			}*/
			
			String username = usernames.next();
			//String range = blogs.get(username); 
		
			Blog b = Blog.processBlog(profilesDirectory, test ? testEntriesDirectory : entriesDirectory, username);

			String string = "";
			 
			// friend count
			if (attributesKeep.containsKey("friends")) {
				string += attributes.get("FRIENDS NUMERIC f") + " " + b.getFriendCount() + ", ";
			}
			// post count
			if (attributesKeep.containsKey("posts")) {
				string += attributes.get("POSTS NUMERIC p") + " " + b.getLifetimePostCount() + ", ";
			}

			ArrayList<String> values = new ArrayList<String>();
			
			//interests				
			if (attributesKeep.containsKey("interests")) {
				 Iterator<?> interestIterator = b.getInterests();
				 
				 while (interestIterator.hasNext()) {
					 String interest = ((String)interestIterator.next()).replaceAll("'", "\\\\'");
					 if (attributes.containsKey("'" + interest + "' NUMERIC i")) {
						 values.add(attributes.get("'" + interest + "' NUMERIC i") + " 1");
					 }
				 }
			}

			Iterator<String> gramit = test ? testGramAttributes.get(username).keySet().iterator() : gramAttributes.get(username).keySet().iterator();
			
			while (gramit.hasNext()) {
				
				String key = (String)gramit.next();
				String type = test ? (String)testGramAttributes.get(username).get(key) : (String)gramAttributes.get(username).get(key); 
				
				if (type.equals("b") && attributesKeep.containsKey("bigrams") || 
						type.equals("s") && attributesKeep.containsKey("syntaxBigrams") ||
						type.equals("1") && attributesKeep.containsKey("bigrams-pos") || 
						type.equals("2") && attributesKeep.containsKey("syntaxBigrams-pos") ||
						type.equals("g") && attributesKeep.containsKey("grams")) {
					values.add(attributes.get(key.substring(0,key.lastIndexOf(" "))) + key.substring(key.lastIndexOf(" ")));
				}
			}
			
			Collections.sort(values,new NumberComparator());
			
			if (values.size() > 0) {
				string += values.get(0);
				
				for (int index = 1; index < values.size(); index++) {
					string += ", " + values.get(index);
				}
			}
			
			
//			 total # of comments, entries, average # of comments
			int numComments = 0;
			int numEntries = 0;
			double numSentences = 0;
			double totalSentenceLength = 0;
			
			// average time & day post
			int [] days = new int[7];
			// most popular time & day of post
			int [] times = new int[24];
			
			/*// # of words that are not in dictionary (misspellings/slang)
			double misspellings = 0;
			// # of emoticons
			double numEmoticons = 0;
			// # of acronyms
			double numAcronyms = 0;
			// punctuation (excluding emoticons)
			double punctuation = 0;
			
			// new things!!
			
			double numLinks = 0; //refers to [LINK] and [IMAGE]
			double numCapitalWords = 0;*/
			
			Iterator<?> it = b.getEntries();
			
			while (it.hasNext()) {
				
				Entry et = (Entry)b.getEntry((String)it.next());
				numEntries++;
				numComments += et.getNumComments();
				Calendar date = Calendar.getInstance();
				date.setTime(et.getDate());
				
				if (date.get(Calendar.DAY_OF_WEEK) > 0)
					days[date.get(Calendar.DAY_OF_WEEK)-1]++;
				if (date.get(Calendar.HOUR_OF_DAY) > 0) 
					times[date.get(Calendar.HOUR_OF_DAY)-1]++;
				
				String entryText = et.getEntryText();
				totalSentenceLength += entryText.length();
				numSentences += et.getSize();
				
				// check each word to see if it is in the dictionary
				/*String [] words = entryText.split(" ");
				
				
				double na = 0;
				double punct = 0;
				double emot = 0;
				double acr = 0;
				double links = 0;
				double caps = 0;
				
				for (int index = 0; index < words.length; index++) {
					
					if (words[index].contains("[LINK]") || words[index].contains("[IMAGE]")) {
						links++;
						if (words[index].replaceAll("\\[(LINK|IMAGE)\\]", "").equals("")) continue;
						words[index] = words[index].replaceAll("\\[(LINK|IMAGE)\\]","");
					}
					
					// to capture normal punctuation such as: . and ,
					if (words[index].length() > 1 && !words[index].matches("\\p{Punct}+") 
							&& String.valueOf(words[index].charAt(words[index].length()-1)).matches("\\p{Punct}")) {
						words[index] = words[index].replaceFirst("\\p{Punct}+$", "");
						punct++;
					}
					
					// at least two letters to avoid I,A
					if (words[index].matches("[A-Z][A-Z']+")) caps++;
					
					if (emoticons.contains(words[index])) 
						emot++;
					else if (acronyms.contains(words[index].toLowerCase())) 
						acr++;
					else if (words[index].toLowerCase().matches("\\p{Punct}+")) punct++;
					else if (dictionary.contains(words[index].replaceAll("\\p{Punct}","").toLowerCase())
							|| dictionary.contains(words[index].toLowerCase())
							|| words[index].matches("[0-9]+")) continue;					
					else na++;
				}
				punctuation += punct; ///words.length;
				misspellings += na; ///words.length;
				numEmoticons += emot; ///words.length;
				numAcronyms += acr; ///words.length;
				numLinks += links; ///words.length;
				numCapitalWords += caps; ///words.length;*/
			}
			
			
			
			// capital words
			if (attributesKeep.containsKey("capitalization")) {
				string += ", " + attributes.get("CAPITALIZATION REAL C") + " " + 
					(test ? testGramAttributes.get(username).get("CAPITALIZATION REAL .") : gramAttributes.get(username).get("CAPITALIZATION REAL ."));
			}
				
			// links and images
			if (attributesKeep.containsKey("links")) {
				string += ", " + attributes.get("LINKS REAL l") + " " + 
					(test ? testGramAttributes.get(username).get("LINKS REAL .") : gramAttributes.get(username).get("LINKS REAL ."));
			}
			// sentence length
			if (attributesKeep.containsKey("sentenceLength")) {
				string += ", " + attributes.get("SENTENCELENGTH REAL s") + " " + (totalSentenceLength/numSentences);
			}
		
			// # of words that are not in dictionary - use unix dictionary (/usr/share/dict/words)
			if (attributesKeep.containsKey("misspellings")) {
				string += ", " + attributes.get("MISSPELLINGS REAL m") + " " +
					(test ? testGramAttributes.get(username).get("MISSPELLINGS REAL .") : gramAttributes.get(username).get("MISSPELLINGS REAL ."));
			}
			
			// # of emoticons
			if (attributesKeep.containsKey("emoticons")) {
				string += ", " + attributes.get("EMOTICONS REAL :") + " " +
					(test ? testGramAttributes.get(username).get("EMOTICONS REAL .") : gramAttributes.get(username).get("EMOTICONS REAL ."));
			}
			
			// # of acronyms
			if (attributesKeep.containsKey("acronyms")) {
				string += ", " + attributes.get("ACRONYMS REAL l") + " " +
					(test ? testGramAttributes.get(username).get("ACRONYMS REAL .") : gramAttributes.get(username).get("ACRONYMS REAL ."));
			}
			
			// # of punctuation (excluding emoticons)
			if (attributesKeep.containsKey("punctuation")) {
				string += ", " + attributes.get("PUNCTUATION REAL .") + " " +
					(test ? testGramAttributes.get(username).get("PUNCTUATION REAL .") : gramAttributes.get(username).get("PUNCTUATION REAL ."));
			}
			
			// total # of comments
			if (attributesKeep.containsKey("comments"))
				string += ", " + attributes.get("COMMENTS NUMERIC c") + " " + numComments; 
			
			// total # of entries
			if (attributesKeep.containsKey("entries"))
				string += ", " + attributes.get("ENTRIES NUMERIC e") + " " + numEntries;
			
			// average # of comments
			if (attributesKeep.containsKey("avgComments"))
				string += ", " + attributes.get("AVGCOMMENTS REAL a") + " " + ((double)numComments/numEntries);
			
			// average time & day post
			if (attributesKeep.containsKey("avgDay")) {
				
				int total = 0;
				
				for (int i = 0; i < 7; i++) {
					total += days[i] * (i+1);
				}
				
				string += ", " + attributes.get("AVGDAY REAL a") + " " + ((double)total/numEntries);
			}
			if (attributesKeep.containsKey("avgTime")) {
				
				int total = 0;
				
				for (int i = 0; i < 24; i++) {
					total += times[i] * (i+1);
				}
				
				string += ", " + attributes.get("AVGTIME REAL a") + " " + ((double)total/numEntries);
			}
			
			// most popular time & day of post
			if (attributesKeep.containsKey("modeDay")) {
				
				int max = days[0];
				int day = 0; 
				
				for (int i = 1; i < 7; i++) {
					if (days[i] > max) {
						max = days[i];
						day = i;
					}
				}
				
				string += ", " + attributes.get("COMMONDAY NUMERIC c") + " " + day;
			}
			if (attributesKeep.containsKey("modeTime")) {
				
				int max = times[0];
				int time = 0;
				
				for (int i = 0; i < 24; i++) {
					if (times[i] > max) {
						max = times[i];
						time = i;
					}
				}
				
				string += ", " + attributes.get("COMMONTIME NUMERIC c") + " " +  time;
			}
			
			//	label
			string += ", " + (attributes.size() - 1) + " " + this.getRange(new java.text.SimpleDateFormat("yyyy").format(b.getDOB()));
			//show year as label: string += ", " + (attributes.size() - 1) + " " + new java.text.SimpleDateFormat("yyyy").format(b.getDOB());
			data.add(string);
		}
		return data;
	}
	
	class NumberComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			String s1 = (String)t1;
			String s2 = (String)t2;
			return Integer.valueOf(s1.substring(0,s1.indexOf(" "))).compareTo(Integer.valueOf(s2.substring(0,s2.indexOf(" "))));
		}
	}
	
	class AttributeComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			return attributes.get(t1).compareTo(attributes.get(t2));
		}
	}
	
	public String toString(boolean test) {
		String string = "@RELATION LIVEJOURNAL\n\n";
		Hashtable<String,Integer> attributes = this.getAttributes();
		
		Object [] keys = attributes.keySet().toArray();
				
		Arrays.sort(keys, new AttributeComparator());
		
		for (int index = 0; index < keys.length; index++) {
			//string += "@ATTRIBUTE " keys[index].toString().substring(0,keys[index].toString().length()-2) + "\n";
		
			if ( keys[index].toString().startsWith("'")) {
				string += "@ATTRIBUTE '" + keys[index].toString().substring(keys[index].toString().length()-2).trim() + keys[index].toString().substring(1,keys[index].toString().length()-2) + "\n";
			}
			else {
				string += "@ATTRIBUTE " + keys[index].toString().substring(keys[index].toString().length()-2).trim() + keys[index].toString().substring(0,keys[index].toString().length()-2) + "\n";
			}
		}
		System.err.println("[Weka.toString] generated list of " + keys.length + " attributes.");
		
		string += "\n@DATA\n";
		
		ArrayList <String> data = this.getData(attributes,test);
		
		for (int index = 0; index < data.size(); index++) {
			String item = data.get(index).trim();
			string += "{" + item + "}\n";
		}
		
		System.err.println("[Weka.toString] generated data for " + data.size() + " blogs.");
		
		return string;
	}
	
	public String toString(Hashtable<String,Integer> attributesSet, boolean test) {
		Hashtable<String,Integer> temp = attributesKeep;
		attributesKeep = attributesSet;
		String string = this.toString(test);
		attributesKeep = temp;
		return string;
	}
	
	public void addFeatures(Hashtable<String,Integer> usedAttributes, String features, int numFeatures) {
		for (int index = 0; index < features.length(); index++) {
			switch (features.charAt(index)) {
				case 'i':
					usedAttributes.put("interests",numFeatures);
					break;
				case 'f':	
					usedAttributes.put("friends",0);
					break;
				case 'p':
					usedAttributes.put("posts",0);
					break;
				case 'g':	
					usedAttributes.put("grams",numFeatures);
					break;
				case 'b':
					usedAttributes.put("bigrams",numFeatures);
					break;
				case 's':	
					usedAttributes.put("syntaxBigrams",numFeatures);
					break;
				case '1':
					usedAttributes.put("bigrams-pos",numFeatures);
					break;
				case '2':	
					usedAttributes.put("syntaxBigrams-pos",numFeatures);
					break;					
				case 'c':	
					usedAttributes.put("comments",0);
					usedAttributes.put("avgComments",0);
					break;
				case 'e':	
					usedAttributes.put("entries",0);
					break;
				case 't':
					usedAttributes.put("avgDay",0);
					usedAttributes.put("avgTime",0);
					//usedAttributes.put("modeDay",0);
					//usedAttributes.put("modeTime",0);
					break;
				case 'm':
					usedAttributes.put("misspellings", 0);
					break;
				case ':':
					usedAttributes.put("emoticons", 0);
					break;
				case 'a':
					usedAttributes.put("acronyms", 0);
					break;
				case '.':
					usedAttributes.put("punctuation", 0);
					break;
				case 'l':
					usedAttributes.put("links",0);
					break;
				case 'k':
					usedAttributes.put("capitalization",0);
					break;
				case '0':
					usedAttributes.put("sentenceLength",0);
					break;
			}	
		}
	}
	
	
	public static void main(String[] args) {

		boolean merge = false;
		String [] ages = {"1940-1968","1969-1978","1979-1988","1989-2009"};
		String experimentName = "";
		int numWords = 1000; //1333;
		int numBloggers = 1000;
		int numFeatures = 200;
		int numBloggersTest = 250;
		String comparer = "tf";
		String dictionaryFile = "/usr/share/dict/words";
		String emoticonFile = "/proj/nlp/users/sara/corpora/sentiment/emoticons/emoticons.txt";
		String acronymFile = "/proj/nlp/users/sara/corpora/sentiment/emoticons/acronyms.txt";
		
		System.err.println("START: " + new Date());
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-a")) {
				ages = args[++i].split(",");
			}
			else if (args[i].equals("-name")) {
				experimentName = args[++i] + "-" + experimentName; 
			}
			else if (args[i].endsWith("-merge")) {
				experimentName += "merge-";
				merge = true;
			}
			else if (args[i].endsWith("-nomerge")) {
				merge = false;
			}
			// tf, ef, bf, tfidf
			else if (args[i].equals("-c")) {
				comparer = args[++i];
				experimentName += comparer + "-";
				
			}
			else if (args[i].equals("-nw")) {
				numWords = Integer.valueOf(args[++i]);
			}
			else if (args[i].equals("-nb")) {
				numBloggers = Integer.valueOf(args[++i]);
			}
			else if (args[i].equals("-nbt")) {
				numBloggersTest = Integer.valueOf(args[++i]);
			}
			else if (args[i].equals("-nf")) {
				numFeatures = Integer.valueOf(args[++i]);
				experimentName += numFeatures + "-";
			}
			else if (args[i].equals("-d")) {
				dictionaryFile = args[++i];
			}
			else if (args[i].equals("-e")) {
				emoticonFile = args[++i];
			}
			else if (args[i].equals("-acronyms")) {
				acronymFile = args[++i];
			}
		}
		
		experimentName = experimentName.substring(0,experimentName.length()-1);
		
		System.err.println("SETTINGS: ");
		System.err.println("name: " + experimentName);
		System.err.print("ages: ");
		for (int i = 0; i < ages.length; i++) {
			System.err.print(ages[i] + " ");
		}
		System.err.println("\nmerge: " + merge);
		System.err.println("comparer: " + comparer);
		System.err.println("num words: " + numWords);
		System.err.println("num bloggers: " + numBloggers);
		System.err.println("num test bloggers: " + numBloggersTest);
		System.err.println("num features: " + numFeatures);
		
		String directory = "/proj/nlp/users/sara/corpora/blogs/livejournal/";

		Hashtable<String,Integer> attributes = new Hashtable<String,Integer>();
		attributes.put("interests",numFeatures);
		attributes.put("friends",0);
		attributes.put("posts",0);
		attributes.put("grams",numFeatures);
		//attributes.put("ngrams",200);
		attributes.put("bigrams",numFeatures);
		attributes.put("syntaxBigrams",numFeatures);
		attributes.put("bigrams-pos",numFeatures);
		//attributes.put("syntaxBigrams-pos",numFeatures);
		attributes.put("comments",0);
		attributes.put("entries",0);
		attributes.put("avgComments",0);
		attributes.put("avgDay",0);
		attributes.put("avgTime",0);
		//attributes.put("modeDay",0);
		//attributes.put("modeTime",0);
		attributes.put("misspellings", 0);
		attributes.put("emoticons", 0);
		attributes.put("acronyms",0);
		
		Weka weka = new Weka(directory + "output", directory + "original-corpus", directory + "subcorpus/", 
				ages, attributes, numBloggers, numWords, dictionaryFile, emoticonFile, acronymFile,directory + "subcorpus-test",numBloggersTest);
		
		for (int index = 0; index < ages.length; index++) {
			weka.addAgeRange(ages[index], directory + "output/" + ages[index], ages.length, comparer);
			System.err.println("[Weka.main] added age range " + ages[index]);
		}

		// 4. get unique words by merging ranges
		weka.merge(merge);
		
		// 5. print out detailed report
		weka.printReport(directory + "logs/report-" + experimentName + ".txt");
		
		// 6. print out arff files with different permutation of attributes
		try {
			
			String output = directory + "output/" + experimentName + "/results/";
			
			// just do this once because it's time consuming
			weka.computeGramData(weka.getAttributes());
			
			String featureTests [] = {":","m",".","c","e","t","f","p","b","g","1","s","i","a","0","l","k",
					":m.cetfpbsg1ia0lk",":m.cetfpia0lk","cetfp","cetfpg",":m.cetfpbia0lk",":m.cetfpgia0lk",":m.cetfpbg1ia0lk"};
			
			for (int index = 0; index < featureTests.length; index++) {
					attributes = new Hashtable<String,Integer>();
					weka.addFeatures(attributes, featureTests[index], numFeatures);
					System.err.println("[Weka.main] starting to run results for data-" + featureTests[index] + " at " + new Date());
					new File(output).mkdirs();
					
					// train
					BufferedWriter out = new BufferedWriter(new FileWriter(output + "data-" + featureTests[index] + ".arff"));
					out.write(weka.toString(attributes,false));
					out.close();					
					System.err.println("[Weka.main] completed results for data-" + featureTests[index] + " at " + new Date());
					
					// test
					out = new BufferedWriter(new FileWriter(output + "data-test-" + featureTests[index] + ".arff"));
					out.write(weka.toString(attributes,true));
					out.close();					
					System.err.println("[Weka.main] completed results for data-test-" + featureTests[index] + " at " + new Date());
					
					try {
						String all [] = {"-t",output + "data-" + featureTests[index] + ".arff","-T", output + "data-test-" + featureTests[index] + ".arff","-i"};
						System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(output + "data-" + featureTests[index] + ".out")),true));
						Logistic.main(all);
						System.err.println("[Weka.main] completed running weka for data-" + featureTests[index] + " at " + new Date());
					} catch (Exception e) {
						System.err.println("[Weka.main] error running weka: " + e);
						e.printStackTrace();
					}
			}

		} catch (Exception e) {
			System.err.println("[Weka.main] Error occurred in writing to file: " + e);
			e.printStackTrace();
		}
		
		System.err.println("END: " + new Date());
	}
}
