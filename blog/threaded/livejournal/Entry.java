/**
 * 
 */
package blog.threaded.livejournal;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.ArrayList;
import blog.Sentence;
import org.w3c.dom.*;

/**
 * @author sara
 *
 */
public class Entry extends blog.Entry {

	//protected String [] words;
	//protected String [] pos;
	//String entryText;
	//String posText;
	Element entryXML; 
	
	protected ArrayList<Sentence> sentences; 
	
	/**
	 * @param urlIn
	 * @param dateIn
	 * @param entryIn
	 */
	public Entry(String urlIn, Date dateIn, String entryIn) {
		this(urlIn, dateIn, entryIn, null, null);
	}
	
	public Entry(String urlIn, Date dateIn, String entryIn, Element entryXMLIn) {
		this(urlIn, dateIn, entryIn, entryXMLIn, null);
	}
	
	public Entry(String urlIn, Date dateIn, String entryIn, Element entryXMLIn, String titleIn) {
		super(urlIn, dateIn, entryIn, titleIn);
		entryXML = entryXMLIn;
		sentences = new ArrayList<Sentence>();
	}

	/**
	 * Process live journal entry from the xml file - an "item"
	 * @param entryIn
	 * @return
	 */
	public static Entry processEntry(Element entryIn, int numComments, int maxSentenceSize, int numWords) {
		
		try {
			SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
			Date pubDate = entryIn.getElementsByTagName("pubDate").item(0).getFirstChild() == null ? null : format.parse(entryIn.getElementsByTagName("pubDate").item(0).getFirstChild().getNodeValue().trim());        	
	    	String link = entryIn.getElementsByTagName("link").item(0).getFirstChild().getNodeValue();
	    	String title = entryIn.getElementsByTagName("title").item(0) != null ? entryIn.getElementsByTagName("title").item(0).getFirstChild().getNodeValue() : null;
	    	
	    	Entry e = new Entry(link,pubDate,"",entryIn, title);

	    	Element description = ((Element)entryIn.getElementsByTagName("description").item(0));
    		
	    	// no words in entry - this happens when it's adult content. entry is not visible in feed.
	    	if (description == null) return e;
	    	
	    	NodeList sentences = description.getElementsByTagName("sentence");
	    	
	    	if (sentences.getLength() == 0) {
	    		e.addSentence(Sentence.processSentence(description,maxSentenceSize));
	    	}
	    	else {

		    	int count = 0;
		    	
		    	for (int index = 0; index < sentences.getLength(); index++) {
		    		Sentence s = Sentence.processSentence((Element)sentences.item(index),maxSentenceSize);
		    		if (s != null) {
		    			e.addSentence(s);
		    			count += s.getNumWords();
		    		}
		    		if (numWords != -1 && count > numWords) break;
		    	}
	    	}
	    	
    	
	    	NodeList comments = entryIn.getElementsByTagName("comment");
	    	
	    	for (int index = 0; index < comments.getLength() && (numComments == -1 || index < numComments); index++) {
	    		Comment c = Comment.processComment((Element)comments.item(index));	    
	    		// if there is a comment missing an important element the entire thread is unreliable
	    		if (c == null) continue;
	    		e.addComment(c);
	    	}	    	
	    	return e;
	    	
		} catch (Exception e) {
			System.err.println("Error occurred in Entry.processEntry: " + e);
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Element getEntryElement() {
		return entryXML;
	}
	
	public String getEntry(int index) {
		return sentences.get(index).getSentence();
	}
	
	public String getEntry() {
		String sentenceText = "";
		for (int index = 0; index < sentences.size(); index++) {
			sentenceText += this.getEntry(index) + " ";
		}
		return sentenceText;
	}
	
	public String getEntryText(int index) {
		return sentences.get(index).getText();
	}
	
	public String getEntryText() {
		String entryText = "";
		for (int index = 0; index < sentences.size(); index++) {
			entryText += this.getEntryText(index) + " ";
		}
		return entryText;
	}
	
	public String getPOSText(int index) {
		return sentences.get(index).getSentencePOS();
	}
	
	public String getPOSText() {
		String posText = "";
		
		for (int index = 0; index < sentences.size(); index++)
			posText += sentences.get(index).getSentencePOS() + " ";
		return posText;
	}
	
	public String getWord(int sentenceIndex, int wordIndex) {
		if (sentences.get(sentenceIndex) == null) return null;
		return sentences.get(sentenceIndex).getWord(wordIndex);
	}
	
	public String getPOS(int sentenceIndex, int wordIndex) {
		if (sentences.get(sentenceIndex) == null) return null;
		return sentences.get(sentenceIndex).getPOS(wordIndex);
	}
	
	public void addSentence(Sentence s) {
		if (s == null) return;
		entry += s.getText() + "\n";
		sentences.add(s);
	}
	
	public Sentence getSentence(int index) {
		return sentences.get(index);
	}
	
	public ArrayList<Sentence> getSentences() {
		return sentences;
	}
	
	public int getSize() {
		return sentences.size();
	}
	
	public String toXML () {
		String string = "<item>";
		string += "<guid isPermalink=\"true\">" + this.getURL() + "</guid>\n";
		string += "<pubDate>" + new SimpleDateFormat("EEE,  dd MMM yyyy HH:mm:ss").format(this.getDate()) + " GMT</pubDate>\n";
		
		return string;
		
	}
	
	class CommentComparator implements Comparator<Object>{

		public int compare(Object c1In, Object c2In){
			
			Comment c1 = (Comment)c1In;
			Comment c2 = (Comment)c2In;
			
			
			if (c1.getParentURL() != null && c2.getParentURL() != null && c1.getParentURL().equals(c2.getParentURL()))
				return (c1).compareTo(c2);
			else if (c1.getParentId().startsWith("t") && c2.getParentId().startsWith("t")) {
				while (c1 != null && c1.getParentId().startsWith("t") && !c1.getParentURL().equals(c2)) {
					c1 = (Comment)comments.get(c1.getParentURL().toString());
				}
				while (c2 != null && c2.getParentId().startsWith("t") && !c2.getParentURL().equals(c1)) {
					c2 = (Comment)comments.get(c2.getParentURL().toString());
				}
				if (c1 == null) return 1;
				if (c2 == null) return -1;
				
				return c1.compareTo(c2);
			}
			else if (c1.getParentId().startsWith("t")) {
				while (c1 != null && c1.getParentId().startsWith("t") && !c1.getParentURL().equals(c2)) {
					c1 = (Comment)comments.get(c1.getParentURL().toString());
				}
				if (c1 == null) return 1;
				//if (!c1.getParentId().startsWith("t")) 
				return c1.compareTo(c2);
				//return -1;
			}
			else {
				while (c2 != null && c2.getParentId().startsWith("t") && !c2.getParentURL().equals(c1)) {
					c2 = (Comment)comments.get(c2.getParentURL().toString());
				}
				if (c2 == null) return -1;
				return c1.compareTo(c2);
			}
		}
	}

	/*
	 * Get comments sorted by thread/date
	 */
	public Object[] getSortedComments()
	{
		Object [] c = (Object [])comments.values().toArray();
		Arrays.sort(c, new CommentComparator());
		return c;
	}
	
	/*
	 * Get comments sorted by thread/date
	 */
	public String getSortedCommentsToString() {
		Object [] c = (Object [])comments.values().toArray();
		Arrays.sort(c, new CommentComparator());
		
		String string = "";
		
		for (int index = 0; index < c.length; index++) {
			string += c[index] + "\n";
		}
		return string;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
