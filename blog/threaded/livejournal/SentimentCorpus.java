/**
 * 
 */
package blog.threaded.livejournal;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;	
import sentiment.simple.*;

import dom.Writer;

/**
 * @author sara
 *
 */
public class SentimentCorpus {

	/*
	 * compute:
	 *  flat_reply: responses only to blogger
	 *  blogger_discourse: only blogger replies to others
	 *  other_discourse: other's excluding blogger reply to others
	 *  blogger_other_discourse: blogger and others reply to others
	 *  
	 *  longest_chain:
	 */ 
	public class EntryStats {
    	double numSentimentWords = 0;
    	double numWords = 0;
    	double numComments = 0;
    	double longestChain = 0;
    	double conversationSize = 0;
    	String url;
    	String blogger;
    	String year;
    	
    	public EntryStats(String bloggerIn, String urlIn, String yearIn, double wordCount, double sentimentCount, double commentCount, double longestChainIn, int conversationSizeIn) {
    		blogger = bloggerIn;
    		url = urlIn;
    		year = yearIn;
    		numSentimentWords = sentimentCount;
    		numWords = wordCount;
    		numComments = commentCount;
    		longestChain = longestChainIn;
    		conversationSize = conversationSizeIn;
    	}
    	
    	public String getURL() {
    		return url;
    	}
    	
    	public String getBlogger() {
    		return blogger;
    	}
    	
    	public String getYear() {
    		return year;
    	}
    	
    	public double getWordCount() {
    		return numWords;
    	}
    	
    	public double getSentimentCount() {
    		return numSentimentWords;
    	}
    	
    	public double getCommentCount() {
    		return numComments;
    	}
    	
    	public double getConversationSize() {
    		return conversationSize;
    	}
    	
    	public double getLongestChain() {
    		return longestChain;
    	}
    	
    	public String toString() {
    		return url + ", " + blogger + ", " + year + ", " + numWords + ", " +
    			numSentimentWords + ", " + numComments + ", " + conversationSize + ", " + longestChain;
    	}
	}
	
	public class Group {
		String name;
		double entryCount = 0;
    	double sentimentCount = 0;
    	double wordCount = 0;
    	double commentCount = 0;
    	double conversationSizeCount = 0;
    	double longestChainCount = 0;
    	Hashtable<String,SentimentCorpus.EntryStats> entries = new Hashtable<String,SentimentCorpus.EntryStats>();
    	
    	public Group(String nameIn) {
    		name = nameIn;
    	}
    	
    	public void addEntry(SentimentCorpus.EntryStats entry) {
    		entries.put(entry.getURL(), entry);
    		entryCount++;
    		sentimentCount += entry.getSentimentCount();
    		wordCount += entry.getWordCount();
    		commentCount += entry.getCommentCount();
    		conversationSizeCount += entry.getConversationSize();
    		longestChainCount += entry.getLongestChain();
    	}
    	
    	public Iterator<String> getEntries() {
    		return entries.keySet().iterator();
    	}
    	
    	public String entriesToString() {
    		String string = "url, blogger, year, numWords, numSentimentWords, numComments, conversationSize, longestChain\n";
    		Iterator<String> e = this.getEntries();
    		
    		while (e.hasNext()) {
    			SentimentCorpus.EntryStats entry = entries.get(e.next());
    			string += entry.toString() + "\n";
    		}
    		return string;
    	}
    	
    	public String toString() {
    		return name + ", " + entryCount + ", " + wordCount + ", " + sentimentCount + ", "
    			+ commentCount + ", " + conversationSizeCount + ", " + longestChainCount; 
    	}
	}
		
	public void computeGroupStatistics(String directory, String inputFile, String outputDirectory) {
		
		Group flatReply = this. new Group("flat reply");
		Group bloggerDiscourse = this. new Group("blogger discourse");
		Group otherDiscourse = this. new Group("other discourse");
		Group bloggerOtherDiscourse = this. new Group("blogger+other discourse");
		
		try {
			BufferedReader in = new BufferedReader(
				new FileReader(inputFile));
    	
    		String inputLine = "";
			int count = 0;
			
    		while ((inputLine = in.readLine()) != null) {

    			count++;
    			System.err.print("\n" + count + ". " + inputLine);
    			
				Blog b = Blog.processBlog(directory, inputLine);
				
				if (b == null || (b.getCountry() != null && !b.getCountry().matches("US|CA|AU|UK"))) continue;
				
				System.err.print(": Y");
				
				Iterator<?> entries = b.getEntries();
				
				while (entries.hasNext()) {
					
					Entry entry = (Entry)b.getEntry((String)entries.next());
					
					double numComments = entry.getNumComments();
					
					// we are only interested in entries where conversation occurred
					if (numComments == 0) continue;
					
			    	double numWords = entry.getEntryText().split(" ").length;
			    	
					String file = "/proj/nlp/users/sara/corpora/sentiment/DAL/dict_of_affect.txt";
					SimpleSentiment s = SimpleSentiment.readDAL(file,.5,.81);
					double numSentimentWords = s.getNumSentimentWords(entry.getEntryText());
					
					// no sentiment probably means its not english
					if (numSentimentWords == 0) continue;
					
					Iterator<?> ic = entry.getComments();
						
					/*
					 * compute:
					 *  flat_reply: responses only to blogger
					 *  blogger_discourse: only blogger replies to others
					 *  other_discourse: other's excluding blogger reply to others
					 *  blogger_other_discourse: blogger and others reply to others
					 */ 
					double longest_chain = 0;
					HashSet<String> people = new HashSet<String>();
					boolean blogger_discourse = false;
					boolean other_discourse = false;
					
					while (ic.hasNext()) {
						String url = (String)ic.next();
						blog.threaded.Comment comment = (blog.threaded.Comment)entry.getComment(url);
						
						// compute how many people are in the conversation (or lackof)
						people.add(comment.getUsername());
						
						// just a flat reply to original post
						if (comment.getParentURL() == null || comment.getParentURL().equals(entry.getURL())) continue;
						
						// get discourse type for this comment
						if (comment.getUsername().equals(inputLine))
							blogger_discourse = true;
						else 
							other_discourse = true;
						
						// compute whether this is the longest chain
						Comment parent = (Comment)entry.getComment(comment.getParentURL().toString());
						int lc = 1;
						
						while (parent != null && !parent.getParentURL().equals(entry.getURL())) {
							lc++;
							parent = (Comment)entry.getComment(parent.getParentURL().toString());
						}	
						
						if (lc > longest_chain) longest_chain = lc;
					}
					
					SentimentCorpus.EntryStats entryStat = new SentimentCorpus.EntryStats(b.getUsername(), entry.getURL().toString(), new SimpleDateFormat("yyyy").format(b.getDOB()),
							numWords, numSentimentWords, numComments, longest_chain, people.size());
					
					// classify the entry
					if (blogger_discourse && other_discourse) {
						bloggerOtherDiscourse.addEntry(entryStat);
					}
					else if (blogger_discourse) {
						bloggerDiscourse.addEntry(entryStat);
					}
					else if (other_discourse) {
						otherDiscourse.addEntry(entryStat);
					}
					else {
						flatReply.addEntry(entryStat);
					}
				}
    		}
				
			new File(outputDirectory).mkdir();
			
			BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/group_stats.txt"));
			
			out.write("name, entryCount, wordCount, sentimentCount, commentCount, conversationSizeCount, longestChainCount\n");
			out.write(flatReply.toString() + "\n");
			out.write(bloggerDiscourse.toString() + "\n");
			out.write(otherDiscourse.toString() + "\n");
			out.write(bloggerOtherDiscourse.toString() + "\n");
			out.close();
			
			out = new BufferedWriter(new FileWriter(outputDirectory + "/flat_reply.txt"));
			out.write(flatReply.entriesToString());
			out.close();
			
			out = new BufferedWriter(new FileWriter(outputDirectory + "/blogger_discourse.txt"));
			out.write(bloggerDiscourse.entriesToString());
			out.close();
			
			out = new BufferedWriter(new FileWriter(outputDirectory + "/other_discourse.txt"));
			out.write(otherDiscourse.entriesToString());
			out.close();
			
			out = new BufferedWriter(new FileWriter(outputDirectory + "/blogger_other_discourse.txt"));
			out.write(bloggerOtherDiscourse.entriesToString());
			out.close();
		
		} catch (Exception e) {
			System.err.println("Error occurred in Blog.main: " + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * for now: 
	 * blogger discourse and other discourse categories only
	 * comment size <= 25
	 * entry size < 200
	 * comment size/ conversation size >= 2
	 * 
	 * blacklist - files to be ignored
	 */
	public void createTestSet(String inputFile, String directory, String outputDirectory, int testSize, int maxComments, int maxEntrySize, double threshold, HashSet<String> blacklist) {
		
		Hashtable<String, String> qualifiedEntries = new Hashtable<String,String>();
		int numEntries = 0;
		
		try {
			BufferedReader in = new BufferedReader(
				new FileReader(inputFile));
    	
    		String inputLine = "";
			//int count = 0;
			
    		while ((inputLine = in.readLine()) != null) {

    			//count++;
    			//System.err.print("\n" + count + ". " + inputLine);
    			
				Blog b = Blog.processBlog(directory, inputLine);
				
				if (b == null || (b.getCountry() != null && !b.getCountry().matches("US|CA|AU|UK"))) continue;
						
				Iterator<?> entries = b.getEntries();
				
				while (entries.hasNext()) {
					
					Entry entry = (Entry)b.getEntry((String)entries.next());
					
					if (blacklist.contains(entry.getURL().toString())) continue;
					
					double numComments = entry.getNumComments();
					
					// we are only interested in entries where conversation is < maxCommments and > 0
					if (numComments == 0) continue;
					
					numEntries++;
					
					if (numComments > maxComments) continue;
					
			    	double numWords = entry.getEntryText().split(" ").length;
			    	
			    	// only look at entries that are less than maxEntrySize words.
			    	if (numWords == 0 || numWords > maxEntrySize) continue;
					
					Iterator<?> ic = entry.getComments();
						
					/*
					 * include:
					 *  blogger_discourse: only blogger replies to others
					 *  other_discourse: other's excluding blogger reply to others
					 */ 
					HashSet<String> people = new HashSet<String>();
					boolean blogger_discourse = false;
					boolean other_discourse = false;
					
					while (ic.hasNext()) {
						String url = (String)ic.next();
						blog.threaded.Comment comment = (blog.threaded.Comment)entry.getComment(url);
						
						// compute how many people are in the conversation (or lackof)
						people.add(comment.getUsername());
						
						// just a flat reply to original post
						//if (comment.getParentURL() == null || comment.getParentURL().equals(entry.getURL())) continue;
						
						// get discourse type for this comment
						if (comment.getUsername().equals(inputLine))
							blogger_discourse = true;
						else 
							other_discourse = true;
						
						break;
					}
					
					//SentimentCorpus.EntryStats entryStat = new SentimentCorpus.EntryStats(b.getUsername(), entry.getURL().toString(), new SimpleDateFormat("yyyy").format(b.getDOB()),
					//		numWords, numSentimentWords, numComments, longest_chain, people.size());
					
					// classify the entry
					if ((blogger_discourse || other_discourse) && !(blogger_discourse && other_discourse)
							&& numComments/people.size() > threshold) {
						qualifiedEntries.put(entry.getURL().toString(),b.getUsername());
					}
				}
    		}
		} catch (Exception e) {
			System.err.println("[SentimentCorpus.computeTestSet] Error computing test set candidates:" + e);
		}
			
		// now that we have all the qualified entries lets randomly pick our test set.
		List<Object> entries = Arrays.asList(qualifiedEntries.keySet().toArray());
		System.err.println("There are " + entries.size() + "/" + numEntries + " qualified entries");
		
		Collections.shuffle(entries);
		
		new File(outputDirectory).mkdirs();
		
		// output the first "testSize" entries into a folder to be used as a test set.
		for (int index = 0; index < testSize; index++) {
			String url = (String)entries.get(index);
			Blog b = Blog.processBlog(directory, qualifiedEntries.get(url));
			
			Entry e = (Entry)b.getEntry(url);
			
			try {
				// copy profile and remove personal information
				/*FileChannel srcChannel = new FileInputStream(directory + "/profiles/" + b.getUsername() + ".xml").getChannel();
				new File(outputDirectory + "/profiles/").mkdirs();
			    FileChannel dstChannel = new FileOutputStream(outputDirectory + "/profiles/" + b.getUsername() + ".xml").getChannel();
			    dstChannel.transferFrom(srcChannel, 0, srcChannel.size());*/
			    //Blog.removePersonalInformation(inputDirectory, outputDirectory, b.getUsername());
			    
				// copy entry
				new File(outputDirectory + "/entries/" + b.getUsername() + "/").mkdirs();
				String fileName = outputDirectory + "/entries/" + b.getUsername() + "/" + url.substring(url.lastIndexOf("/") + 1,url.length() - 5) + ".xml";
				new File(fileName).createNewFile();
				FileOutputStream xml = new FileOutputStream(fileName);
				Writer writer = new Writer();
				PrintStream p = new PrintStream( xml );
				
                p.println ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
						"<rss version=\"2.0\" xmlns:atom10=\"http://www.w3.org/2005/Atom\" " + 
						"xmlns:lj=\"http://www.livejournal.org/rss/lj/1.0/\" xmlns:media=\"http://search.yahoo.com/mrss/\">\n" + 
						"<channel>\n<title>" + b.getUsername() + "</title>");
				writer.setOutput(xml,"UTF-8");
				writer.write(e.getEntryElement());
				p.println("\n</channel>\n</rss>\n");
			} catch (Exception exc) {
				System.err.println("[SentimentCorpus.createTestSet] error writing to file: " + exc);
				exc.printStackTrace();
			}
		}

	}
	
	public HashSet<String> getUrlFromFiles(String fileName) {
		
		HashSet<String> files = new HashSet<String>();
		ArrayList<File> directories = new ArrayList<File>();
		directories.add(new File(fileName));
		
		while (!directories.isEmpty()) {
			File directory = directories.remove(0);
		
			if (directory.isDirectory()) {
				File [] theseFiles = directory.listFiles();
				
				for (int index = 0; index < theseFiles.length; index++)
					directories.add(theseFiles[index]);
			}
			else if (directory.toString().matches(fileName + ".*/[0-9]*.xml")){
				String d = directory.toString().substring(fileName.length());
				d = d.replace(".xml", "");
				String blogger = d.substring(0,d.indexOf("/"));
				String entry = d.substring(d.indexOf("/")+1);
				files.add("http://" + blogger + ".livejournal.com/" + entry + ".html");
			}
		}
		
		return files;
	}
	
	/**
	 * Add all entries in the array to the training/test corpus
	 * @param entries
	 * @param directory
	 * @param outputDirectory
	 */
	public void addEntries(Hashtable<String,String> entries, String directory, String outputDirectory ) {

		new File(outputDirectory).mkdirs();
		
		Iterator<String> iterator = entries.keySet().iterator();
		
		while (iterator.hasNext()) {
			String url = (String)iterator.next();
			String username = (String)entries.get(url);
			Blog b = Blog.processBlog(directory, username);
			
			Entry e = (Entry)b.getEntry(url);
			
			try {
				
			    
				// copy entry
				new File(outputDirectory + "/entries/" + username + "/").mkdirs();
				String fileName = outputDirectory + "/entries/" + username + "/" + url.substring(url.lastIndexOf("/") + 1,url.length() - 5) + ".xml";
				new File(fileName).createNewFile();
				FileOutputStream xml = new FileOutputStream(fileName);
				Writer writer = new Writer();
				PrintStream p = new PrintStream( xml );
				
                p.println ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
						"<rss version=\"2.0\" xmlns:atom10=\"http://www.w3.org/2005/Atom\" " + 
						"xmlns:lj=\"http://www.livejournal.org/rss/lj/1.0/\" xmlns:media=\"http://search.yahoo.com/mrss/\">\n" + 
						"<channel>\n<title>" + b.getUsername() + "</title>");
				writer.setOutput(xml,"UTF-8");
				writer.write(e.getEntryElement());
				p.println("\n</channel>\n</rss>\n");
			} catch (Exception exc) {
				System.err.println("[SentimentCorpus.createTestSet] error writing to file: " + exc);
				exc.printStackTrace();
			}
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
    	System.err.println("START: " + new Date());
    	
    	String directory = "/proj/nlp/users/sara/corpora/blogs/livejournal/";
    	String profile_list = directory + "original-corpus/profile_list.txt";
    	
    	// test processing of entry for blog (without profile!)
    	//Blog b = Blog.processBlog(directory + "persuasion-training", "ourdancer");
    	//System.exit(0);
    	
    	SentimentCorpus sc = new SentimentCorpus();
    	//sc.computeGroupStatistics(directory + "original-corpus", profile_list, directory + "opinion-output");
    	
    	// create test set for iarpa
    	//sc.createTestSet(profile_list, directory + "original-corpus",directory + "new-test-set", 200, 25, 200, 2, null);
    	
    	// create persuasion training set
    	//HashSet<String> blacklist = sc.getUrlFromFiles(directory + "new-test-set/entries/");
    	//sc.createTestSet(profile_list, directory + "original-corpus",directory + "persuasion-training", 1000, 25, 200, 2, blacklist);

    	// create sentiment training set
    	HashSet<String> blacklist = sc.getUrlFromFiles(directory + "persuasion-testing-evalonly/entries/");
    	blacklist.addAll(sc.getUrlFromFiles(directory + "persuasion-training/entries/"));
    	sc.createTestSet(profile_list, directory + "original-corpus",directory + "sentiment-training", 1000, 25, 200, 1.5, blacklist);
    	
    	// add the blogs we know have persuasion to the training set
    	/*Hashtable<String,String> entries = new Hashtable<String,String>();
    	
    	try {
			BufferedReader in = new BufferedReader(
				new FileReader(directory + "persuasion-training/persuasion-entries.txt"));
    	
    		String inputLine = "";
			
    		while ((inputLine = in.readLine()) != null) {
    			String [] entry = inputLine.split(",");
    			entries.put(entry[0], entry[1].trim());
    		}
    	} catch (Exception e) {
    		System.err.println("[SentimentCorpus.main] error reading persuasion entries from file: " + e);
    		e.printStackTrace();
    	}
    	sc.addEntries(entries, directory + "original-corpus", directory + "persuasion-training");
    	*/
    	System.err.println("END: " + new Date());
	}
}
