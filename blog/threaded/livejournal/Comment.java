/**
 * 
 */
package blog.threaded.livejournal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import org.w3c.dom.*;

import blog.Sentence;

/**
 * @author sara
 *
 */
public class Comment extends blog.threaded.Comment {

	protected String [] words;
	protected String [] pos;
	String commentText;
	String posText;
	
	String userBlogURL;
	String userProfileURL;
	
	protected ArrayList<Sentence> sentences; 
	
	/**
	 * @param commentUrlIn
	 * @param usernameIn
	 * @param dateIn
	 * @param commentIn
	 */
	public Comment(String commentUrlIn, String usernameIn, Date dateIn,
			String commentIn, String parentUrlIn, String parentIdIn, String userBlogURLIn, String userProfileURLIn, String commentID ) {
		super(commentUrlIn, usernameIn, dateIn, commentIn, commentID, parentUrlIn, parentIdIn);

		sentences = new ArrayList<Sentence>();
		userBlogURL = userBlogURLIn;
		userProfileURL = userProfileURLIn;
		
		if (comment == null) return;
		
		String [] data = comment.split(" ");
		commentText = "";
		posText = "";
		words = new String[data.length];
		pos = new String[data.length];
		
		// check if pos is available
		if (comment.split("/").length >= data.length) {
			
			for (int index = 0; index < data.length; index++) {
				if (data[index].equals("")) continue;
				
				int split = data[index].lastIndexOf("/");
				//System.out.println("<" + word[0] + ">");
				
				
				
				if (split == -1 || !data[index].substring(split+1).matches("[A-Z\\p{Punct}]*")) { //data[index].length() - split > 5) {
					words[index] = data[index];
					commentText += data[index];  
				}
				else {
					words[index] = data[index].substring(0,split);
					pos[index] = data[index].substring(split+1);
					
					if (words[index].matches("-RRB-")) 
						words[index] = ")";
					else if (words[index].matches("-LRB-"))
						words[index] = "(";
					
					commentText += words[index] + " ";
					posText += pos[index] + " ";
				}
			}
			commentText.trim();
			//commentText = commentText.replaceAll("-RRB-|-LRB-", "");
			posText.trim();
		}
		else {
			words = data;
			commentText = comment;
		}
		commentText = web.HTML.htmlToText(commentText);
	}
	
	public static Comment processComment(Element comment) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a z");
			Date dateTime = comment.getAttribute("date-time") == null || comment.getAttribute("date-time").isEmpty() ? null : format.parse(comment.getAttribute("date-time"));
			String parentID = comment.getAttribute("parent-id");
			
			String parentURL = !comment.getAttribute("parent-url").equals("") ? 
					comment.getAttribute("parent-url") : null;
							
			//if (dateTime == null) return null;
			// thread structure is missing
			if (parentID == null && parentURL == null) return null;
			
			String url = comment.getAttribute("url");
			Element userNode = (Element)comment.getElementsByTagName("user").item(0);
	    	String user = userNode == null ? "anonymous" : userNode.getFirstChild().getNodeValue();
	    	
	    	String commentID = "-1";
	    	if (url == null || url.equals("")) 
	    		url = dateTime.toString();
	    	else {
	    		try {
		    		if (url.indexOf("=") >= 0)
		    			commentID = url.substring(url.indexOf("=")+1,url.indexOf("#"));
		    		else 
	    				commentID = url;
	    		} catch (NumberFormatException ne) {
	    			System.err.println("[Comment.processComment] error getting comment ID: " + url);
	    		}
	    	}

	    	String blogURL = "";
	    	String profileURL = "";
	    	
	    	if (userNode != null) {
		    	blogURL = userNode.getAttribute("blog_url") == null || userNode.getAttribute("blog_url").equals("") ? null : userNode.getAttribute("blog_url");
				profileURL = userNode.getAttribute("profile_url") == null || userNode.getAttribute("profile_url").equals("") ? null : userNode.getAttribute("profile_url");
	    	}
	    	
	    	//NodeList textTags = comment.getElementsByTagName("string");
	    	//if (textTags.getLength() == 0) 
	    	NodeList textTags = comment.getElementsByTagName("sentence");
	    	
	    	String text = "";
	    	
	    	if (textTags.getLength() > 0) {
	    		for (int index = 0; textTags != null && index < textTags.getLength(); index++) {
		    	
		    		text += textTags.item(index) != null && textTags.item(index).getFirstChild() != null ? 
		    				web.HTML.htmlToText(textTags.item(index).getFirstChild().getTextContent()) : "" + " ";
	    		}
    		}
	    	else {
	    		if (comment.getElementsByTagName("text").item(0).getFirstChild() == null) return null;
	    		text += web.HTML.htmlToText(comment.getElementsByTagName("text").item(0).getFirstChild().getTextContent());
	    	}
	    	
	    	Comment c = new Comment(url,user,dateTime,text,parentURL,parentID,blogURL,profileURL,commentID); 
	    	
	    	if (textTags.getLength() > 0) {
	    		for (int index = 0; textTags != null && index < textTags.getLength(); index++) {
	    			Sentence s = Sentence.processSentence((Element)textTags.item(index));
		    		c.addSentence(s);
	    		}
	    	}
	    	else {
	    		c.addSentence(Sentence.processSentence(text));
	    	}
	    	return c;
	    	
		} catch (Exception e) {
			System.err.println("Error occurred in Entry.processComment: " + e);
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getComment(int index) {
		return sentences.get(index).getSentence();
	}
	
	public String getComment() {
		String sentenceText = "";
		for (int index = 0; index < sentences.size(); index++) {
			sentenceText += this.getComment(index) + " ";
		}
		return sentenceText;
	}
	
	public String getCommentText() {
		if (sentences != null) {
			String sentenceText = "";
			for (int index = 0; index < sentences.size(); index++) {
				sentenceText += sentences.get(index).getText() + " ";
			}
			return sentenceText;
		}
		return commentText;
	}
	
	public String getPOSText() {
		if (sentences != null) {
			String sentenceText = "";
			for (int index = 0; index < sentences.size(); index++) {
				sentenceText += sentences.get(index).getSentencePOS() + " ";
			}
			return sentenceText;
		}
		return posText;
	}
	
	public String getWord(int index) {
		if (words == null) return null;
		return words[index];
	}
	
	public String getPOS(int index) {
		if (pos == null) return null;
		
		return pos[index];
	}
	
	public String getWord(int sentenceIndex, int wordIndex) {
		if (sentences.get(sentenceIndex) == null) return null;
		return sentences.get(sentenceIndex).getWord(wordIndex);
	}
	
	public String getPOS(int sentenceIndex, int wordIndex) {
		if (sentences.get(sentenceIndex) == null) return null;
		return sentences.get(sentenceIndex).getPOS(wordIndex);
	}
	
	public void addSentence(Sentence s) {
		if (s == null) return;
		sentences.add(s);
	}
	
	public Sentence getSentence(int index) {
		return sentences.get(index);
	}
	
	public ArrayList<Sentence> getSentences() {
		return sentences;
	}
	
	public String getUserBlogURL() {
		return userBlogURL;
	}
	
	public String getUserProfileURL() {
		return userProfileURL;
	}
	
	public int compareTo(Comment c) {
		if (c == null) return 0;
		if (Long.valueOf(this.getID()) >= 0)
			return Long.valueOf(this.getID()).compareTo(Long.valueOf(c.getID()));
		else return super.compareTo(c);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
