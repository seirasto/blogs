/**
 * 
 */
package blog.threaded.livejournal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/**
 * @author sara
 *
 */
public class LivejournalScript {

	LexicalizedParser lexicalizedParser;
    MaxentTagger tagger;
	
	LivejournalScript(String parser) {
		lexicalizedParser = LexicalizedParser.loadModel(parser);
	    try {
			tagger = new MaxentTagger("edu/stanford/nlp/models/pos-tagger/english-left3words-distsim.tagger");
	    }
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void CreateSubCorpus(int size, String age, String directory) {

		System.err.println("[Blog.main] processing " + size + " bloggers between " + age);
		
		// code to add dependencies & POS from downloaded blogs
    	String inputDirectory = directory + "original-corpus/";
    	String outputDirectory = directory + "subcorpus/" +age + "/";
   	
    	ArrayList<String> bloggers = new ArrayList<String>();
    	
    	try {
			BufferedReader in = new BufferedReader(
					new FileReader(directory + "age-lists/" + age + ".txt"));
	
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				bloggers.add(inputLine);
			}
			in.close();
			
			Collections.shuffle(bloggers);
			new File(outputDirectory).mkdirs();
			BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "profile_list.txt"));
			
			int i = 0; // keep track of number of files processed successfully
			
			for (int index = 0; index < bloggers.size() && index < size; index++) {
	        				        	
				System.err.println("[Blog.main] " + i + ". adding Dependencies & POS from " + bloggers.get(index));

				try {
					Blog.addSyntaxDependencies(inputDirectory, outputDirectory, bloggers.get(index), tagger, lexicalizedParser, true, new processing.SwapEmoticons());
    				// add to profile_list.txt
    				out.write(bloggers.get(index) + "\n");
				} catch (Exception e) {
					System.err.println("Error occurred in Blog.main adding syntax dependencies: " + e);
					e.printStackTrace();
					continue;
				}
				i++;
			}
			out.close();
			System.err.println("[Blog.main] completed adding syntax dependencies");
    	} catch (Exception e) {
			System.err.println("Error occurred in Blog.main: " + e);
			e.printStackTrace();
		}
	}
	
	/*public void runWeka(String [] ages, String directory, int numWords) {
		Hashtable<String,Integer> attributes = new Hashtable<String,Integer>();
		//attributes.put("interests",200);
		//attributes.put("friends",0);
		//attributes.put("posts",0);
		//attributes.put("grams",200);
		attributes.put("ngrams",200);
		attributes.put("bigrams",200);
		attributes.put("syntaxBigrams",200);
		
		Weka weka = null; //new Weka(directory + "output", directory + "original-corpus", directory + "subcorpus/", ages, attributes,1000,1000);
		
		for (int index = 0; index < ages.length; index++) {
			weka.addAgeRange(ages[index], directory + "output/" + ages[index], ages.length, "tf");
			System.err.println("[Weka.main] added age range " + ages[index]);
		}

		// 4. get unique words by merging ranges
		weka.merge(true);
		
		// 5. print out detailed report
		weka.printReport(null);
		
		try {
			new File(directory + "output/results/").mkdirs();
			BufferedWriter out = new BufferedWriter(new FileWriter(directory + "output/results/data-bs.arff"));
			out.write(weka.toString());
			out.close();
			//System.out.println(weka.toString());
		} catch (Exception e) {
			System.err.println("[Weka.main] Error occurred in writing to file: " + e);
			e.printStackTrace();
		}
	}*/
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String directory = "/proj/nlp/users/sara/corpora/blogs/livejournal/";
    	String parser = "/proj/nlp/users/sara/java/input/englishPCFG.ser.gz";
    	int size = 750;
    	
    	String [] ages = {"1940-1968","1969-1978","1979-1988","1989-2009"};
    	
    	LivejournalScript script = new LivejournalScript(parser);

		// step 1: Create a subcorpus of size n with POS and Dependencies
    	for (int index = 0; index < ages.length; index++) {
    		script.CreateSubCorpus(size, ages[index], directory);
    	}

    	// step 2: run weka (includes Xtract)
    	// int numWords = 1333;
    	// script.runWeka(ages, directory, numWords);
	}

}
