/**
 * 
 */
package blog.threaded.create_debate;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.net.URL;

import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import dom.Writer;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.*;

import processing.*;

/**
 * @author sara
 *
 */
public class Blog extends blog.Blog {

		private String fullName;
		private int numPosts;
		private String bio;
		private Hashtable<String,String> interests;
		private String outputDirectory;
		private String title;
		private String description;
		private String type;
		private String image;
		private String id;
		private Date lastBuildDate;
		
        /**
         * @param usernameIn
         * @param languageInisr.close
         * @param dobIn
         * @param countryIn
         * @param cityIn
         * @param homepageIn
         */
        public Blog(String outputDirectoryIn, String usernameIn, String languageIn, Date dobIn,
                        String countryIn, String cityIn, URL homepageIn, String fullNameIn, int numPostsIn, String bioIn) {
                super(usernameIn, languageIn, dobIn, countryIn, cityIn, homepageIn, null, null, null);
                fullName = fullNameIn;
                numPosts = numPostsIn;
                bio = bioIn;
                interests = new Hashtable<String,String>();
                outputDirectory = outputDirectoryIn;
        }
        
        public static Blog processBlog(String outputDirectory, String username) {
        	File directory = new File(outputDirectory + "/entries/" + username + "/");
        	return Blog.processBlog(outputDirectory, outputDirectory + "/profiles/" + username +".xml", directory.listFiles());
        }
        
        public static Blog processBlog(String profilesDirectory, String entriesDirectory, String username) {
        	File directory = entriesDirectory != null ? new File(entriesDirectory + "/entries/" + username + "/") : null;
        	return Blog.processBlog(profilesDirectory, profilesDirectory + "/profiles/" + username +".xml", entriesDirectory == null ? null : directory.listFiles());
        }

        public static Blog processBlog(String entry) {
        	File [] directory = {new File(entry)};
        	return Blog.processBlog(null, null, directory);
        }
        
        public static void addPOS(String outputDirectory, String username, String taggerIn) {
        	Blog b = Blog.processBlog(outputDirectory, username);

        	if (b.hasPOS()) {
        		System.err.println("[Blog.addPOS] blog entries for " + username + " already contains POS.");
        		return;
        	}
        	
        	// only add pos if its english
        	if (b.getCountry() == null || (!b.getCountry().equals("US") && !b.getCountry().equals("CA") && !b.getCountry().equals("UK") && !b.getCountry().equals("AU"))) {
        		System.err.println("[Blog.addPOS] invalid country " + b.getCountry() + " for " + username);
        		return;
        	}
        	
        	File [] entriesFilesIn = new File(outputDirectory + "/entries/" + username + "/").listFiles();
        	
        	DOMParser parser = new DOMParser();
        	MaxentTagger tagger = null;
        	
    		try {
    			tagger = new MaxentTagger(taggerIn);
    			parser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return;
    		}
    		
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	try {
            		InputStreamReader isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		parser.parse(in);
            		
            		Document blog = parser.getDocument();
            		
            		// Each entry            		
    	        	NodeList entries = blog.getElementsByTagName("item");
    	        	
    	        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
    	        		NodeList des = ((Element)entries.item(entriesIndex)).getElementsByTagName("description");
    	        		
    	        		if (des == null) continue;
    	        		Element descriptionTag = (Element)des.item(0);
    	    	    	
    	        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null ? 
    	        				descriptionTag.getFirstChild().getNodeValue() : "";
    	        		
    	        		String [] paragraphs = description.split("&lt;br /&gt;");
    	        		
    	        		String taggedDescription = "";
    	        		
    	        		for (int i = 0; i < paragraphs.length; i++) {
    	        			if (paragraphs[i].length() > 20000) {
    	        				
    	        				String [] sentences = paragraphs[i].split("\\. ");
    	        				
    	        				for (int j = 0; j < sentences.length; j++) {
    	        					taggedDescription += tagger.tagString(web.HTML.stripTags(sentences[j]) + ".");
    	        				}
    	        				
    	        				taggedDescription += "\n";
    	        			}
    	        			else {
    	        				taggedDescription += tagger.tagString(web.HTML.stripTags(paragraphs[i]) + "\n");
    	        			}
    	        		}
    	        		if (taggedDescription.length() > 0) 
    	        			descriptionTag.getFirstChild().setNodeValue(taggedDescription);

    	        		// Each comment
    	        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
    	        		
    	        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
    	        			Element commentTag = (Element)comments.item(commentsIndex);
    	        			
    	        			String comment = commentTag != null && commentTag.getFirstChild() != null ?
    	        					tagger.tagString(web.HTML.stripTags(commentTag.getFirstChild().getNodeValue())) : "";
    	        					
    	        			if (comment.length() > 0)		
    	        				commentTag.getFirstChild().setNodeValue(comment);		
    	        		}
    	        	}
    	        	
    	        	isr.close();
    	        	// overwrite to store pos version of file since it includes text
    	        	System.err.println("[Blog.addPOS] writing to " + entriesFilesIn[index]);
    	        	FileOutputStream xml = new FileOutputStream(entriesFilesIn[index]);
    				Writer writer = new Writer();
    				writer.setOutput(xml,"UTF-8");
    				writer.write(blog);
    	        	
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.processBlog for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	}
        	}
        }
        
        public static void addPOSFlatEntry(String inputFile, String taggerIn) {

        	new File(new File(inputFile).getParent() + "-pos/").mkdir();
        	File outputFile = new File(new File(inputFile).getParent() + "-pos/" + new File(inputFile).getName());
        	
        	if (outputFile.exists()) {
        		System.err.println("[Blog.addPOSFlatEntry] " + inputFile + " already contains POS.");
        		return;
        	}
        
        	DOMParser parser = new DOMParser();
        	MaxentTagger tagger = null;
        	
    		try {
    			tagger = new MaxentTagger(taggerIn);
    			parser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return;
    		}
    		
        	try {
        		InputStreamReader isr = new InputStreamReader(new FileInputStream(inputFile),"UTF-8");
        		InputSource in = new InputSource(isr);
        		parser.parse(in);
        		
        		Document blog = parser.getDocument();
        		
        		// Each entry            		
	        	NodeList entries = blog.getElementsByTagName("item");
	        	
	        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
	        		NodeList des = ((Element)entries.item(entriesIndex)).getElementsByTagName("description");
	        		
	        		if (des == null) continue;
	        		Element descriptionTag = (Element)des.item(0);
	    	    	
	        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null ? 
	        				descriptionTag.getFirstChild().getNodeValue() : "";
	        		
	        		String [] paragraphs = description.split("&lt;br /&gt;");
	        		
	        		String taggedDescription = "";
	        		
	        		for (int i = 0; i < paragraphs.length; i++) {
	        			if (paragraphs[i].length() > 20000) {
	        				
	        				String [] sentences = paragraphs[i].split("\\. ");
	        				
	        				for (int j = 0; j < sentences.length; j++) {
	        					taggedDescription += tagger.tagString(web.HTML.stripTags(sentences[j]) + ".");
	        				}
	        				
	        				taggedDescription += "\n";
	        			}
	        			else {
	        				taggedDescription += tagger.tagString(web.HTML.stripTags(paragraphs[i]) + "\n");
	        			}
	        		}
	        		if (taggedDescription.length() > 0) 
	        			descriptionTag.getFirstChild().setNodeValue(taggedDescription);

	        		// Each comment
	        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
	        		
	        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
	        			Element commentTag = (Element)comments.item(commentsIndex);
	        			
	        			String comment = commentTag != null && commentTag.getFirstChild() != null ?
	        					tagger.tagString(web.HTML.stripTags(commentTag.getFirstChild().getNodeValue())) : "";
	        					
	        			if (comment.length() > 0)		
	        				commentTag.getFirstChild().setNodeValue(comment);		
	        		}
	        	}
	        	
	        	isr.close();
	        	// overwrite to store pos version of file since it includes text
	        	System.err.println("[Blog.addPOS] writing to " + outputFile);
	        	FileOutputStream xml = new FileOutputStream(outputFile);
				Writer writer = new Writer();
				writer.setOutput(xml,"UTF-8");
				writer.write(blog);
	        	
        	} catch (Exception e) {
        		System.err.println("Error occurred in Blog.processBlog for blog entries file " + outputFile + ": " + e);
        		e.printStackTrace();
        	}
        }
        
        public static void removePOS(String inputDirectory, String outputDirectory, String username) {
        	Blog b = Blog.processBlog(inputDirectory, username);
        	
       		File [] entriesFilesIn = new File(inputDirectory + "/entries/" + username + "/").listFiles();
    		
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

            	// only add pos if its english
            	if (b.getCountry() == null || (!b.getCountry().equals("US") && !b.getCountry().equals("CA") && !b.getCountry().equals("UK") && !b.getCountry().equals("AU"))) {
            		System.err.println("[Blog.removePOS] invalid country " + b.getCountry() + " for " + username  + " just moving file over.");
        			File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
        			outputFile.getParentFile().mkdirs();
        			entriesFilesIn[index].renameTo(outputFile);
        			entriesFilesIn[index].getParentFile().delete();
        			continue;
        		}
        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		if (entriesFilesIn[index].toString().endsWith("~")) {
        			entriesFilesIn[index].delete();
        			entriesFilesIn[index] = null;
        			System.err.println("[Blog.removePOS] deleting file " + entriesFilesIn[index]);
        			continue;
        		}
        		
            	DOMParser parser = new DOMParser();
            	
        		try {
        			parser.setFeature("http://xml.org/sax/features/namespaces", false);
        		} catch (Exception e) {
        			System.err.println("Error occurred in Blog.processBlog " + e);
        			return;
        		}
        		
            	try {
            		InputStreamReader  isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		parser.parse(in);
            		
            		Document blog = parser.getDocument();
            		
            		// Each entry            		
    	        	NodeList entries = blog.getElementsByTagName("item");
    	        	
    	        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
    	        		Element descriptionTag = (Element)((Element)entries.item(entriesIndex)).getElementsByTagName("description").item(0);
    	    	    	
    	        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null ? 
    	        				descriptionTag.getFirstChild().getNodeValue() : "";
    	        	
    	        		String [] paragraphs = description.split("&lt;br /&gt;");
    	        		
    	        		String taggedDescription = "";
    	        		
    	        		for (int i = 0; i < paragraphs.length; i++) {

   	        				String [] sentences = paragraphs[i].split("\\. ");
    	        				
	        				for (int j = 0; j < sentences.length; j++) {
	        					taggedDescription += sentences[j].replaceAll("/\\p{Graph}* "," ");
	        				}
	        				
	        				taggedDescription += "\n";
    	        		}

    	        		if (taggedDescription.length() > 0) 
    	        			descriptionTag.getFirstChild().setNodeValue(taggedDescription);

    	        		// Each comment
    	        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
    	        		
    	        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
    	        			Element commentTag = (Element)comments.item(commentsIndex);
    	        			
    	        			String comment = commentTag != null && commentTag.getFirstChild() != null ?
    	        					web.HTML.stripTags(commentTag.getFirstChild().getNodeValue()).replaceAll("/\\p{Graph}* "," ") : "";
    	        					
    	        			if (comment.length() > 0)		
    	        				commentTag.getFirstChild().setNodeValue(comment);		
    	        		}
    	        	}
    	        	
    	        	isr.close();
    	        	// overwrite to store pos version of file since it includes text
    	        	File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
    	        	outputFile.getParentFile().mkdirs();
    	        	System.err.println("[Blog.removePOS] writing to " + outputFile);
    	        	FileOutputStream xml = new FileOutputStream(outputFile);
    				Writer writer = new Writer();
    				writer.setOutput(xml,"UTF-8");
    				writer.write(blog);
    	        	
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.processBlog for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	}
        	}
        }
        
        private static void removeTag(Document d, String t) {
        	NodeList tags = d.getElementsByTagName(t);
        	
        	for (int i = 0; i < tags.getLength(); i++) {
	    		Element tag = (Element)d.getElementsByTagName(t).item(i);
	    		
	        	if (tag != null) {
	        		tag.getParentNode().removeChild(tag);
	        	}
        	}
        }
        
        /**
         * Remove personal information from the profiles and blogs
         * @param inputDirectory
         * @param outputDirectory
         * @param username
         */
        public static void removePersonalInformation(String inputDirectory, String outputDirectory, String username) {
        	
        	// remove personal info and save file
    		
        	DOMParser xmlParser = new DOMParser();
        	
    		try {
    			xmlParser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return;
    		}
    		
    		String profileFile = inputDirectory + "/profiles/" + username + ".xml";
    		
    		try {
    			InputStreamReader isr = new InputStreamReader(new FileInputStream(profileFile),"UTF-8");
	    		InputSource in = new InputSource(isr);
	    		xmlParser.parse(in);
	    		
	    		Document profile = xmlParser.getDocument();
	    		
	    		Blog.removeTag(profile,"foaf:name");
	    		Blog.removeTag(profile,"foaf:icqChatID");
	    		Blog.removeTag(profile,"foaf:aimChatID");
	    		Blog.removeTag(profile,"foaf:jabberID");
	    		Blog.removeTag(profile,"foaf:msnChatID");
	    		Blog.removeTag(profile,"foaf:yahooChatID");
	    		Blog.removeTag(profile,"foaf:member_name");
	    		isr.close();
	        	//profile.removeChild(profile.getElementById("foaf:icqChatID"));
        	
	        	// store syntax dependencies version of file
				File outputFile = new File(profileFile.replaceFirst(inputDirectory,outputDirectory));
				outputFile.getParentFile().mkdirs();
	        	System.err.println("[Blog.removePersonalInformation] writing to " + outputFile);
	        	FileOutputStream xml = new FileOutputStream(outputFile);
				Writer writer = new Writer();
				writer.setOutput(xml,"UTF-8");
				writer.write(profile);
        	} catch (Exception e) {
        		System.err.println("Error occurred in Blog.removePersonalInformation for blog " + profileFile + ": " + e);
        		e.printStackTrace();
        	} 
        	
        	// copy entries
        	File [] entriesFilesIn = new File(inputDirectory + "/entries/" + username + "/").listFiles();
        	
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	try {
            		
            		InputStream ins = new FileInputStream(entriesFilesIn[index]);
            		File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
            		outputFile.getParentFile().mkdirs();
                    OutputStream out = new FileOutputStream(outputFile);
                
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = ins.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    ins.close();
                    out.close();
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.removePersonalInformation for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	} 
        	}
        }

        /**
         * Add syntax dependencies and POS
         * @param inputDirectory
         * @param outputDirectory
         * @param username
         * @param lexicalizedParser
         */
        public static void addSyntaxDependencies(String inputDirectory, String outputDirectory, String username, LexicalizedParser lexicalizedParser, boolean updateComments, SwapEmoticons swapper) {
        	
        	File [] entriesFilesIn = new File(inputDirectory + "/entries/" + username + "/").listFiles();
        	
    		for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	DOMParser xmlParser = new DOMParser();
            	
        		try {
        			xmlParser.setFeature("http://xml.org/sax/features/namespaces", false);
        		} catch (Exception e) {
        			System.err.println("Error occurred in Blog.processBlog " + e);
        			return;
        		}
        		
            	try {
            		InputStreamReader isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		xmlParser.parse(in);
            		
            		// update entries with dependencies and POS
            		Document blog = Blog.updateEntries(xmlParser.getDocument(), xmlParser, lexicalizedParser, updateComments, swapper);
            		isr.close();
    	        	// store syntax dependencies version of file
    				File outputFile = new File(entriesFilesIn[index].toString().replaceFirst(inputDirectory,outputDirectory));
    	        	outputFile.getParentFile().mkdirs();
    	        	System.err.println("[Blog.addSyntaxDependencies] writing to " + outputFile);
    	        	FileOutputStream xml = new FileOutputStream(outputFile);
    				Writer writer = new Writer();
    				writer.setOutput(xml,"UTF-8");
    				writer.write(blog);
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.addSyntaxDependencies for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            	}
        	}
        }
        

        private static Document updateEntries(Document blog, DOMParser xmlParser, LexicalizedParser lexicalizedParser, boolean updateComments, SwapEmoticons swapper) throws Exception {

        	// to be used later
		    TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
      	
    		// Each entry            		
        	NodeList entries = blog.getElementsByTagName("item");
        	
        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
        		NodeList des = ((Element)entries.item(entriesIndex)).getElementsByTagName("description");
        		
        		if (des == null || des.getLength() == 0) continue;
        		Element descriptionTag = (Element)des.item(0);
    	    	
        		String description = descriptionTag != null && descriptionTag.getFirstChild() != null ? 
        				descriptionTag.getFirstChild().getNodeValue() : "";
        		if(descriptionTag != null && descriptionTag.getFirstChild() != null)
        			descriptionTag.removeChild(descriptionTag.getFirstChild());
        		
        		description = description.replaceAll("&nbsp;|&amp;nbsp;"," ");
        		description = description.replaceAll("&lt;br /&gt;", "<br />");
        		String [] paragraphs = description.split("(<br />)+");
        		
        		int id = 1;
        		
        		for (int i = 0; i < paragraphs.length; i++) {
        			
        			paragraphs[i] = web.HTML.htmlToText(paragraphs[i]); 
       				
        			//List<String> sentences = processing.NlpUtils.splitSentences(paragraphs[i]);
        			List<String> sentences = processing.StringProcessing.SentenceSplitter(paragraphs[i]);
        				
    				for (int j = 0; j < sentences.size(); j++) {
    					
    					String s = sentences.get(j);
    					
    					// remove all link's and image's for parsing
    					s = s.replaceAll("LLLINKKK|IIIMAGEEE", "");
    					
    					if (s.length() == 0 || s.trim().length() == 0) continue;
    					
    					if (s.length() > 100) { 
    						
							int space = s.indexOf(" ",90);
							if (space >= 0 && space < 100)
								s = s.substring(0, s.indexOf(" ", 90));
							else if (s.length() > 100) {
								s = s.substring(0, 100);
								if (s.lastIndexOf(" ") > 0)
									s = s.substring(0,s.lastIndexOf(" "));
								s = "";
							}
						}	
    					

    					
    					// convert all emoticons so that we don't lose them
    					s = swapper.swapEmoticons(s, true);
    					
    					if (s.length() == 0 || s.trim().length() == 0) continue;
						
    					Tree parse = null;
    					
    					try {
    						parse = (Tree) lexicalizedParser.parse(s);
    					} catch (Exception e) {
    						System.err.println("[Blog.updateEntries] error parsing tree");
    						continue;
    					}
        					
    					GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
    				    
    				    String dependencies = gs.typedDependencies().toString();
    				    String taggedSentence = parse.toString();
    				    
    				    // put the emoticons back in - we don't need to do this. we can always refer to the codes later!
    				    //taggedSentence = swapper.swapEmoticons(taggedSentence, false);
    				    
    				    Element sentence = blog.createElement("sentence");
    				    sentence.setAttribute("id", new Integer(id).toString());
    				    
    				    Element text = blog.createElement("string");
    				    text.setTextContent(sentences.get(j));
			    
    				    Element pos = blog.createElement("pos");
    				    pos.setTextContent(taggedSentence);
    				    
    				    Element deps = blog.createElement("dependencies");
    				    deps.setTextContent(dependencies.substring(1, dependencies.length()-1));
    				    
    				    sentence.appendChild(text);
    				    sentence.appendChild(pos);
    				    sentence.appendChild(deps);
    				    descriptionTag.appendChild(sentence);
    				    id++;
    				}
    			}
        		
        		if (!updateComments) continue;
        		
        		// Each comment
        		NodeList comments = ((Element)entries.item(entriesIndex)).getElementsByTagName("text");
        		
        		for (int commentsIndex = 0; commentsIndex < comments.getLength(); commentsIndex++) {
        			Element commentTag = (Element)comments.item(commentsIndex);
        			
        			String comment = commentTag != null && commentTag.getFirstChild() != null ?
        					commentTag.getFirstChild().getNodeValue() : "";
        					
        			comment = web.HTML.htmlToText(comment);		
        			
        			// clear comment - different setup being created here
   					commentTag.getFirstChild().setNodeValue("");
       					
					ArrayList<String> sentences = processing.StringProcessing.SentenceSplitter(comment);
					//List<String> sentences = processing.NlpUtils.splitSentences(comment);
    				
    				for (int j = 0; j < sentences.size(); j++) {
    					//String s = sentences[j].replaceAll("/\\p{Graph}* "," ").trim();
    					if (sentences.get(j).length() == 0) continue;
    					
    					sentences.set(j, sentences.get(j)); 
    					
						// truncate if too long
    					String s = sentences.get(j);
    					
    					if (s.length() > 100) { 
    						
							int space = s.indexOf(" ",90);
							if (space >= 0 && space < 100)
								s = s.substring(0, s.indexOf(" ", 90));
							else if (s.length() > 100) {
								s = s.substring(0, 100);
								s = s.substring(0,s.lastIndexOf(" "));
							}
						}	
						
    					Tree parse = null;
    					
    					try {
    						parse = (Tree) lexicalizedParser.parse(sentences.get(j));
    					} catch (Exception e) {
    						System.err.println("[Blog.updateEntries] error parsing tree");
    						continue;
    					}
    					
    				    GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
    				    String dependencies = gs.typedDependenciesCollapsed().toString();

    				    String taggedSentence = parse.taggedYield().toString();
    				    
    				    Element sentence = blog.createElement("sentence");
    				    sentence.setAttribute("id", new Integer(j+1).toString());
    				    
    				    Element text = blog.createElement("string");
    				    text.setTextContent(sentences.get(j));
    				    
    				    Element pos = blog.createElement("pos");
    				    pos.setTextContent(taggedSentence);
    				    
    				    Element deps = blog.createElement("dependencies");
    				    deps.setTextContent(dependencies.substring(1, dependencies.length()-1));
    				    
    				    sentence.appendChild(text);
    				    sentence.appendChild(pos);
    				    sentence.appendChild(deps);
    				    commentTag.appendChild(sentence);
    				}		
        		}
        	}

        	return blog;
        }
        
        private static Blog processBlog(String outputDirectory, String profileFileIn,
                       File [] entriesFilesIn) {
        	
        	DOMParser parser = new DOMParser();
    		
    		try {
    			parser.setFeature("http://xml.org/sax/features/namespaces", false);
    		} catch (Exception e) {
    			System.err.println("Error occurred in Blog.processBlog " + e);
    			return null;
    		}
    		
    		Blog blog = null;

    		if (profileFileIn != null && new File(profileFileIn).exists()) {
    		
		    	try {
		    		InputStreamReader isr = new InputStreamReader(new FileInputStream(profileFileIn),"UTF-8");
		    		InputSource in = new InputSource(isr);
		    		parser.parse(in);
		
		    		// add profile info
		        	Document profile = parser.getDocument();
		        	Element person = (Element)profile.getElementsByTagName("foaf:Person").item(0);
		        	String username = person.getElementsByTagName("foaf:nick").item(0).getFirstChild().getNodeValue();
		        	NodeList nameTag = person.getElementsByTagName("foaf:name");
		        	String fullname = nameTag != null && nameTag.getLength() > 0 && nameTag.item(0).getFirstChild() != null ? nameTag.item(0).getFirstChild().getNodeValue() : null;
		        	NodeList postedTag = person.getElementsByTagName("ya:posted");
		        	int postCount = postedTag != null && postedTag.getLength() > 0 && postedTag.item(0).getFirstChild() != null? Integer.parseInt(postedTag.item(0).getFirstChild().getNodeValue()) : -1;
		        	
		        	NodeList dobTag = person.getElementsByTagName("foaf:dateOfBirth");
		        	
		        	// somehow blog without date got in... delete it
		        	if (dobTag == null || dobTag.getLength() == 0) {
						System.err.println("[Blog.main] invalid id, " + username + ", missing dob. ");
		        		return null;
		        	}
		        	
		        	String date = dobTag.item(0).getFirstChild().getNodeValue();
		        	SimpleDateFormat format = date.length() > 5 ? new SimpleDateFormat("yyyy-MM-dd") : new SimpleDateFormat("yyyy");
		        	Date dob = dobTag != null && dobTag.getLength() > 0 ? format.parse(date) : null;
		        	NodeList countryTag = person.getElementsByTagName("ya:country");
		        	String country = countryTag != null && countryTag.getLength() > 0 ? countryTag.item(0).getAttributes().getNamedItem("dc:title").getNodeValue() : null;
		        	NodeList cityTag = person.getElementsByTagName("ya:city");
		        	String city = cityTag != null && cityTag.getLength() > 0 ? cityTag.item(0).getAttributes().getNamedItem("dc:title").getNodeValue() : null;
		        	URL homepage = new URL(person.getElementsByTagName("foaf:openid").item(0).getAttributes().getNamedItem("rdf:resource").getNodeValue());
		        	NodeList bioTag = person.getElementsByTagName("ya:bio");
		        	String biography = bioTag != null && bioTag.getLength() > 0 ? 
		        			 bioTag.item(0).getFirstChild() != null ? web.HTML.stripTags(bioTag.item(0).getFirstChild().getNodeValue()) : ""
		        					 : null;
		            blog = new Blog(outputDirectory, username, null, dob, country, city, homepage, fullname, postCount, biography);
		            
		            // add interests
		            NodeList interests = person.getElementsByTagName("foaf:interest");
		            
		            for (int index = 0; index < interests.getLength(); index++) {
		            	String id = interests.item(index).getAttributes().getNamedItem("dc:interestID") != null ? interests.item(index).getAttributes().getNamedItem("dc:interestID").getNodeValue() : "";
		            	String title = interests.item(index).getAttributes().getNamedItem("dc:title") != null ? interests.item(index).getAttributes().getNamedItem("dc:title").getNodeValue() : "";
		    			String count = interests.item(index).getAttributes().getNamedItem("dc:interestWideCount") != null ? interests.item(index).getAttributes().getNamedItem("dc:interestWideCount").getNodeValue() : "";
		            	blog.addInterest(id,title,count);
		            }
		            
		            // add friends
		            NodeList friends = person.getElementsByTagName("foaf:knows");
		            
		            for (int index = 0; index < friends.getLength(); index++) {
		            	
		            	// make sure there is a friend type. If there is none it is probably a deleted account, so ignore it.
		            	if (((Element)friends.item(index)).getAttributeNode("dc:type") == null) continue;
		            	
		            	String friendName = ((Element)friends.item(index)).getElementsByTagName("foaf:nick").item(0).getFirstChild().getNodeValue();
		            	String friendType = ((Element)friends.item(index)).getAttributeNode("dc:type").getNodeValue();
		            	blog.addFriend(friendName,friendType);
		            }
		            isr.close();
		    	} catch (Exception e) {
		    		System.err.println("Error occurred in Blog.processBlog for profile file " + profileFileIn + ": " + e);
		    		e.printStackTrace();
		    		return null;
		    	}
    		}
    		else {
    			//System.err.println("[Blog.processBlog] no profile available");
    			blog = new Blog(outputDirectory,null, null, null, null, null, null, null, -1, null);
		           
    		}
    		
        	// add blog entries
        	for (int index = 0; entriesFilesIn != null && index < entriesFilesIn.length; index++) {

        		if (entriesFilesIn[index].isDirectory()) continue;
        		if (entriesFilesIn[index].length() == 0) continue;
        		
            	try {
            		InputStreamReader isr = new InputStreamReader(new FileInputStream(entriesFilesIn[index]),"UTF-8");
            		InputSource in = new InputSource(isr);
            		parser.parse(in);
            		
            		Document profile = parser.getDocument();
            		
            		// General entry information
		        	String author = profile.getElementsByTagName("author").item(0) == null ? null : profile.getElementsByTagName("author").item(0).getFirstChild().getNodeValue();
            		blog.setTitle(profile.getElementsByTagName("title").item(0) == null ? "" : profile.getElementsByTagName("title").item(0).getFirstChild().getNodeValue());
            		if (blog.getUsername() == null) {
            			if (author != null) blog.setUsername(author);
            			else blog.setUsername(blog.getTitle());
            		}
            		
            		blog.setDescription(profile.getElementsByTagName("description") == null || profile.getElementsByTagName("description").item(0) == null || profile.getElementsByTagName("description").item(0).getFirstChild() == null ? "" : profile.getElementsByTagName("description").item(0).getFirstChild().getNodeValue());
            		if (profile.getElementsByTagName("lj:journaltype").getLength() != 0) blog.setType(profile.getElementsByTagName("lj:journaltype").item(0).getFirstChild().getNodeValue());
            		if (profile.getElementsByTagName("lastBuildDate").item(0) != null) blog.setLastBuildDate(profile.getElementsByTagName("lastBuildDate").item(0).getFirstChild().getNodeValue());
            		//blog.setID(profile.getElementsByTagName("lj:journalid").item(0).getFirstChild().getNodeValue());
            		//blog.setImage(profile.getElementsByTagName("url").item(0).getFirstChild().getNodeValue());
            		
            		// Each entry            		
    	        	NodeList entries = profile.getElementsByTagName("item");
    	        	
    	        	for (int entriesIndex = 0; entriesIndex < entries.getLength(); entriesIndex++) {
    	        		try {
    	        			blog.addEntry(Entry.processEntry((Element)entries.item(entriesIndex)));
    	        		} catch (Exception e) {
    	        			System.err.println("Error occured in Blog.processBlog processing entry " + entriesIndex + " for " + entriesFilesIn[index] + ": " + e);
    	        			e.printStackTrace();
    	        		}
    	        	}
    	        	isr.close();
    	        	
            	} catch (Exception e) {
            		System.err.println("Error occurred in Blog.processBlog for blog entries file " + entriesFilesIn[index] + ": " + e);
            		e.printStackTrace();
            		return null;
            	}
        	}
        	return blog;
        }
        
        public boolean hasPOS() {
        	Entry first = (blog.threaded.create_debate.Entry)entries.get(this.getEntries().next());
        	if (first.getPOSText().isEmpty() || first.getPOSText().trim().equals("")) return false;
        	return true;
        	
        }
        
        public void setTitle(String titleIn) {
        	title = titleIn;
        }
        
        public void setDescription(String descriptionIn) {
        	description = descriptionIn;
        }
        
        public void setType(String typeIn) {
        	type = typeIn;
        }
        
        public void setLastBuildDate(String lbd) {
        	try {
	        	SimpleDateFormat lastBuildDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
	        	lastBuildDate = (Date)lastBuildDateFormat.parse(lbd);
        	} catch (Exception e) {
        		System.err.println("[Blog.setLastBuildDate] " + e);
        		e.printStackTrace();
        	}
        }
        
        public Date getLastBuildDate() {
        	return lastBuildDate;
        }
        
        public void setImage(String imageURL) {
        	image = imageURL;
        }
        
        public void setID(String idIn) {
        	id = idIn;
        }
        
        public String getTitle() {
        	return title;
        }
        
        public String getDescription() {
        	return description;
        }
        
        public String getType() {
        	return type;
        }
        
        public String getImage() {
        	return image;
        }
        
        public String getID() {
        	return id;
        }
        
        /**
         * Full name as defined by user. May not be their actual name
         * @return
         */
        public String getFullName() {
        	return fullName;
        }
        
        /**
         * Number of posts as per profile, all of these posts may not be downloaded
         * @return
         */
        public int getLifetimePostCount() {
        	return numPosts;
        }
        
        public void addInterest(String id, String interest, String interestCount) {
        	interests.put(interest,id + ":" + interestCount);
        }
        
        public boolean hasInterest(String interest) {
        	return interests.containsKey(interest);
        }
        
        public String getInterest(String interest) {
        	return interests.get(interest);
        }
        
        public Iterator<String> getInterests() {
        	return interests.keySet().iterator();
        }
        
        public String getBio() {
        	return bio;
        }
        
        public Blog getFriend(String username) {
        	if (!friends.containsKey(username)) return null;
			return Blog.processBlog(outputDirectory, username);
		}
        
    	public Blog getFriendsOf(String username) {
    		if (!friendsOf.containsKey(username)) return null;
			return Blog.processBlog(outputDirectory, username);
    	}
    	
    	public Blog getMutualFriends(String username) {
    		if (!mutualFriends.containsKey(username)) return null;
			return Blog.processBlog(outputDirectory, username);
    	}
    	
    	public void addFriend(String username, String type) {
    		if (type.equals("Friend")) {
    			friends.put(username, username);
    		}
    		else if (type.equals("FriendOf")) {
    			friendsOf.put(username, username);
    		}
    		else if (type.equals("MutualFriend")) {
    			mutualFriends.put(username, username);
    		}
    	}
    	
    	public int getNumEntries() {
    		return entries.size();
    	}
    	
        /**
         * @param args
         */
        public static void main(String[] args) throws Exception {
			System.err.println("START: " + new Date());
        	
        	// code to process blog
			File directory = new File(args[0]);
			System.out.println(directory);
			
			String [] files = directory.list();
			int i = 0;
			
			for (String file : files) {
				System.out.println(i + "." + file);
	       		Blog b = Blog.processBlog(directory + "/" + file);
	    		Iterator<?> it = b.getEntries();
	    		
	    		while (it.hasNext()) {
	    			i++;
	    			//blog.threaded.create_debate.Entry e = (blog.threaded.create_debate.Entry)b.getEntry((String)it.next());
	    			//System.out.println(i + "." + e.getTitle() + ": " + e.getEntryText());
	    			//System.out.println("---------------------- COMMENTS -----------------");
	    			//System.out.println(e.getSortedCommentsToString());
	    		}
			}
        	System.err.println("END: " + new Date());
        }
}
