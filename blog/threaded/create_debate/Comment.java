/**
 * 
 */
package blog.threaded.create_debate;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import org.w3c.dom.*;

import blog.Sentence;

/**
 * @author sara
 *
 */
public class Comment extends blog.threaded.Comment {

	protected String [] words;
	protected String [] pos;
	String commentText;
	String posText;
	String userBlogURL;
	String userProfileURL;
	String _side;
	String _correctSide;
	
	protected ArrayList<Sentence> sentences; 
	
	/**
	 * @param commentUrlIn
	 * @param usernameIn
	 * @param dateIn
	 * @param commentIn
	 */
	public Comment(String commentUrlIn, String usernameIn, Date dateIn,
			String commentIn, String parentUrlIn, String parentIdIn, String userBlogURLIn, String userProfileURLIn, String commentId, String side, String correctSide ) {
		super(commentUrlIn, usernameIn, dateIn, commentIn, commentId, parentUrlIn, parentIdIn);

		sentences = new ArrayList<Sentence>();
		userBlogURL = userBlogURLIn;
		userProfileURL = userProfileURLIn;
		_side = side;
		_correctSide = correctSide;
		
		if (comment == null) return;
		
		String [] data = comment.split(" ");
		commentText = "";
		posText = "";
		words = new String[data.length];
		pos = new String[data.length];
		
		// check if pos is available
		if (comment.split("/").length >= data.length) {
			
			for (int index = 0; index < data.length; index++) {
				if (data[index].equals("")) continue;
				
				int split = data[index].lastIndexOf("/");
				//System.out.println("<" + word[0] + ">");
				
				
				
				if (split == -1 || !data[index].substring(split+1).matches("[A-Z\\p{Punct}]*")) { //data[index].length() - split > 5) {
					words[index] = data[index];
					commentText += data[index];  
				}
				else {
					words[index] = data[index].substring(0,split);
					pos[index] = data[index].substring(split+1);
					
					if (words[index].matches("-RRB-")) 
						words[index] = ")";
					else if (words[index].matches("-LRB-"))
						words[index] = "(";
					
					commentText += words[index] + " ";
					posText += pos[index] + " ";
				}
			}
			commentText.trim();
			//commentText = commentText.replaceAll("-RRB-|-LRB-", "");
			posText.trim();
		}
		else {
			words = data;
			commentText = comment;
		}
		commentText = web.HTML.htmlToText(commentText);
	}
	
	public static Comment processComment(Element comment) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a z");
			Date dateTime = comment.getAttribute("date-time") == null || comment.getAttribute("date-time").isEmpty() ? null : 
				format.parse(comment.getAttribute("date-time").replace("P", "PM"));
			String parentURL = !comment.getAttribute("parent-url").equals("") ? 
					comment.getAttribute("parent-url") : null;
			String parentID = parentURL;
							
			//if (dateTime == null) return null;
			// thread structure is missing
			if (parentID == null && parentURL == null) return null;
			
			String url = comment.getAttribute("url");
			Element userNode = (Element)comment.getElementsByTagName("user").item(0);
	    	String user = userNode == null ? "anonymous" : userNode.getFirstChild().getNodeValue();
	    	
	    	String commentID = "-1";
	    	if (url == null || url.equals("")) 
	    		url = dateTime.toString();
	    	else {
	    		try {
		    		if (url.indexOf("=") >= 0)
		    			commentID = url.substring(url.indexOf("=")+1,url.indexOf("#"));
		    		else 
	    				commentID = url;
	    		} catch (NumberFormatException ne) {
	    			System.err.println("[Comment.processComment] error getting comment ID: " + url);
	    		}
	    	}
	    	
	    	String side = comment.getAttribute("side");
	    	String correctside = comment.getAttribute("correct_side");

	    	String blogURL = "";
	    	String profileURL = "";
	    	
	    	if (userNode != null) {
		    	blogURL = userNode.getAttribute("blog_url") == null || userNode.getAttribute("blog_url").equals("") ? null : userNode.getAttribute("blog_url");
				profileURL = userNode.getAttribute("profile_url") == null || userNode.getAttribute("profile_url").equals("") ? null : userNode.getAttribute("profile_url");
	    	}
	    	
	    	NodeList textTags = comment.getElementsByTagName("string");
	    	if (textTags.getLength() == 0) textTags = comment.getElementsByTagName("sentence");
	    	
	    	String text = "";
	    	
	    	
	    	if (textTags.getLength() > 0) {
	    		for (int index = 0; textTags != null && index < textTags.getLength(); index++) {
		    	
		    		text += textTags.item(index) != null && textTags.item(index).getFirstChild() != null ? 
		    				web.HTML.htmlToText(textTags.item(index).getFirstChild().getNodeValue()) : "" + " ";
	    		}
    		}
	    	else {
	    		if (comment.getElementsByTagName("text").item(0).getFirstChild() == null) return null;
	    		text += web.HTML.htmlToText(comment.getElementsByTagName("text").item(0).getFirstChild().getTextContent());
	    	}
	    	
	    	Comment c = new Comment(url,user,dateTime,text,parentURL,parentID,blogURL,profileURL,commentID,side, correctside); 
	    	
	    	if (textTags.getLength() > 0) {
	    		for (int index = 0; textTags != null && index < textTags.getLength(); index++) {
	    			Sentence s = Sentence.processSentence((Element)textTags.item(index));
		    		c.addSentence(s);
	    		}
	    	}
	    	else {
	    		c.addSentence(Sentence.processSentence(text));
	    	}
	    	return c;
	    	
		} catch (Exception e) {
			System.err.println("Error occurred in Entry.processComment: " + e);
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getCommentText() {
		return commentText;
	}
	
	public String getPOSText() {
		return posText;
	}
	
	public String getWord(int index) {
		if (words == null) return null;
		return words[index];
	}
	
	public String getPOS(int index) {
		if (pos == null) return null;
		
		return pos[index];
	}
	
	public String getWord(int sentenceIndex, int wordIndex) {
		if (sentences.get(sentenceIndex) == null) return null;
		return sentences.get(sentenceIndex).getWord(wordIndex);
	}
	
	public String getPOS(int sentenceIndex, int wordIndex) {
		if (sentences.get(sentenceIndex) == null) return null;
		return sentences.get(sentenceIndex).getPOS(wordIndex);
	}
	
	public void addSentence(Sentence s) {
		if (s == null) return;
		sentences.add(s);
	}
	
	public Sentence getSentence(int index) {
		return sentences.get(index);
	}
	
	public ArrayList<Sentence> getSentences() {
		return sentences;
	}
	
	public String getUserBlogURL() {
		return userBlogURL;
	}
	
	public String getUserProfileURL() {
		return userProfileURL;
	}
	
	public String getSide() {
		return _side;
	}
	
	public String getCorrectSide() {
		if (_correctSide.isEmpty()) return _side;
		return _correctSide;
	}
	
	public int compareTo(Comment c) {
		if (c == null) return 0;
		if (Integer.valueOf(this.getID()) >= 0)
			return Integer.valueOf(this.getID()).compareTo(Integer.valueOf(c.getID()));
		else return super.compareTo(c);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
