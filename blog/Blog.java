/**
 * 
 */
package blog;

import java.util.*;
import java.net.*;
import org.w3c.dom.*;

import processing.NlpUtils;
/**
 * @author sara
 *
 */
public abstract class Blog {

	// profile id
	protected String username;
	// language blog is written in
	protected String language;
	protected Date dob;
	protected String country;
	protected String city;
	protected URL homepage;
	// blog posts
	protected Hashtable<String,Entry> entries;
	// people who are their friends.
	protected Hashtable<String,String> friends;
	// people who have friended them. 
	protected Hashtable<String,String> friendsOf;
	// people who are their friends and have friended them. 
	protected Hashtable<String,String> mutualFriends;
	
	protected String politicalParty;
	protected String politicalView;
	protected String religion;
	protected String gender;
	
	Document profileFile;
	ArrayList<Document> entriesFiles;
	
	public Blog(String usernameIn, String languageIn, Date dobIn, String countryIn, String cityIn, URL homepageIn,
			 String politicalPartyIn, String politicalViewIn, String religionIn) {
		username = usernameIn;
		language = languageIn;
		dob = dobIn;
		country = countryIn;
		city = cityIn;
		homepage = homepageIn;
		politicalParty = politicalPartyIn;
		politicalView = politicalViewIn;
		religion = religionIn;
		friends = new Hashtable<String,String>();
		friendsOf = new Hashtable<String,String>();
		mutualFriends = new Hashtable<String,String>();
		entries = new Hashtable<String,Entry>();
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String usernameIn) {
		username = usernameIn;		
	}
	
	public void setPoliticalView(String politicalViewIn) {
		politicalView = politicalViewIn;
	}

	public void setPoliticalParty(String politicalPartyIn) {
		politicalParty = politicalPartyIn;
	}
	
	public void setReligion(String religionIn) {
		religion = religionIn;
		if (religion == null) return;
		if (religion.indexOf("buddh") >= 0) religion = "buddhism";
		if (religion.indexOf("christ") >= 0) religion = "christian";
		if (religion.indexOf("hind") >= 0) religion = "hinduism";	
	}

	public String getLanguage() {
		return language;
	}
	
	public Date getDOB() {
		return dob;
	}
	
	public void setDOB(Date dobIn) {
		dob = dobIn;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String genderIn) {
		if (genderIn.toUpperCase().startsWith("M")) gender = "M";
		else if (genderIn.toUpperCase().startsWith("F")) gender = "F";
	}
	
	public String getPoliticalParty() {
		return politicalParty;
	}

	public String getPoliticalView() {
		return politicalView;
	}

	public String getReligion() {
		return religion;
	}

	/**
	 * Compute the age here using today's date and dob.
	 * @return
	 */
	public String getAge() {
		return "";
	}
	
	public String getCountry() {
		return country;
	}
	
	public String getCity() {
		return city;
	}
	
	public URL getHomePage() {
		return homepage;
	}
	
	public int getFriendCount() {
		return friends.size() + friendsOf.size();
	}
	
	public void addFriend(String username) {
		friends.put(username, username);
	}
	
	public void addFriendsOf(String username) {
		friendsOf.put(username, username);
	}
	
	public void addEntry(Entry entry) {
		entries.put(entry.getURL(),entry);
	}
	
	public abstract Blog getFriend(String username);
	
	/**
	 * If they are your friend you are also watching them
	 * @param username
	 * @return
	 */
	public abstract Blog getFriendsOf(String username);
	
	public Entry getEntry(String url) {
		return entries.get(url);
	}
	
	public Iterator<String> getFriends() {
		return friends.keySet().iterator();
	}
	
	public boolean isFriend(String username) {
		return friends.containsKey(username);
	}
	
	public boolean isFriendOf(String username) {
		return friendsOf.containsKey(username);
	}
	
	public boolean isMutualFriend(String username) {
		return mutualFriends.containsKey(username);
	}
	
	public Iterator<String> getAllFriendsOf() {
		return friendsOf.keySet().iterator();
	}
	
	public Iterator<String> getMutualFriends() {
		return mutualFriends.keySet().iterator();
	}
	
	/*public boolean mutualFriends(String username) {
		Blog friend = this.getFriend(username);
		if (friend == null) return false;
		if (!friend.isFriend(this.username)) return false;
		return true;
	}*/

	public Iterator<String> getEntries() {
		return entries.keySet().iterator();
	}
	
	public List<String> getBlogText() {
		
		Iterator<String> entries = this.getEntries();
		
		String text = "";
		
		while (entries.hasNext()) {
			Entry e = this.getEntry((String)entries.next());
			text += e.getEntryText() + "\n\n";
			
			Iterator<?> comments = e.getComments();
			
			while (comments.hasNext()) {
				Comment c = e.getComment((String)comments.next());
				text += c.getCommentText() + "\n\n";
			}
		}
		
		// use sentence splitter to ensure that each sentence is on 
		// it's own line so that sentences will be looked at for sentiment
    	return NlpUtils.splitSentences(text);
    }
	
	/**
	 * To Do: given date range, get all entries within that range and sort them. 
	 * @return
	 */
	/*public Object [] getEntriesSortedByDate(Date start, Date end) {
		return entries.keySet().toArray();
	}*/
}
